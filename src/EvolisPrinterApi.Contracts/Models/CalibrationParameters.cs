﻿using System;

namespace EvolisPrinterApi.Contracts.Models
{
    public class CalibrationParameters
    {
        public TimeSpan? MaxAllowedTime { get; set; }
    }
}