﻿namespace EvolisPrinterApi.Contracts.Models
{
    public class PrinterInfo
    {
        public int PrintsTillNextCleaning { get; set; }

        public int AdvancedCleaningCount { get; set; }

        public int AverageCleaningCount { get; set; }
    }
}