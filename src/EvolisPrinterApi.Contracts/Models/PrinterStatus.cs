﻿using System.Collections.Generic;

namespace EvolisPrinterApi.Contracts.Models
{
    public class PrinterStatus
    {
        public bool PrinterBusy { get; set; }

        public bool LcdDetected { get; set; }

        public bool PrinterNotLocked { get; set; }

        public bool PrinterCoverOpen { get; set; }

        public bool CardFeederEmpty { get; set; }

        public bool HopperFull { get; set; }

        public bool RejectBoxFull { get; set; }

        public bool NoRibbon { get; set; }

        public bool RibbonUsed { get; set; }

        public bool UnsupportedRibbon { get; set; }

        public bool MechanicalError { get; set; }

        public bool FeederError { get; set; }

        public bool ErrorWritingMagneticStripe { get; set; }

        public bool RibbonError { get; set; }

        public bool IsPrinterReady => !PrinterBusy && !PrinterNotLocked && !PrinterCoverOpen && !CardFeederEmpty &&
                                      !HopperFull && !RejectBoxFull && !NoRibbon && !RibbonUsed &&
                                      !UnsupportedRibbon && !MechanicalError && !FeederError &&
                                      !ErrorWritingMagneticStripe && !RibbonError;

        public ISet<InfoStatusFlag> InfoFlags { get; set; }

        public ISet<WarningStatusFlag> WarningFlags { get; set; }

        public ISet<ErrorStatusFlag> ErrorFlags { get; set; }

        public ISet<ConfigStatusFlag> ConfigFlags { get; set; }
    }
}