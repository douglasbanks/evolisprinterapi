﻿namespace EvolisPrinterApi.Contracts.Models
{
    public enum ConfigStatusFlag : uint 
    {  
                        CFG_X01 = 0x80000000,
                        CFG_X02 = 0x40000000,
                        CFG_R02 = 0x20000000,
                        CFG_X04 = 0x10000000,
                CFG_EXTENSION_1 = 0x08000000,
                        CFG_S01 = 0x04000000,
                        CFG_X07 = 0x02000000,
                      CFG_KC200 = 0x01000000,
                       CFG_WIFI = 0x00800000,
                   CFG_ETHERNET = 0x00400000,
                CFG_USB_OVER_IP = 0x00200000, 
                       CFG_FLIP = 0x00100000,
                CFG_CONTACTLESS = 0x00080000,
                      CFG_SMART = 0x00040000,
                   CFG_MAGNETIC = 0x00020000,
                    CFG_REWRITE = 0x00010000,
              CFG_FEED_MANUALLY = 0x00008000,
                CFG_FEED_BY_CDE = 0x00004000,
             CFG_FEED_BY_FEEDER = 0x00002000,
              CFG_EJECT_REVERSE = 0x00001000,
           CFG_FEED_CDE_REVERSE = 0x00000800,
        CFG_EXTENDED_RESOLUTION = 0x00000400,
                        CFG_LCD = 0x00000200,
                       CFG_LOCK = 0x00000100,
                        CFG_OEM = 0x00000080,
               CFG_JIS_MAG_HEAD = 0x00000040,
                CFG_REJECT_SLOT = 0x00000020,
                     CFG_IO_EXT = 0x00000010,
                  CFG_MONO_ONLY = 0x00000008,
                      CFG_KC100 = 0x00000004,
                       CFG_KINE = 0x00000002,
                   CFG_WIFI_ENA = 0x00000001
    }

    public enum InfoStatusFlag : uint
    {
                        INF_CLAIM = 0x80000000,
                  INF_CARD_HOPPER = 0x40000000,
                  INF_CARD_FEEDER = 0x20000000,
                    INF_CARD_FLIP = 0x10000000,
             INF_CARD_CONTACTLESS = 0x08000000,
                   INF_CARD_SMART = 0x04000000,
                   INF_CARD_PRINT = 0x02000000,
                   INF_CARD_EJECT = 0x01000000,
               INF_PRINTER_MASTER = 0x00800000,
                 INF_PCSVC_LOCKED = 0x00400000,
                   INF_SLEEP_MODE = 0x00200000,
               INF_UNKNOWN_RIBBON = 0x00100000,
                   INF_RIBBON_LOW = 0x00080000,
           INF_CLEANING_MANDATORY = 0x00040000,
                     INF_CLEANING = 0x00020000,
                        INF_RESET = 0x00010000,
            INF_CLEAN_OUTWARRANTY = 0x00008000, 
       INF_CLEAN_LAST_OUTWARRANTY = 0x00004000,
               INF_CLEAN_2ND_PASS = 0x00002000,
           INF_READY_FOR_CLEANING = 0x00001000,
            INF_CLEANING_ADVANCED = 0x00000800,
            INF_WRONG_ZONE_RIBBON = 0x00000400,
               INF_RIBBON_CHANGED = 0x00000200,
            INF_CLEANING_REQUIRED = 0x00000100,
             INF_PRINTING_RUNNING = 0x00000080,
             INF_ENCODING_RUNNING = 0x00000040,
             INF_CLEANING_RUNNING = 0x00000020,
             INF_WRONG_ZONE_ALERT = 0x00000010,
           INF_WRONG_ZONE_EXPIRED = 0x00000008,
           INF_SYNCH_PRINT_CENTER = 0x00000004,
            INF_UPDATING_FIRMWARE = 0x00000002,
                         INF_BUSY = 0x00000001
    }

    public enum WarningStatusFlag : uint
    {
                  // Reserved = 0x80000000
                  // Reserved = 0x40000000
                  // Reserved = 0x20000000
          DEF_RECEPTACLE_OPEN = 0x10000000,
          DEF_REJECT_BOX_FULL = 0x08000000,
            DEF_CARD_ON_EJECT = 0x04000000,
                DEF_WAIT_CARD = 0x02000000, 
             DEF_FEEDER_EMPTY = 0x01000000,
                  // Reserved = 0x00800000
                  // Reserved = 0x00400000
                  DEF_COOLING = 0x00200000,
              DEF_HOPPER_FULL = 0x00100000,
             DEF_RIBBON_ENDED = 0x00080000,
           DEF_PRINTER_LOCKED = 0x00040000,
               DEF_COVER_OPEN = 0x00020000,
                DEF_NO_RIBBON = 0x00010000,
       DEF_UNSUPPORTED_RIBBON = 0x00008000,
       DEF_RET_TEMP_NOT_READY = 0x00004000,
                 DEF_NO_CLEAR = 0x00002000,
              DEF_CLEAR_ENDED = 0x00001000,
        DEF_CLEAR_UNSUPPORTED = 0x00000800,
    DEF_REJECT_BOX_COVER_OPEN = 0x00000400
                  // Reserved = 0x00000200
                  // Reserved = 0x00000100
                  // Reserved = 0x00000080
                  // Reserved = 0x00000040
                  // Reserved = 0x00000020
                  // Reserved = 0x00000010
                  // Reserved = 0x00000008
                  // Reserved = 0x00000004
                  // Reserved = 0x00000002
                  // Reserved = 0x00000001
    }

    public enum ErrorStatusFlag : uint
    {
                // Reserved = 0x80000000
                // Reserved = 0x40000000
              ERR_HEAD_TEMP = 0x20000000,
              ERR_NO_OPTION = 0x10000000,
           ERR_FEEDER_ERROR = 0x08000000,
           ERR_RIBBON_ERROR = 0x04000000,
             ERR_COVER_OPEN = 0x02000000,
             ERR_MECHANICAL = 0x01000000,
        ERR_REJECT_BOX_FULL = 0x00800000,
             ERR_BAD_RIBBON = 0x00400000,
           ERR_RIBBON_ENDED = 0x00200000,
          ERR_HOPPER_FULL_F = 0x00100000,
            ERR_BLANK_TRACK = 0x00080000,
          ERR_MAGNETIC_DATA = 0x00040000,
          ERR_READ_MAGNETIC = 0x00020000,
         ERR_WRITE_MAGNETIC = 0x00010000,
                ERR_FEATURE = 0x00008000,
        ERR_RET_TEMPERATURE = 0x00004000,
            ERR_CLEAR_ERROR = 0x00002000,
            ERR_CLEAR_ENDED = 0x00001000
                // Reserved = 0x00000800
                // Reserved = 0x00000400
                // Reserved = 0x00000200
                // Reserved = 0x00000100
                // Reserved = 0x00000080
                // Reserved = 0x00000040
                // Reserved = 0x00000020
                // Reserved = 0x00000010
                // Reserved = 0x00000008
                // Reserved = 0x00000004
                // Reserved = 0x00000002
                // Reserved = 0x00000001
    }

    public enum Extension1StatusFlag : uint
    {
                    CFG_EXTENSION_2 = 0x80000000,
                          CFG_KIOSK = 0x40000000,
                        CFG_QUANTUM = 0x20000000,
                       CFG_SECURION = 0x10000000,
                         CFG_DUALYS = 0x08000000,
                         CFG_PEBBLE = 0x04000000,
                         //Reserved = 0x02000000
        CFG_MEM_LAMINATION_MODULE_2 = 0x01000000,
            INF_NO_LAMINATION_TO_DO = 0x00800000,
                   CFG_SEICO_FEEDER = 0x00400000,
                CFG_KYTRONIC_FEEDER = 0x00200000,
                         CFG_HOPPER = 0x00100000,
                      CFG_LAMINATOR = 0x00080000,
           INF_LAMI_ALLOW_TO_INSERT = 0x00040000,
             INF_LAMINATING_RUNNING = 0x00020000,
                 INF_CLEAN_REMINDER = 0x00010000,
            INF_LAMI_TEMP_NOT_READY = 0x00008000,
               INF_SYNCHRONOUS_MODE = 0x00004000,
                    INF_LCD_BUT_ACK = 0x00002000,
                     INF_LCD_BUT_OK = 0x00001000,
                  INF_LCD_BUT_RETRY = 0x00000800,
                 INF_LCD_BUT_CANCEL = 0x00000400,
                          CFG_BEZEL = 0x00000200,
              INF_FEEDER_NEAR_EMPTY = 0x00000100,
                  INF_FEEDER1_EMPTY = 0x00000080,
                  INF_FEEDER2_EMPTY = 0x00000040,
                  INF_FEEDER3_EMPTY = 0x00000020,
                  INF_FEEDER4_EMPTY = 0x00000010,
             INF_FEEDER1_NEAR_EMPTY = 0x00000008,
             INF_FEEDER2_NEAR_EMPTY = 0x00000004,
             INF_FEEDER3_NEAR_EMPTY = 0x00000002,
             INF_FEEDER4_NEAR_EMPTY = 0x00000001
    }

    public enum Extension2StatusFlag : uint
    {
                    CFG_EXTENSION_3 = 0x80000000,
                  INF_SA_PROCESSING = 0x40000000,
                 INF_SCP_PROCESSING = 0x20000000,
                 INF_OPT_PROCESSING = 0x10000000,
           INF_X08_PRINTER_UNLOCKED = 0x08000000,
                INF_X08_FEEDER_OPEN = 0x04000000,
              INF_X08_EJECTBOX_FULL = 0x02000000,
             INF_X08_PRINT_UNLOCKED = 0x01000000,
            CFG_LAMINATION_MODULE_2 = 0x00800000,
               INF_LAMINATE_UNKNOWN = 0x00400000,
                   INF_LAMINATE_LOW = 0x00200000,
                      INF_LAMI_CARD = 0x00100000,
          INF_LAMI_CLEANING_RUNNING = 0x00080000,
          INF_LAMI_UPDATING_FIMWARE = 0x00040000,
        INF_LAMI_READY_FOR_CLEANING = 0x00020000, 
                      INF_CARD_REAR = 0x00010000,
                    DEF_NO_LAMINATE = 0x00008000,
                DEF_LAMI_COVER_OPEN = 0x00004000,
                   DEF_LAMINATE_END = 0x00002000,
               DEF_LAMI_HOPPER_FULL = 0x00001000,
           DEF_LAMINATE_UNSUPPORTED = 0x00000800,
                  INF_CLEAR_UNKNOWN = 0x00000400,
                      INF_CLEAR_LOW = 0x00000200,
               INF_WRONG_ZONE_CLEAR = 0x00000100,
               ERR_LAMI_TEMPERATURE = 0x00000080,
                       ERR_LAMINATE = 0x00000040,
                ERR_LAMI_MECHANICAL = 0x00000020,
                   ERR_LAMINATE_END = 0x00000010,
                ERR_LAMI_COVER_OPEN = 0x00000008,
                  INF_CLEAR_CHANGED = 0x00000004,
         INF_WRONG_ZONE_CLEAR_ALERT = 0x00000002,
       INF_WRONG_ZONE_CLEAR_EXPIRED = 0x00000001
    }
}
