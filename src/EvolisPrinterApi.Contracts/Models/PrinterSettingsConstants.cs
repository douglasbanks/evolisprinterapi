﻿namespace EvolisPrinterApi.Contracts.Models
{
    public class PrinterSettingsConstants
    {
        public const string SharpeningMode = "SharpeningMode";

        public const string MonochromeThreshold = "MonochromeThreshold";

        public const string Pida = "Pida";

        public const string UseColorProfile = "UseColorProfile";

        public const int ContrastColorSettingId = 1;
        
        public const int ContrastBlackSettingId = 2;
        
        public const int LuminosityColorSettingId = 3;
        
        public const int LuminosityBlackSettingId = 4;
        
        public const int JitterEnabledSettingId = 5;
        
        public const int JitterPassesSettingId = 6;
        
        public const int JitterFlagsSettingId = 7;
        
        public const int JitterBlackDitheringModeSettingId = 8;
    }
}