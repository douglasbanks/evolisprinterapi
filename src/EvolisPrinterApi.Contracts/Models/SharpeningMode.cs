﻿namespace EvolisPrinterApi.Contracts.Models
{
    public enum SharpeningMode
    {
        None,
        Evolis,
        Boost
    }
}