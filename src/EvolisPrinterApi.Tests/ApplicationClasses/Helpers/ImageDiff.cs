﻿using System;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Formats.Png;
using SixLabors.ImageSharp.PixelFormats;

namespace EvolisPrinterApiTests.ApplicationClasses.Helpers
{
    public class ImageDiff
    {
        public static void Save(Image<Rgb24> img, Image<Rgb24> imgRef, string s, int margin = 0)
        {
            Image<Rgb24> outputImage = new Image<Rgb24>(img.Width, img.Height, new Rgb24(128, 128, 128));

            for (int x = margin; x < img.Width-margin; ++x)
                for (int y = margin; y < img.Height-margin; ++y)
                {
                    Rgb24 imgPixel = img[x, y];
                    Rgb24 refPixel = imgRef[x, y];

                    if (imgPixel.R != refPixel.R || imgPixel.G != refPixel.G || imgPixel.B != refPixel.B)
                    {
                        int dR = 128 + refPixel.R - imgPixel.R;
                        int dG = 128 + refPixel.G - imgPixel.G;
                        int dB = 128 + refPixel.B - imgPixel.B;
                        
                        outputImage[x, y] = new Rgb24((byte) Math.Clamp(dR, 0, 255), (byte) Math.Clamp(dG, 0, 255), (byte) Math.Clamp(dB, 0, 255));
                    }
                }

                outputImage.Save(s, new PngEncoder());
        }

        public static int Calculate(Image<Rgb24> img, Image<Rgb24> imgRef, int margin = 0)
        {
            int  maximumDiffPerColorPlane = 0;

            for (int x = margin; x < img.Width-margin; ++x)
                for (int y = margin; y < img.Height-margin; ++y)
                {
                    Rgb24 imgPixel = img[x, y];
                    Rgb24 refPixel = imgRef[x, y];
                    maximumDiffPerColorPlane = Math.Max(maximumDiffPerColorPlane, Math.Abs(refPixel.R - imgPixel.R));
                    maximumDiffPerColorPlane = Math.Max(maximumDiffPerColorPlane, Math.Abs(refPixel.G - imgPixel.G));
                    maximumDiffPerColorPlane = Math.Max(maximumDiffPerColorPlane, Math.Abs(refPixel.B - imgPixel.B));
                }
            
            return maximumDiffPerColorPlane;
        }
    }
}