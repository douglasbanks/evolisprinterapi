﻿using StronglyTyped.PixelMatch;

namespace EvolisPrinterApiTests.ApplicationClasses.Helpers
{
    /// <summary>
    /// Compares colors using highest 7 bits of 8 bit color resolution for each of 3 channels (low 3 bytes).
    /// </summary>
    public sealed class PixelMatcher7Bit : AbstractPixelMatcher<uint>
    {
        protected override bool AreEqual(uint color1, uint color2)
        {
            color1 &= 0xFEFEFE;
            color2 &= 0xFEFEFE;
            return color1 == color2;
        }
    }
}
