﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Drawing;
using System.IO;
using EvolisPrinterApi.ApplicationClasses.Drivers;
using EvolisPrinterApi.Contracts.Models;
using EvolisPrinterApiTests.ApplicationClasses.Helpers;
using NUnit.Framework;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.PixelFormats;
using StronglyTyped.PixelMatch;
using Color = System.Drawing.Color;
using Image = SixLabors.ImageSharp.Image;

// ReSharper disable StringLiteralTypo
namespace EvolisPrinterApiTests.ApplicationClasses.Drivers
{
    [TestFixture(), SuppressMessage("ReSharper", "InconsistentNaming")]
    public class EvolisImageCodecTests
    {
        // expected bitmap dimensions
        public const int TestBitmapWidth = 1016;
        public const int TestBitmapHeight = 648;

        // list of test bitmaps
        public const string A00_Orientation = "A00_orientation.png"; // Simple test image showing proper bitmap orientation and all basic ribbon colors when printed.
        public const string A01_TestPattern = "A01_c_img25dbeb6d-bf59-42c9-a267-cc05f8aae71f_1_color.png"; // test pattern showing all shades of gray (255), smooth/quick transitions and fine structures .
        public const string A02_Corners_Black = "A02_corners_cropTest.png"; // Test pattern with fine details designed to recognize resizing and cropping issues at printing.
        public const string A03_Corners_Cyan = "A03_corners_cropTest_C255.png"; // Same as A02 but only in cyan color.
        public const string A04_Corners_Magenta = "A04_corners_cropTest_M255.png"; // Same as A02 but only in magenta color.
        public const string A05_Corners_Yellow = "A05_corners_cropTest_Y255.png"; // Same as A02 but only in yellow color.
        public const string A10_LensTest = "A10_1016x648_Koren2003lensTest.png"; // lens test image
        public const string A10_LensTestGray5 = "A10_grey5_1016x648_Koren2003lensTest.png"; // lens test image with black/gray capped to rgb(5,5,5) minimum color
        public const string A14_Gradients = "A14_Stepchart_large_HSL_1016.png"; // Color test chart with all 16.7M colors
        public const string A14_Gradients_icc = "A14_Stepchart_large_HSL_1016_evolis_icc.png"; // Color test chart with all 16.7M colors using Primacy 2 color profile
        public const string S00_Train = "S00_train.png"; // Sample test image
        public const string S01_FI = "S01_6f65a656-e86d-490d-8059-f05b69e93e3a.png"; // Sample test image
        public const string S02_PCU = "S02_peoples credit union.png"; // Sample test image
        public const string S03_FA = "S03_first american.png"; // Sample test image
        public const string Primacy2_Color_Profile = "Evolis_Primacy_2_color_profile.icm"; // Precision2 color profile

        // image matchers
        private readonly PixelMatcher32 exactMatcher = new PixelMatcher32 { Threshold = 0f };
        private readonly PixelMatcher7Bit pixelMatcher7Bit = new PixelMatcher7Bit { Threshold = 0f };
        private readonly PixelMatcher32 interval5Matcher = new PixelMatcher32 { Threshold = 0.05f };

        private readonly Bitmap diffBitmap = new Bitmap(TestBitmapWidth, TestBitmapHeight);
        private readonly Bitmap diffBitmap90 = new Bitmap(TestBitmapHeight, TestBitmapWidth);

        private void OnDiff(int x, int y, float arg3)
        {
            lock (diffBitmap)
            {
                int x90 = Math.Clamp(x, 0, diffBitmap90.Width-1);
                int y90 = Math.Clamp(y, 0, diffBitmap90.Height-1);
                x = Math.Clamp(x, 0, diffBitmap.Width-1);
                y = Math.Clamp(y, 0, diffBitmap.Height-1);
                int colorDiffR = (int)Math.Clamp(240 - 256*Math.Abs(arg3), 0, 255); // boost on Red
                diffBitmap.SetPixel(x, y, Color.FromArgb(255, 255, colorDiffR, colorDiffR));
                diffBitmap90.SetPixel(x90, y90, Color.FromArgb(255, 255, colorDiffR, colorDiffR));
            }
        }

        private void DumpImageDiffs(string methodName, Bitmap bitmap1, Bitmap bitmap2, Bitmap bitmapDiff, Image<Rgb24> img, Image<Rgb24> imgRef, int margin = 0)
        {
            bitmap1.Save($"{methodName}_bitmap1.png");
            bitmap2.Save($"{methodName}_bitmap2.png");
            bitmapDiff.Save($"{methodName}_diff.png");
            ImageDiff.Save(img, imgRef, $"{methodName}_diff2.png", margin);
        }

        [SetUp()]
        public void Setup()
        {
            for (int y = 0; y < diffBitmap.Height; y++)
                for (int x = 0; x < diffBitmap.Width; x++)
                {
                    diffBitmap.SetPixel(x, y, Color.White);
                    diffBitmap90.SetPixel(y, x, Color.White);
                }
        }

        [Test(Description = "Make sure that library for image comparison works on two similar (but different) images.")]
        public void TestCompareModifiedImageWithOriginal()
        {
            //image 1
            Image<Rgb24> img1 = Image.Load<Rgb24>(Path.Combine("resources", "testimages", A10_LensTest));
            using Bitmap bitmap1 = EvolisImageCodec.ToBitmap(img1);
            using BitmapImagePBgra32 image1 = new BitmapImagePBgra32(bitmap1);

            //image 2
            Image<Rgb24> imgRef = Image.Load<Rgb24>(Path.Combine("resources", "testimages", A10_LensTestGray5));
            using Bitmap bitmap2 = EvolisImageCodec.ToBitmap(imgRef);
            using BitmapImagePBgra32 image2 = new BitmapImagePBgra32(bitmap2);

            Assert.AreEqual(TestBitmapWidth, bitmap1.Width);
            Assert.AreEqual(TestBitmapHeight, bitmap1.Height);
            Assert.AreEqual(TestBitmapWidth, bitmap2.Width);
            Assert.AreEqual(TestBitmapHeight, bitmap2.Height);

            // comparison
            int compareResult = exactMatcher.Compare(image1, image2, OnDiff);
            int coreDiff = ImageDiff.Calculate(img1, imgRef, 0);

            if (compareResult != 0)
            {
                string methodName = System.Reflection.MethodBase.GetCurrentMethod()?.Name;
                DumpImageDiffs(methodName, bitmap1, bitmap2, diffBitmap, img1, imgRef);
            }

            Assert.AreNotEqual(0, compareResult);
            Assert.AreNotEqual(0, coreDiff);
        }

        [Test(Description = "Round trip Rgb24 image to CMY printer raster and back. Images should match exactly.")]
        public void TestRgb24ImageConversionBasic()
        {
            Image<Rgb24> img = Image.Load<Rgb24>(Path.Combine("resources", "testimages", A00_Orientation));
            CMYImageData convertedCMY = EvolisImageCodec.ConvertColorImage(img);
            byte[] raster = convertedCMY.GetRaster();
            CMYImageData cmy2 = new CMYImageData(raster, 1016, 648);
            Image<Rgb24> img2= EvolisImageCodec.DecodeColorImage(cmy2);

            using Bitmap bitmap1 = EvolisImageCodec.ToBitmap(img);
            using Bitmap bitmap2 = EvolisImageCodec.ToBitmap(img2);

            using BitmapImagePBgra32 image1 = new BitmapImagePBgra32(bitmap1);
            using BitmapImagePBgra32 image2 = new BitmapImagePBgra32(bitmap2);

            int compareResult = exactMatcher.Compare(image1, image2, OnDiff);
            int coreDiff = ImageDiff.Calculate(img, img2, 0);

            if (compareResult != 0)
            {
                string methodName = System.Reflection.MethodBase.GetCurrentMethod()?.Name;
                DumpImageDiffs(methodName, bitmap1, bitmap2, diffBitmap, img, img2);
            }

            Assert.AreEqual(0, compareResult);
            Assert.AreEqual(0, coreDiff);
        }

        [Test(Description = "Make sure that sharpening/smoothing does change the input image at least.")]
        [TestCase(SharpeningMode.Evolis)]
        [TestCase(SharpeningMode.Boost)]
        public void TestImageSharpeningWorks1(SharpeningMode sharpeningMode)
        {
            // img1
            Image<Rgb24> img1 = Image.Load<Rgb24>(Path.Combine("resources", "testimages", A01_TestPattern));
            CMYImageData convertedCMY = EvolisImageCodec.ConvertColorImage(img1, true, sharpeningMode);
            byte[] raster = convertedCMY.GetRaster(); // get down to raster level to test whole conversion codebase

            // img2 - decode result of img1 smoothing
            CMYImageData cmy2 = new CMYImageData(raster, 1016, 648);
            Image<Rgb24> imgSmooth = EvolisImageCodec.DecodeColorImage(cmy2); // decode "Smooth" image

            using Bitmap bitmap1 = EvolisImageCodec.ToBitmap(img1);
            using Bitmap bitmap2 = EvolisImageCodec.ToBitmap(imgSmooth);

            //comparison
            using BitmapImagePBgra32 image1 = new BitmapImagePBgra32(bitmap1);
            using BitmapImagePBgra32 image2 = new BitmapImagePBgra32(bitmap2);

            int compareResult = exactMatcher.Compare(image1, image2, OnDiff);
            int coreDiff = ImageDiff.Calculate(img1, imgSmooth, 0);

            if (compareResult != 0)
            {
                string methodName = System.Reflection.MethodBase.GetCurrentMethod()?.Name;
                DumpImageDiffs(methodName, bitmap1, bitmap2, diffBitmap, img1, imgSmooth);
            }

            // result image should be different from original
            Assert.AreNotEqual(0, compareResult);
            Assert.AreNotEqual(0, coreDiff);
        }

        [Test(Description = "Compare sharpened image with reference image obtained from reference tool (Java test tool based on evolisPrimacyG2.jar), working with png + ras.")]
        [TestCase(A00_Orientation, "A00")]
        [TestCase(A01_TestPattern, "A01")]
        [TestCase(A02_Corners_Black, "A02")]
        [TestCase(A03_Corners_Cyan, "A03")]
        [TestCase(A04_Corners_Magenta, "A04")]
        [TestCase(A05_Corners_Yellow, "A05")]
        [TestCase(A14_Gradients, "A14")]
        [TestCase(S00_Train, "S00")]
        [TestCase(S01_FI, "S01")]
        [TestCase(S02_PCU, "S02")]
        [TestCase(S03_FA, "S03")]
        public void TestImageSharpening3(string imageNamePng, string testRunName)
        {
            // img1 
            Image<Rgb24> img = Image.Load<Rgb24>(Path.Combine("resources", "testimages", imageNamePng));
            CMYImageData convertedCMY = EvolisImageCodec.ConvertColorImage(img, true, SharpeningMode.Evolis);
            byte[] raster = convertedCMY.GetRaster(); // get down to raster level to test whole conversion codebase
            CMYImageData cmy2 = new CMYImageData(raster, 1016, 648);
            Image<Rgb24> imgSmooth = EvolisImageCodec.DecodeColorImage(cmy2); // decode "Smooth" image

            // img2
            string img2Name = $"{imageNamePng.Substring(0, imageNamePng.Length - 4)}_smooth.ras";
            byte[] ras = File.ReadAllBytes(Path.Combine("resources", "testimages", img2Name));
            CMYImageData cmy1 = new CMYImageData(ras, 1016, 648);
            Image<Rgb24> img2 = EvolisImageCodec.DecodeColorImage(cmy1);

            using Bitmap bitmap1 = EvolisImageCodec.ToBitmap(imgSmooth);
            using Bitmap bitmap2 = EvolisImageCodec.ToBitmap(img2);

            using BitmapImagePBgra32 image1 = new BitmapImagePBgra32(bitmap1);
            using BitmapImagePBgra32 image2 = new BitmapImagePBgra32(bitmap2);

            // comparison - small differences are expected
            // note: 1. C# codebase is clamping pixel values at image border (convolution matrix is 5x5) by using border pixel values at the edge while Java code is not calculating anything for 2px wide border.
            //       2. C# codebase is rounding pixel values instead of removing fractional part when doing convolution.
            int compareResult = exactMatcher.Compare(image1, image2, OnDiff); // this is expected to find difference for most if not all images
            int coreDiff = ImageDiff.Calculate(imgSmooth, img2, 5); // calculate maximum pixel color difference excluding 5px margin from all borders
            
            if (compareResult != 0)
            {
                string methodName = $"{System.Reflection.MethodBase.GetCurrentMethod()?.Name}_{testRunName}";
                DumpImageDiffs(methodName, bitmap1, bitmap2, diffBitmap, imgSmooth, img2, 5);
            }

            Assert.LessOrEqual(coreDiff, 1); // relaxed condition due to known difference between original and ported codebase
        }

        // The .ras file was prepared using original Java code, exported using 'Pida' and smooth (level 1) params. Reference image is a source image (loaded also by Java code).
        [Test()]
        public void TestImageLoadingFromRaster1()
        {
            //img1 from raster
            byte[] ras = File.ReadAllBytes(Path.Combine("resources", "testimages", "A01_TestImageLoadingFromRaster1_bitmap1_c_img25dbeb6d-bf59-42c9-a267-cc05f8aae71f_1_color.ras"));
            CMYImageData cmy1 = new CMYImageData(ras, 1016, 648);
            Image<Rgb24> imgRasterLoaded = EvolisImageCodec.DecodeColorImage(cmy1);
            using Bitmap bitmap1 = EvolisImageCodec.ToBitmap(imgRasterLoaded);

            //img2 is a reference image
            Image<Rgb24> imgRef = Image.Load<Rgb24>(Path.Combine("resources", "testimages", "A01_TestImageLoadingFromRaster1_bitmap1_c_img25dbeb6d-bf59-42c9-a267-cc05f8aae71f_1_color.png"));
            using Bitmap bitmap2 = EvolisImageCodec.ToBitmap(imgRef);

            //comparison
            using BitmapImagePBgra32 image1 = new BitmapImagePBgra32(bitmap1);
            using BitmapImagePBgra32 image2 = new BitmapImagePBgra32(bitmap2);

            int compareResult = exactMatcher.Compare(image1, image2, OnDiff);
            int coreDiff = ImageDiff.Calculate(imgRasterLoaded, imgRef, 5);
            
            if (compareResult != 0)
            {
                string methodName = System.Reflection.MethodBase.GetCurrentMethod()?.Name;
                DumpImageDiffs(methodName, bitmap1, bitmap2, diffBitmap, imgRasterLoaded, imgRef);
            }

            Assert.AreEqual(0, compareResult);
            Assert.AreEqual(0, coreDiff);
        }

        // The same as TestImageLoadingFromRaster1 but using monochrome reference raster and mono converter.
        [Test()]
        public void TestImageLoadingFromRaster2()
        {
            //img1 from raster
            byte[] ras = File.ReadAllBytes(Path.Combine("resources", "testimages", "A01_TestImageLoadingFromRaster1_bitmap1_c_img25dbeb6d-bf59-42c9-a267-cc05f8aae71f_1_color_mono.ras"));
            MonochromeImageData cmy1 = new MonochromeImageData(ras, TestBitmapHeight, TestBitmapWidth);
            Image<Rgb24> imgRasterLoaded = EvolisImageCodec.DecodeMonoImage(cmy1);
            using Bitmap bitmap1 = EvolisImageCodec.ToBitmap(imgRasterLoaded);

            //img2 is a reference image
            Image<Rgb24> img2 = Image.Load<Rgb24>(Path.Combine("resources", "testimages", "A01_TestImageLoadingFromRaster1_bitmap1_c_img25dbeb6d-bf59-42c9-a267-cc05f8aae71f_1_color.png"));

            MonochromeImageData imgMono = EvolisImageCodec.ConvertMonoImage(img2, "k");
            Image<Rgb24> img2Mono = EvolisImageCodec.DecodeMonoImage(imgMono);
            using Bitmap bitmap2 = EvolisImageCodec.ToBitmap(img2Mono);

            //comparison
            using BitmapImagePBgra32 image1 = new BitmapImagePBgra32(bitmap1);
            using BitmapImagePBgra32 image2 = new BitmapImagePBgra32(bitmap2);

            int compareResult = exactMatcher.Compare(image1, image2, OnDiff);
            int coreDiff = ImageDiff.Calculate(imgRasterLoaded, img2Mono, 0);
            
            if (compareResult != 0)
            {
                string methodName = System.Reflection.MethodBase.GetCurrentMethod()?.Name;
                DumpImageDiffs(methodName, bitmap1, bitmap2, diffBitmap90, imgRasterLoaded, img2Mono);
            }

            Assert.AreEqual(0, compareResult);
            Assert.AreEqual(0, coreDiff);
        }

        // The same as TestImageLoadingFromRaster1 but using monochrome reference raster slightly different (smoothed) reference image (should produce diff images of proper size and orientation).
        [Test()]
        public void TestImageLoadingFromRaster3()
        {
            //img1 from raster
            byte[] ras = File.ReadAllBytes(Path.Combine("resources", "testimages", "A01_TestImageLoadingFromRaster1_bitmap1_c_img25dbeb6d-bf59-42c9-a267-cc05f8aae71f_1_color_mono.ras"));
            MonochromeImageData cmy1 = new MonochromeImageData(ras, TestBitmapHeight, TestBitmapWidth);
            Image<Rgb24> imgRasterLoaded = EvolisImageCodec.DecodeMonoImage(cmy1);
            using Bitmap bitmap1 = EvolisImageCodec.ToBitmap(imgRasterLoaded);

            //img2 is a reference image
            Image<Rgb24> img2 = Image.Load<Rgb24>(Path.Combine("resources", "testimages", "A01_gimp_reference_smooth_c_img25dbeb6d-bf59-42c9-a267-cc05f8aae71f_1_color.png"));

            MonochromeImageData imgMono = EvolisImageCodec.ConvertMonoImage(img2, "k");
            Image<Rgb24> img2Mono = EvolisImageCodec.DecodeMonoImage(imgMono);
            using Bitmap bitmap2 = EvolisImageCodec.ToBitmap(img2Mono);

            //comparison
            using BitmapImagePBgra32 image1 = new BitmapImagePBgra32(bitmap1);
            using BitmapImagePBgra32 image2 = new BitmapImagePBgra32(bitmap2);

            int compareResult = exactMatcher.Compare(image1, image2, OnDiff);
            int coreDiff = ImageDiff.Calculate(imgRasterLoaded, img2Mono, 0);
            
            if (compareResult != 0)
            {
                string methodName = System.Reflection.MethodBase.GetCurrentMethod()?.Name;
                DumpImageDiffs(methodName, bitmap1, bitmap2, diffBitmap90, imgRasterLoaded, img2Mono);
            }

            Assert.AreNotEqual(0, compareResult);
            Assert.AreNotEqual(0, coreDiff);
        }

        // Compare raster prepared by C# codebase with raster output from Java codebase. Settings = SmoothingType.None, Pida - ON
        [Test()]
        [TestCase(A00_Orientation)]
        [TestCase(A01_TestPattern)]
        [TestCase(A02_Corners_Black)]
        [TestCase(A03_Corners_Cyan)]
        [TestCase(A04_Corners_Magenta)]
        [TestCase(A05_Corners_Yellow)]
        [TestCase(A14_Gradients)]
        [TestCase(S00_Train)]
        [TestCase(S01_FI)]
        [TestCase(S02_PCU)]
        [TestCase(S03_FA)]
        public void  TestRasterCreation1(string imageNamePng)
        {
            // img1
            Image<Rgb24> img1 = Image.Load<Rgb24>(Path.Combine("resources", "testimages", imageNamePng));
            CMYImageData image1 = EvolisImageCodec.ConvertColorImage(img1, true, SharpeningMode.None);
            byte[] ras1 = image1.GetRaster();

            // img2 from raster
            string img2Name = $"{imageNamePng.Substring(0, imageNamePng.Length - 4)}.ras";
            byte[] ras2 = File.ReadAllBytes(Path.Combine("resources", "testimages", img2Name));

            Assert.AreEqual(ras2.Length, ras1.Length);

            for (int i = 0; i < ras2.Length; ++i)
            {
                Assert.AreEqual(ras2[i], ras1[i], 0, $"Position: {i}");
            }
        }

        [Test()]
        public void EvolisImageCodedAppliesColorProfileToColorImages()
        {
            Image<Rgb24> img = Image.Load<Rgb24>(Path.Combine("resources", "testimages", A14_Gradients_icc));
            CMYImageData cmy = EvolisImageCodec.ConvertColorImage(img, true, SharpeningMode.None, Path.Combine("resources", Primacy2_Color_Profile));
            byte[] ras = cmy.GetRaster();

            string rasFilename = $"{A14_Gradients_icc.Substring(0, A14_Gradients_icc.Length - 4)}.ras";
            byte[] rasIcc = File.ReadAllBytes(Path.Combine("resources", "testimages", rasFilename));

            Assert.AreEqual(ras.Length, rasIcc.Length, "Rasters differ in length");

            for (int i = 0; i < rasIcc.Length; ++i)
            {
                Assert.AreEqual(rasIcc[i], ras[i], 0, $"Position: {i}");
            }
        }

        [Test()]
        public void TestCMYRasterPrefixes1()
        {
            char[] a = "\u001bDb;y;256;".ToCharArray();
            /*
            i:0 - 27
            i:1 - 68
            i:2 - 98
            i:3 - 59
            i:4 - 121
            i:5 - 59
            i:6 - 50
            i:7 - 53
            i:8 - 54
            i:9 - 59
            */

            Assert.AreEqual(10, a.Length);
            Assert.AreEqual(27, (int)a[0]);
            Assert.AreEqual(68, (int)a[1]);
            Assert.AreEqual(98, (int)a[2]);
            Assert.AreEqual(59, (int)a[3]);
            Assert.AreEqual(121, (int)a[4]);
            Assert.AreEqual(59, (int)a[5]);
            Assert.AreEqual(50, (int)a[6]);
            Assert.AreEqual(53, (int)a[7]);
            Assert.AreEqual(54, (int)a[8]);
            Assert.AreEqual(59, (int)a[9]);
        }

        [Test()]
        public void TestCMYRasterPrefixes2()
        {
            byte[] a = CMYImageData.Get8BitColorPlanePrefix('y');

            Assert.AreEqual(10, a.Length);
            Assert.AreEqual(27, (int)a[0]);
            Assert.AreEqual(68, (int)a[1]);
            Assert.AreEqual(98, (int)a[2]);
            Assert.AreEqual(59, (int)a[3]);
            Assert.AreEqual(121, (int)a[4]);
            Assert.AreEqual(59, (int)a[5]);
            Assert.AreEqual(50, (int)a[6]);
            Assert.AreEqual(53, (int)a[7]);
            Assert.AreEqual(54, (int)a[8]);
            Assert.AreEqual(59, (int)a[9]);
        }

        [Test()]
        public void TestPidaPrefix1()
        {
            byte[] a = CMYImageData.GetPidaPrefix(768*65, 1024*768, 142.4, 112.7, 68.3); // \ESC + "Pida;6;68;113;142" + [\r]  (it goes as "\u001bPida;100.0D * whitePixel / area;yMean;mMean;cMean" followed by \r)

            Assert.AreEqual(18, a.Length);

            Assert.AreEqual(27, (int)a[0]);
            Assert.AreEqual(80, (int)a[1]);
            Assert.AreEqual(105, (int)a[2]);
            Assert.AreEqual(100, (int)a[3]);
            Assert.AreEqual(97, (int)a[4]);
            Assert.AreEqual(59, (int)a[5]);
            Assert.AreEqual(54, (int)a[6]);
            Assert.AreEqual(59, (int)a[7]);
            Assert.AreEqual(54, (int)a[8]);
            Assert.AreEqual(56, (int)a[9]);
            Assert.AreEqual(59, (int)a[10]);
            Assert.AreEqual(49, (int)a[11]);
            Assert.AreEqual(49, (int)a[12]);
            Assert.AreEqual(51, (int)a[13]);
            Assert.AreEqual(59, (int)a[14]);
            Assert.AreEqual(49, (int)a[15]);
            Assert.AreEqual(52, (int)a[16]);
            Assert.AreEqual(50, (int)a[17]);
        }
    }
}

// Original notes from 2018 - Image Quality & preprocessing investigation
/*

- printhead used in dualys is kyocera:
  https://global.kyocera.com/prdct/printing-devices/thermal-printheads/tec/resolution.html
  physical dpi is 304.8 in vertical dimension(height of card - 54mm), this corresponds to 648px for 54mm card height

- Evolis programming guide (found somewhere on the internet): Programming_Guide.pdf
  - describes encoding of the image and printer commands
  - information was found to be consistent with mosaic driver and our previous knowledge

- library we are using (prn_adapter 2.0) seems to be outdated, there is a changelog avaliable at evolis (https://www.evolis.com/sites/default/files/atoms/files/changelog.txt):
		CHANGELOG
		=========
		SDK 2.4 - 26/02/2016
		-----------------
		- Fix bug in "Repository" : Adding files "prn_adapter_2.0.h", "prn_adapter_2.0.lib" and "prn_adapter_2.0.dll" in 32 and 64bits in the project 		"YMCKOSequencerExample/sequencer_lib"
		SDK 2.3 - 01/12/2015 
		-----------------
		- Fix bug in "Tutorial" (c# and VB.Net examples) : wrapping iomem is different for target x86 and x64
		SDK 2.2 - 01/12/2015 
		-----------------
		- Fix bug in the example "PrintStatus" (status "EndOfRibbon" was not correct)
		- test images:
			http://www.bealecorner.org/red/test-patterns/
		- production images:
			\\caofiles.eftdomain.net\cao_layouts\5
 - library was introduced into project in this commit: https://source.eftdomain.net/cao/cardprinters/commit/265e8ec6d23dc098f95d8f8e6842cb9c04e044e8
   - note about previous implementation says it was producing corrupted images (and it was marked obsolete)

 - Base test images:
   - ImageQuality\1016x648_G10_star-chart-sine.png (with variants)
                  1016x648_Koren2003lensTest.jpg
                  corners_cropTest.png (with variants)
		  vert_dpi_test_G10.png 
   - all were resampled to 1016x648 (it did fit 2x on card afterwards), black was capped to (10,10,10) (instead of 0-es, marked as G10):

- overview info on filtering with nice pictures and explanation: https://processing.org/tutorials/pixels/

- PrnAdapterTest does recreate the image after it is being preprocessed by prn_adapter-2.0.dll (supplied by Evolis)
  - the image is converted to YMC(K), 7bit, planes are separated and serialized by columns into byte array
  - our test program does recreate the image out of separated/serialized arrays in order to show how preprocessing changed the original picture

- The analysis of Mosaic source code did reveal usage of sharpening matrix (5x5), all -1 with one value 49 in center.
  - this filter is recreated in the test program for comparison

- Conclusion: prn adapter dll is doing different preprocessing than sharpening filter found in mosaic for PrimacyG2 printer, the effect observed in case of Primacy is standard sharpening using 5x5 matrix, mosaic allows to do 0, 1 or 2 passes (2-pass filtering option is called BOOST). Preprocessing done in prn_adapter dll does not fall clearly into any category, Evolis is referring to it as 'softening'. 

 */
