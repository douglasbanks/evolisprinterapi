﻿using EvolisPrinterApi.ApplicationClasses.Drivers;
using EvolisPrinterApi.Contracts.Models;
using LibUsbDotNet;
using NUnit.Framework;

namespace EvolisPrinterApiTests.ApplicationClasses.Drivers
{

    [TestFixture()]
    public class PrinterResponseTests
    {

        [Test()]
        public void ParseAndValidateTest1() // <- Status: 0(Success), Received: 33, Text: '', Binary: 00000010881E273020901000000D00000000000002000104800000000408000000 (33 bytes in total)
        {
            byte[] rawBuffer = new byte[] { 0x00, 0x00, 0x00,                // string part: empty string, length (LSB first) is 0
                                            0x10,                            // length value (1B) of 16 bytes for 4x4 status
                                            0x88, 0x1E, 0x27, 0x30,          // config part (4 byte), (MSB first) value:  0b 1000 1000 0001 1110 0010 0111 0011 0000
                                            0x20, 0x90, 0x10, 0x00,          // info part (4 byte), (MSB first) value:    0b 0010 0000 1001 0000 0001 0000 0000 0000
                                            0x00, 0x0D, 0x00, 0x00,          // warning part (4 byte), (MSB first) value: 0b 0000 0000 0000 1101 0000 0000 0000 0000
                                            0x00, 0x00, 0x00, 0x00,          // error part (4 byte), (MSB first) value:   0b 0000 0000 0000 0000 0000 0000 0000 0000
                                            0x02, 0x00, 0x01,                // session ID length (1B value of 2 + 2B (MSB first) value with session: #1)
                                            0x04, 0x80, 0x00, 0x00, 0x00,    // extension1: 1B length of 4 + 4 bytes, (MSB first) value: 0b 1000 0000 0000 0000 0000 0000 0000 0000
                                            0x04, 0x08, 0x00, 0x00, 0x00 };  // extension2: 1B length of 4 + 4 bytes, (MSB first) value: 0b 0000 1000 0000 0000 0000 0000 0000 0000
            Assert.AreEqual(33, rawBuffer.Length);

            PrinterResponse pr = new PrinterResponse(rawBuffer, rawBuffer.Length, Error.Success);

            Assert.AreEqual("", pr.Text);
            Assert.AreEqual(1, pr.SessionId);

            // config flags
            Assert.AreEqual(12, pr.ConfigFlags.Count);
            Assert.IsTrue(pr.ConfigFlags.Contains(ConfigStatusFlag.CFG_X01));
            Assert.IsTrue(pr.ConfigFlags.Contains(ConfigStatusFlag.CFG_EXTENSION_1));
            Assert.IsTrue(pr.ConfigFlags.Contains(ConfigStatusFlag.CFG_FLIP));
            Assert.IsTrue(pr.ConfigFlags.Contains(ConfigStatusFlag.CFG_CONTACTLESS));
            Assert.IsTrue(pr.ConfigFlags.Contains(ConfigStatusFlag.CFG_SMART));
            Assert.IsTrue(pr.ConfigFlags.Contains(ConfigStatusFlag.CFG_MAGNETIC));
            Assert.IsTrue(pr.ConfigFlags.Contains(ConfigStatusFlag.CFG_FEED_BY_FEEDER));
            Assert.IsTrue(pr.ConfigFlags.Contains(ConfigStatusFlag.CFG_EXTENDED_RESOLUTION));
            Assert.IsTrue(pr.ConfigFlags.Contains(ConfigStatusFlag.CFG_LCD));
            Assert.IsTrue(pr.ConfigFlags.Contains(ConfigStatusFlag.CFG_LOCK));
            Assert.IsTrue(pr.ConfigFlags.Contains(ConfigStatusFlag.CFG_REJECT_SLOT));
            Assert.IsTrue(pr.ConfigFlags.Contains(ConfigStatusFlag.CFG_IO_EXT));

            // info flags
            Assert.AreEqual(4, pr.InfoFlags.Count);
            Assert.IsTrue(pr.InfoFlags.Contains(InfoStatusFlag.INF_CARD_FEEDER));
            Assert.IsTrue(pr.InfoFlags.Contains(InfoStatusFlag.INF_PRINTER_MASTER));
            Assert.IsTrue(pr.InfoFlags.Contains(InfoStatusFlag.INF_UNKNOWN_RIBBON));
            Assert.IsTrue(pr.InfoFlags.Contains(InfoStatusFlag.INF_READY_FOR_CLEANING));

            // warning flags
            Assert.AreEqual(3, pr.WarningFlags.Count);
            Assert.IsTrue(pr.WarningFlags.Contains(WarningStatusFlag.DEF_RIBBON_ENDED));
            Assert.IsTrue(pr.WarningFlags.Contains(WarningStatusFlag.DEF_PRINTER_LOCKED));
            Assert.IsTrue(pr.WarningFlags.Contains(WarningStatusFlag.DEF_NO_RIBBON));

            // error flags
            Assert.AreEqual(0, pr.ErrorFlags.Count);

            //extension1
            Assert.AreEqual(1, pr.Extension1Flags.Count);
            Assert.IsTrue(pr.Extension1Flags.Contains(Extension1StatusFlag.CFG_EXTENSION_2));

            //extension2
            Assert.AreEqual(1, pr.Extension2Flags.Count);
            Assert.IsTrue(pr.Extension2Flags.Contains(Extension2StatusFlag.INF_X08_PRINTER_UNLOCKED));
        }


        [Test()]
        public void ParseAndValidateTest2() // <- 0(Success), Received: 34, Text: D, Binary: 4400010010881E273000901000010D00000000000002000004800000000408000000 (34 bytes in total)
        {
            byte[] rawBuffer = new byte[] { 0x44, 0x00, 0x01, 0x00,          // string part: "D" string, length (LSB first) is 1
                                            0x10,                            // length value (1B) of 16 bytes for 4x4 status
                                            0x88, 0x1E, 0x27, 0x30,          // config part (4 byte), (MSB first) value:  0b 1000 1000 0001 1110 0010 0111 0011 0000
                                            0x00, 0x90, 0x10, 0x00,          // info part (4 byte), (MSB first) value:    0b 0000 0000 1001 0000 0001 0000 0000 0000
                                            0x01, 0x0D, 0x00, 0x00,          // warning part (4 byte), (MSB first) value: 0b 0000 0001 0000 1101 0000 0000 0000 0000
                                            0x00, 0x00, 0x00, 0x00,          // error part (4 byte), (MSB first) value:   0b 0000 0000 0000 0000 0000 0000 0000 0000
                                            0x02, 0x00, 0x00,                // session ID length (1B value of 2 + 2B (MSB first) value with session: #0)
                                            0x04, 0x80, 0x00, 0x00, 0x00,    // extension1: 1B length of 4 + 4 bytes, (MSB first) value: 0b 1000 0000 0000 0000 0000 0000 0000 0000
                                            0x04, 0x08, 0x00, 0x00, 0x00 };  // extension2: 1B length of 4 + 4 bytes, (MSB first) value: 0b 0000 1000 0000 0000 0000 0000 0000 0000
            Assert.AreEqual(34, rawBuffer.Length);

            PrinterResponse pr = new PrinterResponse(rawBuffer, rawBuffer.Length, Error.Success);

            Assert.AreEqual("D", pr.Text);
            Assert.AreEqual(0, pr.SessionId);

            // config flags
            Assert.AreEqual(12, pr.ConfigFlags.Count);
            Assert.IsTrue(pr.ConfigFlags.Contains(ConfigStatusFlag.CFG_X01));
            Assert.IsTrue(pr.ConfigFlags.Contains(ConfigStatusFlag.CFG_EXTENSION_1));
            Assert.IsTrue(pr.ConfigFlags.Contains(ConfigStatusFlag.CFG_FLIP));
            Assert.IsTrue(pr.ConfigFlags.Contains(ConfigStatusFlag.CFG_CONTACTLESS));
            Assert.IsTrue(pr.ConfigFlags.Contains(ConfigStatusFlag.CFG_SMART));
            Assert.IsTrue(pr.ConfigFlags.Contains(ConfigStatusFlag.CFG_MAGNETIC));
            Assert.IsTrue(pr.ConfigFlags.Contains(ConfigStatusFlag.CFG_FEED_BY_FEEDER));
            Assert.IsTrue(pr.ConfigFlags.Contains(ConfigStatusFlag.CFG_EXTENDED_RESOLUTION));
            Assert.IsTrue(pr.ConfigFlags.Contains(ConfigStatusFlag.CFG_LCD));
            Assert.IsTrue(pr.ConfigFlags.Contains(ConfigStatusFlag.CFG_LOCK));
            Assert.IsTrue(pr.ConfigFlags.Contains(ConfigStatusFlag.CFG_REJECT_SLOT));
            Assert.IsTrue(pr.ConfigFlags.Contains(ConfigStatusFlag.CFG_IO_EXT));

            // info flags
            Assert.AreEqual(3, pr.InfoFlags.Count);
            Assert.IsTrue(pr.InfoFlags.Contains(InfoStatusFlag.INF_PRINTER_MASTER));
            Assert.IsTrue(pr.InfoFlags.Contains(InfoStatusFlag.INF_UNKNOWN_RIBBON));
            Assert.IsTrue(pr.InfoFlags.Contains(InfoStatusFlag.INF_READY_FOR_CLEANING));

            // warning flags
            Assert.AreEqual(4, pr.WarningFlags.Count);
            Assert.IsTrue(pr.WarningFlags.Contains(WarningStatusFlag.DEF_RIBBON_ENDED));
            Assert.IsTrue(pr.WarningFlags.Contains(WarningStatusFlag.DEF_PRINTER_LOCKED));
            Assert.IsTrue(pr.WarningFlags.Contains(WarningStatusFlag.DEF_NO_RIBBON));
            Assert.IsTrue(pr.WarningFlags.Contains(WarningStatusFlag.DEF_FEEDER_EMPTY));

            // error flags
            Assert.AreEqual(0, pr.ErrorFlags.Count);

            //extension1
            Assert.AreEqual(1, pr.Extension1Flags.Count);
            Assert.IsTrue(pr.Extension1Flags.Contains(Extension1StatusFlag.CFG_EXTENSION_2));

            //extension2
            Assert.AreEqual(1, pr.Extension2Flags.Count);
            Assert.IsTrue(pr.Extension2Flags.Contains(Extension2StatusFlag.INF_X08_PRINTER_UNLOCKED));
        }

        [Test()]
        public void ParseAndValidateTest3() // <- 0(Success), Received: 35, Text: 'OK', Binary: 4F4B00020010881E273000901000010D00000000000002000004800000000408000000 (35 bytes in total)
        {
            byte[] rawBuffer = new byte[] { 0x4F, 0x4B, 0x00, 0x02, 0x00,    // string part: "OK" string, length (LSB first) is 2
                                            0x10,                            // length value (1B) of 16 bytes for 4x4 status
                                            0x88, 0x1E, 0x27, 0x30,          // config part (4 byte), (MSB first) value:  0b 1000 1000 0001 1110 0010 0111 0011 0000
                                            0x00, 0x90, 0x10, 0x00,          // info part (4 byte), (MSB first) value:    0b 0000 0000 1001 0000 0001 0000 0000 0000
                                            0x01, 0x0D, 0x00, 0x00,          // warning part (4 byte), (MSB first) value: 0b 0000 0001 0000 1101 0000 0000 0000 0000
                                            0x00, 0x00, 0x00, 0x00,          // error part (4 byte), (MSB first) value:   0b 0000 0000 0000 0000 0000 0000 0000 0000
                                            0x02, 0x00, 0x00,                // session ID length (1B value of 2 + 2B (MSB first) value with session: #0)
                                            0x04, 0x80, 0x00, 0x00, 0x00,    // extension1: 1B length of 4 + 4 bytes, (MSB first) value: 0b 1000 0000 0000 0000 0000 0000 0000 0000
                                            0x04, 0x08, 0x00, 0x00, 0x00 };  // extension2: 1B length of 4 + 4 bytes, (MSB first) value: 0b 0000 1000 0000 0000 0000 0000 0000 0000
            Assert.AreEqual(35, rawBuffer.Length);

            PrinterResponse pr = new PrinterResponse(rawBuffer, rawBuffer.Length, Error.Success);

            Assert.AreEqual("OK", pr.Text);
            Assert.AreEqual(0, pr.SessionId);

            // config flags
            Assert.AreEqual(12, pr.ConfigFlags.Count);
            Assert.IsTrue(pr.ConfigFlags.Contains(ConfigStatusFlag.CFG_X01));
            Assert.IsTrue(pr.ConfigFlags.Contains(ConfigStatusFlag.CFG_EXTENSION_1));
            Assert.IsTrue(pr.ConfigFlags.Contains(ConfigStatusFlag.CFG_FLIP));
            Assert.IsTrue(pr.ConfigFlags.Contains(ConfigStatusFlag.CFG_CONTACTLESS));
            Assert.IsTrue(pr.ConfigFlags.Contains(ConfigStatusFlag.CFG_SMART));
            Assert.IsTrue(pr.ConfigFlags.Contains(ConfigStatusFlag.CFG_MAGNETIC));
            Assert.IsTrue(pr.ConfigFlags.Contains(ConfigStatusFlag.CFG_FEED_BY_FEEDER));
            Assert.IsTrue(pr.ConfigFlags.Contains(ConfigStatusFlag.CFG_EXTENDED_RESOLUTION));
            Assert.IsTrue(pr.ConfigFlags.Contains(ConfigStatusFlag.CFG_LCD));
            Assert.IsTrue(pr.ConfigFlags.Contains(ConfigStatusFlag.CFG_LOCK));
            Assert.IsTrue(pr.ConfigFlags.Contains(ConfigStatusFlag.CFG_REJECT_SLOT));
            Assert.IsTrue(pr.ConfigFlags.Contains(ConfigStatusFlag.CFG_IO_EXT));

            // info flags
            Assert.AreEqual(3, pr.InfoFlags.Count);
            Assert.IsTrue(pr.InfoFlags.Contains(InfoStatusFlag.INF_PRINTER_MASTER));
            Assert.IsTrue(pr.InfoFlags.Contains(InfoStatusFlag.INF_UNKNOWN_RIBBON));
            Assert.IsTrue(pr.InfoFlags.Contains(InfoStatusFlag.INF_READY_FOR_CLEANING));

            // warning flags
            Assert.AreEqual(4, pr.WarningFlags.Count);
            Assert.IsTrue(pr.WarningFlags.Contains(WarningStatusFlag.DEF_RIBBON_ENDED));
            Assert.IsTrue(pr.WarningFlags.Contains(WarningStatusFlag.DEF_PRINTER_LOCKED));
            Assert.IsTrue(pr.WarningFlags.Contains(WarningStatusFlag.DEF_NO_RIBBON));
            Assert.IsTrue(pr.WarningFlags.Contains(WarningStatusFlag.DEF_FEEDER_EMPTY));

            // error flags
            Assert.AreEqual(0, pr.ErrorFlags.Count);

            //extension1
            Assert.AreEqual(1, pr.Extension1Flags.Count);
            Assert.IsTrue(pr.Extension1Flags.Contains(Extension1StatusFlag.CFG_EXTENSION_2));

            //extension2
            Assert.AreEqual(1, pr.Extension2Flags.Count);
            Assert.IsTrue(pr.Extension2Flags.Contains(Extension2StatusFlag.INF_X08_PRINTER_UNLOCKED));
        }

        [Test()]
        public void ParseAndValidateTest4() // <- 0(Success), Received: 37, Text: 1814, Binary: 3138313400040010881E273000901000010D00000000000002000004800000000408000000 (37 bytes in total)
        {
            byte[] rawBuffer = new byte[] { 0x31, 0x38, 0x31, 0x34, 0x00, 0x04, 0x00, // string part: "1814" string, length (LSB first) is 4
                                            0x10,                            // length value (1B) of 16 bytes for 4x4 status
                                            0x88, 0x1E, 0x27, 0x30,          // config part (4 byte), (MSB first) value:  0b 1000 1000 0001 1110 0010 0111 0011 0000
                                            0x00, 0x90, 0x10, 0x00,          // info part (4 byte), (MSB first) value:    0b 0000 0000 1001 0000 0001 0000 0000 0000
                                            0x01, 0x0D, 0x00, 0x00,          // warning part (4 byte), (MSB first) value: 0b 0000 0001 0000 1101 0000 0000 0000 0000
                                            0x00, 0x00, 0x00, 0x00,          // error part (4 byte), (MSB first) value:   0b 0000 0000 0000 0000 0000 0000 0000 0000
                                            0x02, 0x00, 0x00,                // session ID length (1B value of 2 + 2B (MSB first) value with session: #0)
                                            0x04, 0x80, 0x00, 0x00, 0x00,    // extension1: 1B length of 4 + 4 bytes, (MSB first) value: 0b 1000 0000 0000 0000 0000 0000 0000 0000
                                            0x04, 0x08, 0x00, 0x00, 0x00 };  // extension2: 1B length of 4 + 4 bytes, (MSB first) value: 0b 0000 1000 0000 0000 0000 0000 0000 0000
            Assert.AreEqual(37, rawBuffer.Length);

            PrinterResponse pr = new PrinterResponse(rawBuffer, rawBuffer.Length, Error.Success);

            Assert.AreEqual("1814", pr.Text);
            Assert.AreEqual(0, pr.SessionId);

            // config flags
            Assert.AreEqual(12, pr.ConfigFlags.Count);
            Assert.IsTrue(pr.ConfigFlags.Contains(ConfigStatusFlag.CFG_X01));
            Assert.IsTrue(pr.ConfigFlags.Contains(ConfigStatusFlag.CFG_EXTENSION_1));
            Assert.IsTrue(pr.ConfigFlags.Contains(ConfigStatusFlag.CFG_FLIP));
            Assert.IsTrue(pr.ConfigFlags.Contains(ConfigStatusFlag.CFG_CONTACTLESS));
            Assert.IsTrue(pr.ConfigFlags.Contains(ConfigStatusFlag.CFG_SMART));
            Assert.IsTrue(pr.ConfigFlags.Contains(ConfigStatusFlag.CFG_MAGNETIC));
            Assert.IsTrue(pr.ConfigFlags.Contains(ConfigStatusFlag.CFG_FEED_BY_FEEDER));
            Assert.IsTrue(pr.ConfigFlags.Contains(ConfigStatusFlag.CFG_EXTENDED_RESOLUTION));
            Assert.IsTrue(pr.ConfigFlags.Contains(ConfigStatusFlag.CFG_LCD));
            Assert.IsTrue(pr.ConfigFlags.Contains(ConfigStatusFlag.CFG_LOCK));
            Assert.IsTrue(pr.ConfigFlags.Contains(ConfigStatusFlag.CFG_REJECT_SLOT));
            Assert.IsTrue(pr.ConfigFlags.Contains(ConfigStatusFlag.CFG_IO_EXT));

            // info flags
            Assert.AreEqual(3, pr.InfoFlags.Count);
            Assert.IsTrue(pr.InfoFlags.Contains(InfoStatusFlag.INF_PRINTER_MASTER));
            Assert.IsTrue(pr.InfoFlags.Contains(InfoStatusFlag.INF_UNKNOWN_RIBBON));
            Assert.IsTrue(pr.InfoFlags.Contains(InfoStatusFlag.INF_READY_FOR_CLEANING));

            // warning flags
            Assert.AreEqual(4, pr.WarningFlags.Count);
            Assert.IsTrue(pr.WarningFlags.Contains(WarningStatusFlag.DEF_RIBBON_ENDED));
            Assert.IsTrue(pr.WarningFlags.Contains(WarningStatusFlag.DEF_PRINTER_LOCKED));
            Assert.IsTrue(pr.WarningFlags.Contains(WarningStatusFlag.DEF_NO_RIBBON));
            Assert.IsTrue(pr.WarningFlags.Contains(WarningStatusFlag.DEF_FEEDER_EMPTY));

            // error flags
            Assert.AreEqual(0, pr.ErrorFlags.Count);

            //extension1
            Assert.AreEqual(1, pr.Extension1Flags.Count);
            Assert.IsTrue(pr.Extension1Flags.Contains(Extension1StatusFlag.CFG_EXTENSION_2));

            //extension2
            Assert.AreEqual(1, pr.Extension2Flags.Count);
            Assert.IsTrue(pr.Extension2Flags.Contains(Extension2StatusFlag.INF_X08_PRINTER_UNLOCKED));
        }

        [Test()]
        public void ParseAndValidateTest5() // <- 0(Success), Received: 47, Text: 'ERROR SECURITY', Binary: 4552524F52205345435552495459000E0010881E273020901000000D00000000000002000104800000000408000000 (47 bytes in total)
        {
            byte[] rawBuffer = new byte[] { 0x45, 0x52, 0x52, 0x4F, 0x52, 0x20, 0x53, 0x45, 0x43, 0x55, 0x52, 0x49, 0x54, 0x59, 0x00, 0x0E, 0x00, // string part: "ERROR SECURITY" string, length (LSB first) is 14
                                            0x10,                            // length value (1B) of 16 bytes for 4x4 status
                                            0x88, 0x1E, 0x27, 0x30,          // config part (4 byte), (MSB first) value:  0b 1000 1000 0001 1110 0010 0111 0011 0000
                                            0x20, 0x90, 0x10, 0x00,          // info part (4 byte), (MSB first) value:    0b 0010 0000 1001 0000 0001 0000 0000 0000
                                            0x00, 0x0D, 0x00, 0x00,          // warning part (4 byte), (MSB first) value: 0b 0000 0000 0000 1101 0000 0000 0000 0000
                                            0x00, 0x00, 0x00, 0x00,          // error part (4 byte), (MSB first) value:   0b 0000 0000 0000 0000 0000 0000 0000 0000
                                            0x02, 0x00, 0x01,                // session ID length (1B value of 2 + 2B (MSB first) value with session: #0)
                                            0x04, 0x80, 0x00, 0x00, 0x00,    // extension1: 1B length of 4 + 4 bytes, (MSB first) value: 0b 1000 0000 0000 0000 0000 0000 0000 0000
                                            0x04, 0x08, 0x00, 0x00, 0x00 };  // extension2: 1B length of 4 + 4 bytes, (MSB first) value: 0b 0000 1000 0000 0000 0000 0000 0000 0000
            Assert.AreEqual(47, rawBuffer.Length);

            PrinterResponse pr = new PrinterResponse(rawBuffer, rawBuffer.Length, Error.Success);

            Assert.AreEqual("ERROR SECURITY", pr.Text);
            Assert.AreEqual(1, pr.SessionId);

            // config flags
            Assert.AreEqual(12, pr.ConfigFlags.Count);
            Assert.IsTrue(pr.ConfigFlags.Contains(ConfigStatusFlag.CFG_X01));
            Assert.IsTrue(pr.ConfigFlags.Contains(ConfigStatusFlag.CFG_EXTENSION_1));
            Assert.IsTrue(pr.ConfigFlags.Contains(ConfigStatusFlag.CFG_FLIP));
            Assert.IsTrue(pr.ConfigFlags.Contains(ConfigStatusFlag.CFG_CONTACTLESS));
            Assert.IsTrue(pr.ConfigFlags.Contains(ConfigStatusFlag.CFG_SMART));
            Assert.IsTrue(pr.ConfigFlags.Contains(ConfigStatusFlag.CFG_MAGNETIC));
            Assert.IsTrue(pr.ConfigFlags.Contains(ConfigStatusFlag.CFG_FEED_BY_FEEDER));
            Assert.IsTrue(pr.ConfigFlags.Contains(ConfigStatusFlag.CFG_EXTENDED_RESOLUTION));
            Assert.IsTrue(pr.ConfigFlags.Contains(ConfigStatusFlag.CFG_LCD));
            Assert.IsTrue(pr.ConfigFlags.Contains(ConfigStatusFlag.CFG_LOCK));
            Assert.IsTrue(pr.ConfigFlags.Contains(ConfigStatusFlag.CFG_REJECT_SLOT));
            Assert.IsTrue(pr.ConfigFlags.Contains(ConfigStatusFlag.CFG_IO_EXT));

            // info flags
            Assert.AreEqual(4, pr.InfoFlags.Count);
            Assert.IsTrue(pr.InfoFlags.Contains(InfoStatusFlag.INF_CARD_FEEDER));
            Assert.IsTrue(pr.InfoFlags.Contains(InfoStatusFlag.INF_PRINTER_MASTER));
            Assert.IsTrue(pr.InfoFlags.Contains(InfoStatusFlag.INF_UNKNOWN_RIBBON));
            Assert.IsTrue(pr.InfoFlags.Contains(InfoStatusFlag.INF_READY_FOR_CLEANING));

            // warning flags
            Assert.AreEqual(3, pr.WarningFlags.Count);
            Assert.IsTrue(pr.WarningFlags.Contains(WarningStatusFlag.DEF_RIBBON_ENDED));
            Assert.IsTrue(pr.WarningFlags.Contains(WarningStatusFlag.DEF_PRINTER_LOCKED));
            Assert.IsTrue(pr.WarningFlags.Contains(WarningStatusFlag.DEF_NO_RIBBON));

            // error flags
            Assert.AreEqual(0, pr.ErrorFlags.Count);

            //extension1
            Assert.AreEqual(1, pr.Extension1Flags.Count);
            Assert.IsTrue(pr.Extension1Flags.Contains(Extension1StatusFlag.CFG_EXTENSION_2));

            //extension2
            Assert.AreEqual(1, pr.Extension2Flags.Count);
            Assert.IsTrue(pr.Extension2Flags.Contains(Extension2StatusFlag.INF_X08_PRINTER_UNLOCKED));
        }

    }
}