﻿using System;
using System.Threading;
using EvolisPrinterApi.ApplicationClasses.CSCalibration;
using NUnit.Framework;

namespace EvolisPrinterApiTests.ApplicationClasses.CSCalibration
{
    [TestFixture]
    public class LinearCalibrationTests
    {

        [Test()]
        public void CalibrationStrategyBasicTest1()
        {
            ICalibrationStrategy cal = new LinearCalibration(null);
            Assert.AreEqual(CalibrationStatus.Init, cal.Status);
            cal.Initialize(420, 370, 470, TimeSpan.MaxValue, true);

            do
            {
                Assert.AreEqual(CalibrationStatus.Sampling, cal.Status);
                cal.SamplingResult(cal.NextSamplingPosition, true);

            } while (cal.Status == CalibrationStatus.Sampling);

            Assert.AreEqual(CalibrationStatus.Result, cal.Status);
        }

        // simulate that wrong default position at 420 (or other) gets replaced by suggestion of new default between X-Y
        [Test()]
        [TestCase(370, 399, 420)]
        [TestCase(440, 465, 420)]
        [TestCase(370, 399, 380)]
        [TestCase(440, 465, 380)]
        [TestCase(370, 399, 467)]
        [TestCase(440, 465, 467)]
        [TestCase(370, 399, 420)]
        [TestCase(440, 465, 420)]
        [TestCase(370, 399, 380)]
        [TestCase(440, 465, 380)]
        [TestCase(370, 399, 467)]
        [TestCase(440, 465, 467)]
        public void CalibrationStrategySamplingTest2(int chipFrom, int chipTo, int defaultOffset)
        {
            ICalibrationStrategy cal = new LinearCalibration(null);

            int lastSuggestion = -1;

            Assert.AreEqual(CalibrationStatus.Init, cal.Status);
            cal.Initialize(defaultOffset, 370, 470, TimeSpan.MaxValue, true);
            Assert.AreEqual(CalibrationStatus.Sampling, cal.Status);

            int sampleCount = 0;
            while (cal.Status == CalibrationStatus.Sampling)
            {
                if (cal.NextSamplingPosition >= chipFrom && cal.NextSamplingPosition <= chipTo)
                {
                    cal.SamplingResult(cal.NextSamplingPosition, true);
                }
                else
                {
                    cal.SamplingResult(cal.NextSamplingPosition, false);
                }
                ++sampleCount;
            }

            Assert.Greater(sampleCount, 0);
            Assert.AreEqual(CalibrationStatus.Result, cal.Status);

            if (cal.NewDefaultPositionRecommended)
            {
                lastSuggestion = cal.NewDefaultPosition;
            }

            Assert.AreNotEqual(-1, lastSuggestion);
            Assert.GreaterOrEqual(lastSuggestion, chipFrom, "calibrated position between 370..399 expected");
            Assert.LessOrEqual(lastSuggestion, chipTo, "calibrated position between 370..399 expected");
        }

        [Test]
        public void CalibrationStrategyTimeoutFail1()
        {
            ICalibrationStrategy cal = new LinearCalibration(null);
            Assert.AreEqual(CalibrationStatus.Init, cal.Status);
            cal.Initialize(420, 370, 470, TimeSpan.FromSeconds(1.0), true);
            Assert.AreEqual(CalibrationStatus.Sampling, cal.Status);

            while (cal.Status == CalibrationStatus.Sampling)
            {
                if (cal.NextSamplingPosition >= 440 && cal.NextSamplingPosition <= 465)
                {
                    cal.SamplingResult(cal.NextSamplingPosition, true);
                }
                else
                {
                    cal.SamplingResult(cal.NextSamplingPosition, false);
                }
                Thread.Sleep(200);
            }

            // 200ms per test should not be enough to sample 370-470 even at step 10 in 1s.
            Assert.AreEqual(CalibrationStatus.Failed, cal.Status);
        }
    }
}
