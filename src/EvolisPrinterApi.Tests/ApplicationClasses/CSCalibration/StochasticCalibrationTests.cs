﻿using System;
using System.Collections.Generic;
using EvolisPrinterApi.ApplicationClasses.CSCalibration;
using NUnit.Framework;

namespace EvolisPrinterApiTests.ApplicationClasses.CSCalibration
{
    [TestFixture()]
    public class StochasticCalibrationTests
    {
        //verify basic advancement through calibration stages init->sampling->result
        [Test()]
        public void CalibrationStrategyBasicTest1()
        {
            StochasticCalibration cal = new StochasticCalibration(null, true);
            Assert.AreEqual(CalibrationStatus.Init, cal.Status);
            cal.Initialize(420, 370, 470, TimeSpan.MaxValue, true);
            Assert.AreEqual(CalibrationStatus.Sampling ,cal.Status);
            cal.SamplingResult(cal.NextSamplingPosition, true);
            Assert.AreEqual(CalibrationStatus.Result, cal.Status);
        }

        // verify that each provided sample is different from previous ones
        [Test()]
        public void CalibrationStrategySamplingTest1()
        {
            HashSet<int> previousPositions = new HashSet<int>();
            StochasticCalibration cal = new StochasticCalibration(null, true);
            Assert.AreEqual(CalibrationStatus.Init, cal.Status);
            cal.Initialize(420, 370, 470, TimeSpan.MaxValue, false);
            Assert.AreEqual(CalibrationStatus.Sampling, cal.Status);

            for (int i = 1; i < 90; ++i)
            {
                previousPositions.Add(cal.NextSamplingPosition);
                cal.SamplingResult(cal.NextSamplingPosition, false);
                Assert.AreEqual(CalibrationStatus.Sampling, cal.Status);
                Assert.IsFalse(previousPositions.Contains(cal.NextSamplingPosition));
            }
        }

        // simulate that wrong default position at 420 gets replaced by suggestion of new default between X-Y
        [Test()]
        [TestCase(370, 399, 420, true)]
        [TestCase(440, 465, 420, true)]
        [TestCase(370, 399, 380, false)]
        [TestCase(440, 465, 380, false)]
        [TestCase(370, 399, 467, false)]
        [TestCase(440, 465, 467, false)]
        [TestCase(370, 399, 420, false)]
        [TestCase(440, 465, 420, false)]
        [TestCase(370, 399, 380, false)]
        [TestCase(440, 465, 380, false)]
        [TestCase(370, 399, 467, false)]
        [TestCase(440, 465, 467, false)]
        public void CalibrationStrategySamplingTest2(int ChipFrom, int ChipTo, int defaultOffset, bool resetHistogram)
        {
            StochasticCalibration cal = new StochasticCalibration(null, resetHistogram);

            int lastSuggestion = -1;

            for (int i = 0; i < 100; i++)
            {

                Assert.AreEqual(CalibrationStatus.Init, cal.Status);
                cal.Initialize(defaultOffset, 370, 470, TimeSpan.MaxValue, true);
                Assert.AreEqual(CalibrationStatus.Sampling, cal.Status);

                int samplecount = 0;
                while (cal.Status == CalibrationStatus.Sampling)
                {
                    if (cal.NextSamplingPosition >= ChipFrom && cal.NextSamplingPosition <= ChipTo)
                    {
                        cal.SamplingResult(cal.NextSamplingPosition, true);
                    }
                    else
                    {
                        cal.SamplingResult(cal.NextSamplingPosition, false);
                    }
                    ++samplecount;
                }

                Assert.Greater(samplecount, 0);
                Assert.AreEqual(CalibrationStatus.Result, cal.Status);

                if (cal.NewDefaultPositionRecommended)
                {
                    lastSuggestion = cal.NewDefaultPosition;
                    break;
                }

                cal = new StochasticCalibration(null); // reset strategy, but history is kept
            }

            Assert.AreNotEqual(-1, lastSuggestion);
            Assert.GreaterOrEqual(lastSuggestion, ChipFrom, $"calibrated position between {ChipFrom}..{ChipTo} expected");
            Assert.LessOrEqual(lastSuggestion, ChipTo, $"calibrated position between {ChipFrom}..{ChipTo} expected");
        }

    }
}
