﻿namespace EvolisPrinterApi.Exceptions
{
    public class PrintJobInProgressException : PrinterApiException
    {
        public PrintJobInProgressException(string jobId) : base(jobId, $"Print job {jobId} is already in progress")
        {
        }
    }
}