﻿namespace EvolisPrinterApi.Exceptions
{
    public class FailedToStartProcessException : PrinterApiException
    {
        public string ProcessName { get; }

        public FailedToStartProcessException(string processName) : base(string.Empty, $"{processName}: failed to start process")
        {
            ProcessName = processName;
        }
    }
}