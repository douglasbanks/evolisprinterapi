﻿namespace EvolisPrinterApi.Exceptions
{
    public class NoExternalProcessingRequiredException : PrinterApiException
    {
        public NoExternalProcessingRequiredException(string jobId) : base(jobId,$"No external processing required for job {jobId} at the moment")
        {
        }
    }
}