﻿using System;
using LibUsbDotNet;

namespace EvolisPrinterApi.Exceptions
{
    public class RepeatedUsbWriteException : Exception
    {
        public Error LastUsbError { get; }

        public int Retries { get; }

        public RepeatedUsbWriteException(Error lastUsbError, int retries) : base($"Failed to write to USB bus (retries={retries}): {lastUsbError:G} ({lastUsbError:D})")
        {
            LastUsbError = lastUsbError;
            Retries = retries;
        }
    }
}