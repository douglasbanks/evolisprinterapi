﻿using System;

namespace EvolisPrinterApi.Exceptions
{
    public class MultipleDriverInstantiationException : Exception
    {
        public MultipleDriverInstantiationException() : base("Only one instance of printer driver class is allowed.")
        {
        }
    }
}