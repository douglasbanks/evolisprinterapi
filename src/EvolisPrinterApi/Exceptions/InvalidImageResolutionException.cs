﻿using System;
using PrinterApi.Contracts.Models.Dataset;

namespace EvolisPrinterApi.Exceptions
{
    public class InvalidImageResolutionException : Exception
    {
        public int Height { get; }

        public int Width { get; }

        public int ExpectedHeight { get; }

        public int ExpectedWidth { get; }

        public PrintImageType PanelType { get; }

        public InvalidImageResolutionException(int height, int width, int expectedHeight, int expectedWidth, PrintImageType panelType)
            : base($"Panel '{panelType:G}': Expected image resolution '{expectedHeight}x{expectedWidth} but got '{height}x{width}' instead")
        {
            Height = height;
            Width = width;
            ExpectedHeight = expectedHeight;
            ExpectedWidth = expectedWidth;
            PanelType = panelType;
        }
    }
}