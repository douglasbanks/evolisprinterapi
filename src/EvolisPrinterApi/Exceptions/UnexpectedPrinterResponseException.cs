﻿using System;
using EvolisPrinterApi.ApplicationClasses.Drivers;

namespace EvolisPrinterApi.Exceptions
{
    public class UnexpectedPrinterResponseException : Exception
    {
        public PrinterResponse PrinterResponse { get; }

        public UnexpectedPrinterResponseException(string message, PrinterResponse printerResponse) : base(message)
        {
            PrinterResponse = printerResponse;
        }
    }
}