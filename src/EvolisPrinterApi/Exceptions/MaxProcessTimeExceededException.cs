﻿using System;

namespace EvolisPrinterApi.Exceptions
{
    public class MaxProcessTimeExceededException : PrinterApiException
    {
        public string ProcessName { get; }

        public TimeSpan MaxExecutionTime { get; }

        public MaxProcessTimeExceededException(string processName, TimeSpan maxExecutionTime) : base(string.Empty, $"{processName}: Maximum execution time ({maxExecutionTime}) exceeded")
        {
            ProcessName = processName;
            MaxExecutionTime = maxExecutionTime;
        }
    }
}