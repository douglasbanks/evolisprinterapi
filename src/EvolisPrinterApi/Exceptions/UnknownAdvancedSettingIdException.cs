﻿using System;

namespace EvolisPrinterApi.Exceptions
{
    public class UnknownAdvancedSettingIdException : Exception
    {
        public int Id { get; }

        public UnknownAdvancedSettingIdException(int id) : base($"{id}: Unknown advanced setting id")
        {
            Id = id;
        }
    }
}