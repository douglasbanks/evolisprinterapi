﻿using System;

namespace EvolisPrinterApi.Exceptions
{
    public class UnableToProcessPrintImageException : Exception
    {
        public string Description { get; }

        public UnableToProcessPrintImageException(Exception exception, string description) : base(description, exception)
        {
            Description = description;
        }
    }
}