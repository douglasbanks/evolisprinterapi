﻿using System;

namespace EvolisPrinterApi.Exceptions
{
    public class MissingPrinterModelException : Exception
    {
        public MissingPrinterModelException() : base("PrinterModel is missing or empty in the application configuration")
        {
        }
    }
}