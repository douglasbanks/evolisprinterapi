﻿using System;

namespace EvolisPrinterApi.Exceptions
{
    public class ContrastOrLuminosityPrinterResponseTooShortException : Exception
    {
        public string Command { get; }

        public ContrastOrLuminosityPrinterResponseTooShortException(string command) : base($"{command}: Expected at least 4 whitespace-separated numerical values for contrast/luminosity")
        {
            Command = command;
        }
    }
}