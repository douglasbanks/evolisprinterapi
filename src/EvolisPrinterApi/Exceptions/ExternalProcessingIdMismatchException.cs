﻿namespace EvolisPrinterApi.Exceptions
{
    public class ExternalProcessingIdMismatchException : PrinterApiException
    {
        public string ExpectedJobId { get; }

        public string ProcessingId { get; }

        public string ExpectedProcessingId { get; }

        public ExternalProcessingIdMismatchException(string jobId, string expectedJobId, string processingId, string expectedProcessingId)
            : base(jobId, $"External processing identification mismatch (JobId={jobId},ProcessingId={processingId},ExpectedJobId={expectedJobId},ExpectedProcessingId={expectedProcessingId})")
        {
            ExpectedJobId = expectedJobId;
            ProcessingId = processingId;
            ExpectedProcessingId = expectedProcessingId;
        }
    }
}