﻿using System;

namespace EvolisPrinterApi.Exceptions
{
    public class NumberExpectedPrinterResponseException : Exception
    {
        public string Method { get; }

        public string Response { get; }

        public NumberExpectedPrinterResponseException(string method, string response) : base($"{method}: Expected number but got '{response}'")
        {
            Method = method;
            Response = response;
        }
    }
}