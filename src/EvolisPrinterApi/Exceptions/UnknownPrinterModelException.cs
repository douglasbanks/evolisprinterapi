﻿using System;

namespace EvolisPrinterApi.Exceptions
{
    public class UnknownPrinterModelException : Exception
    {
        public string PrinterModel { get; }

        public string[] SupportedModels { get; }

        public UnknownPrinterModelException(string printerModel, string[] supportedModels)
            : base($"PrinterModel '{printerModel}' is not supported, use one of: {string.Join(',', supportedModels)}")
        {
            PrinterModel = printerModel;
            SupportedModels = supportedModels;
        }
    }
}