﻿using System;

namespace EvolisPrinterApi.Exceptions
{
    public class UnableToConnectToPrinterException : Exception
    {
        public ushort VendorId { get; }

        public ushort ProductId { get; }

        public UnableToConnectToPrinterException(ushort vendorId, ushort productId, Exception innerException = null)
            : base($"Unable to initialize USB connection to printer '{vendorId:X4}:{productId:X4}'{(innerException != null ? $": {innerException.Message}" : "")}", innerException)
        {
            VendorId = vendorId;
            ProductId = productId;
        }
    }
}