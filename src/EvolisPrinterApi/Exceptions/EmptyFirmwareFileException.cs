﻿using System;

namespace EvolisPrinterApi.Exceptions
{
    public class EmptyFirmwareFileException : Exception
    {
        public EmptyFirmwareFileException() : base("Empty firmware file uploaded")
        {
        }
    }
}