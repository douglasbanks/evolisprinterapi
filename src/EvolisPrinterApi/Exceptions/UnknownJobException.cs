﻿namespace EvolisPrinterApi.Exceptions
{
    public class UnknownJobException : PrinterApiException
    {
        public UnknownJobException(string jobId) : base(jobId, $"No job with given Id is being processed by this instance.")
        {
        }
    }
}