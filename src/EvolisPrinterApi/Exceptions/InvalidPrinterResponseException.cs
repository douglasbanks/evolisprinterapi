﻿using System;

namespace EvolisPrinterApi.Exceptions
{
    public class InvalidPrinterResponseException : Exception
    {
        public InvalidPrinterResponseException(string message) : base(message)
        {
        }
    }
}