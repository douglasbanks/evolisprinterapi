﻿using System;

namespace EvolisPrinterApi.Exceptions
{
    public class NoSmartcardReaderFoundException : Exception
    {
        public string Wildcard { get; }

        public string[] DetectedReaders { get; }

        public NoSmartcardReaderFoundException(string wildcard, string[] detectedReaders) : base($"No smartcard reader with name matching wildcard '{wildcard}' found")
        {
            Wildcard = wildcard;
            DetectedReaders = detectedReaders;
        }
    }
}