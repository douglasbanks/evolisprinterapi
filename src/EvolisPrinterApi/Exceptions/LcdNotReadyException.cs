﻿using System;

namespace EvolisPrinterApi.Exceptions
{
    public class LcdNotReadyException : Exception
    {
        public LcdNotReadyException() : base("LCD Display is not ready")
        {
        }
    }
}
