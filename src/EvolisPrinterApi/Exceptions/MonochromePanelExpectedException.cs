﻿using System;
using PrinterApi.Contracts.Models.Dataset;

namespace EvolisPrinterApi.Exceptions
{
    public class MonochromePanelExpectedException : Exception
    {
        public PrintImageType PanelType { get; }

        public MonochromePanelExpectedException(PrintImageType panelType) : base($"Expected monochrome panel but got '{panelType:G}'")
        {
            PanelType = panelType;
        }
    }
}