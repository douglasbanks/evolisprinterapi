﻿using System;

namespace EvolisPrinterApi.Exceptions
{
    public class PrinterApiException : Exception
    {
        public string JobId { get; }

        public PrinterApiException(string jobId, string error, Exception innerException = null) : base(error, innerException)
        {
            JobId = jobId;
        }
    }
}