﻿using System;

namespace EvolisPrinterApi.Exceptions
{
    public class PrinterBusyException : Exception
    {
        public bool ServiceBusy { get; set; }

        public PrinterBusyException(bool serviceBusy) : base(serviceBusy ? "Printer is busy currently processing a job" : "Printer is not in a ready state")
        {
            ServiceBusy = serviceBusy;
        }
    }
}
