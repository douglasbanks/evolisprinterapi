﻿using System;
using LibUsbDotNet;

namespace EvolisPrinterApi.Exceptions
{
    public class UsbWriteException : Exception
    {
        public Error UsbError { get; }

        public int BytesWritten { get; }

        public UsbWriteException(Error usbError, int bytesWritten) : base($"USB Write {usbError:G} ({usbError:D}): {bytesWritten} bytes written")
        {
            UsbError = usbError;
            BytesWritten = bytesWritten;
        }
    }
}