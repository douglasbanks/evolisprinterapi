﻿using System;

namespace EvolisPrinterApi.Exceptions
{
    public class PrintJobCancelledException : Exception
    {
        public PrintJobCancelledException() : base("Print job is cancelled.")
        {
        }
    }
}