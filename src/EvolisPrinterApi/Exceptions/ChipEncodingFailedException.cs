﻿namespace EvolisPrinterApi.Exceptions
{
    public class ChipEncodingFailedException : PrinterApiException
    {
        public bool External { get; }

        public ChipEncodingFailedException(string jobId, bool external) : base(jobId, $"Failed to encode chip (external:{external}).")
        {
            External = external;
        }
    }
}