﻿using System;

namespace EvolisPrinterApi.Exceptions
{
    public class CalibrationParametersOutOfRangeException : Exception
    {
        public int MinPosition { get; }

        public int DefaultPosition { get; }

        public int MaxPosition { get; }

        public CalibrationParametersOutOfRangeException(int minPosition, int defaultPosition, int maxPosition)
            : base($"Calibration parameters out of range. Expected: 0 <= minimum <= {defaultPosition} <= maximum && maximum-minimum <= 1000 (minimum={minPosition}, maximum={maxPosition})")
        {
            MinPosition = minPosition;
            DefaultPosition = defaultPosition;
            MaxPosition = maxPosition;
        }
    }
}