﻿using System;
using EvolisPrinterApi.ApplicationClasses.Drivers;

namespace EvolisPrinterApi.Exceptions
{
    public class UnexpectedPrinterStatusDetectedException : Exception
    {
        public string Group { get; }

        public string Flag { get; }

        public PrinterResponse PrinterResponse { get; }

        public UnexpectedPrinterStatusDetectedException(string group, string flag, PrinterResponse printerResponse)
            : base($"Unexpected value of {group}/{flag} in the printer status")
        {
            Group = group;
            Flag = flag;
            PrinterResponse = printerResponse;
        }
    }
}