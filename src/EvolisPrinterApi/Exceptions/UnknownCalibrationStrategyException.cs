﻿using System;
using EvolisPrinterApi.Models;

namespace EvolisPrinterApi.Exceptions
{
    public class UnknownCalibrationStrategyException : Exception
    {
        public CalibrationMode CalibrationMode { get; }

        public UnknownCalibrationStrategyException(CalibrationMode calibrationMode) : base($"Unsupported calibration mode '{calibrationMode:G}'")
        {
            CalibrationMode = calibrationMode;
        }
    }
}