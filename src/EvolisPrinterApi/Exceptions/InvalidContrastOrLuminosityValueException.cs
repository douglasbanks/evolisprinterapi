﻿using System;

namespace EvolisPrinterApi.Exceptions
{
    public class InvalidContrastOrLuminosityValueException : Exception
    {
        public string Command { get; }

        public string Value { get; }

        public InvalidContrastOrLuminosityValueException(string command, string value) : base($"{command}: invalid contrast/luminosity value '{value}'")
        {
            Command = command;
            Value = value;
        }
    }
}