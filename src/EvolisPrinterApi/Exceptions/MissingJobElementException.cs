﻿namespace EvolisPrinterApi.Exceptions
{
    public class MissingJobElementException : PrinterApiException
    {
        public string ElementName { get; }

        public MissingJobElementException(string jobId, string elementName) : base(jobId, $"'{elementName}' required in PrintJob")
        {
            ElementName = elementName;
        }
    }
}