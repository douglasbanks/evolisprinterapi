﻿using System;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using EvolisPrinterApi.ApplicationClasses;
using EvolisPrinterApi.ApplicationClasses.CSCalibration;
using EvolisPrinterApi.ApplicationClasses.Drivers;
using EvolisPrinterApi.Config;
using EvolisPrinterApi.Exceptions;
using EvolisPrinterApi.Interfaces;
using EvolisPrinterApi.Util;
using GenericHostLib.Configuration;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using PcscLib.Interfaces;
using PcscLib.Pcsc;

namespace EvolisPrinterApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // Microsoft DI
        public void ConfigureServices(IServiceCollection services)
        {
            services
                .Configure<KestrelServerOptions>(options => options.AllowSynchronousIO = true)
                .AddControllers()
                .AddNewtonsoftJson(opts => opts.SerializerSettings.Converters.Add(new StringEnumConverter()));

            services.AddSwaggerGen(c => c.SwaggerDoc("v1", new OpenApiInfo { Title = "Evolis Printer API", Version = "v1" }));
            services.AddSwaggerGenNewtonsoftSupport();
        }

        // Autofac
        public void ConfigureContainer(ContainerBuilder builder)
        {
            // Configuration
            builder.RegisterType<EvolisPrinterApiConfiguration>().AsSelf().AsImplementedInterfaces().SingleInstance();
            builder.UseConfigurationValidation();

            builder.RegisterType<ProcessExecutor>().As<IProcessExecutor>();
            builder.RegisterType<PCSCHelper>().AsSelf().SingleInstance();
            builder.RegisterType<CalibrationStrategyFactory>().AsSelf().SingleInstance();
            builder.RegisterType<PrinterManager>().AsSelf().SingleInstance();
            builder.RegisterType<EvolisUsbDevice>().As<IEvolisUsbDevice>().SingleInstance();
            builder.RegisterType<PcscScanner>().As<ISmartcardReaderScanner>().SingleInstance();
            builder.RegisterType<CommTelemetry>().AsSelf().SingleInstance();
            builder.RegisterType<PrinterConnectionInitializer>().AsImplementedInterfaces().SingleInstance();

            var printerModel = Configuration.GetValue<string>("PrinterModel");
            if (string.IsNullOrWhiteSpace(printerModel))
            {
                throw new MissingPrinterModelException();
            }

            var drivers = Assembly.GetExecutingAssembly().GetTypes().Where(type => !type.IsInterface && !type.IsAbstract && typeof(IPrinterDriver).IsAssignableFrom(type)).ToArray();
            var driverType = drivers.FirstOrDefault(type => string.Equals(type.Name, printerModel, StringComparison.InvariantCultureIgnoreCase));
            if (driverType == null)
            {
                throw new UnknownPrinterModelException(printerModel, drivers.Select(type => type.Name).ToArray());
            }

            builder.RegisterType(driverType).As<IPrinterDriver>().SingleInstance();
        }

        // HTTP Context Pipeline
        public void Configure(IApplicationBuilder app)
        {
            app.UseExceptionHandler(builder => builder.Run(ConfigureExceptionHandling));

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), 
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c => c.SwaggerEndpoint("v1/swagger.json", "Evolis Printer API V1"));

            app.UseRouting();
            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }

        private Task ConfigureExceptionHandling(HttpContext context)
        {
            var logger = context.RequestServices.GetService<ILogger<Startup>>();
            var errorFeature = context.Features.Get<IExceptionHandlerFeature>();
            var exception = errorFeature.Error;
            var problemDetails = new ProblemDetails
            {
                Title = exception.GetType().Name,
                Status = 500,
                Detail = exception.Message
            };

            switch (exception)
            {
                case PrinterApiException printerApiException:
                    problemDetails.Instance = $"urn:cardatonce:job:{printerApiException.JobId}";
                    break;
                case BadHttpRequestException badHttpRequestException:
                    problemDetails.Title = "Invalid request";
                    problemDetails.Status = badHttpRequestException.StatusCode;
                    problemDetails.Detail = badHttpRequestException.Message;
                    break;
            }

            logger?.LogError(exception, $"{problemDetails.Instance} {exception.Message}");
            context.Response.WriteJson(problemDetails, "application/problem+json");

            return Task.CompletedTask;
        }
    }

    public static class HttpExtensions
    {
        private static readonly JsonSerializer Serializer = new JsonSerializer
        {
            NullValueHandling = NullValueHandling.Ignore
        };

        public static void WriteJson<T>(this HttpResponse response, T obj, string contentType = null)
        {
            response.ContentType = contentType ?? "application/json";
            using var writer = new HttpResponseStreamWriter(response.Body, Encoding.UTF8);
            using var jsonWriter = new JsonTextWriter(writer) {CloseOutput = false, AutoCompleteOnClose = false};
            Serializer.Serialize(jsonWriter, obj);
        }
    }
}