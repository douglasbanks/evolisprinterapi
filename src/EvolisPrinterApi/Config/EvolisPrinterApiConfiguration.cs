﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;
using EvolisPrinterApi.Contracts.Models;
using EvolisPrinterApi.Models;
using GenericHostLib.Configuration;
using Microsoft.Extensions.Configuration;

namespace EvolisPrinterApi.Config
{
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public class EvolisPrinterApiConfiguration : IValidatable
    {
        public EvolisPrinterApiConfiguration(IConfiguration config)
        {
            config.Bind(this);
        }

        /// <summary>
        /// Maximum time for job status archivation within service instance. (Should be considerably greater than maximum processing time for best results.)
        /// </summary>
        public TimeSpan JobStatusTrackingMaxTime { get; set; } = TimeSpan.FromHours(1);

        /// <summary>
        /// Printer Communication Buffer Size
        /// </summary>
        public ushort PrinterCommBuffer { get; set; } = 4096; // default of UsbEndpointReader.mDefReadBufferSize = 4096, "UsbEvolisModuleG2.BufferSize" : "4096"

        /// <summary>
        /// Printer Communication Timeout
        /// </summary>
        public TimeSpan PrinterCommTimeout { get; set; } = TimeSpan.FromSeconds(5);

        /// <summary>
        /// Printer Communication USB polling rate (1/s)
        /// </summary>
        [Range(1, 100)]
        public int PrinterCommPolRate { get; set; } = 50;

        /// <summary>
        /// Number of USB R/W operation retries
        /// </summary>
        [Range(0, 100)]
        public int PrinterCommRetries { get; set; } = 3;

        /// <summary>
        /// USB Port Reset after USB I/O failure
        /// </summary>
        public bool PrinterCommUsbResetOnError { get; set; } = true;

        /// <summary>
        /// Amount of time to wait before reconnecting to the printer after usb reset (when USB error occurs)
        /// </summary>
        public TimeSpan PrinterCommDelayAfterReset { get; set; } = TimeSpan.Zero;

        /// <summary>
        /// Printer Communication Timeout
        /// </summary>
        public TimeSpan PrinterReceiveTimeout { get; set; } = TimeSpan.FromSeconds(15);

        /// <summary>
        /// Make sure that printer returns 'OK' for all calls not returning any other data
        /// </summary>
        public bool PrinterCommValidateOkResponse { get; set; } = true;

        /// <summary>
        /// Default color image sharpening mode (if not overriden in the job)
        /// </summary>
        public SharpeningMode DefaultSharpeningMode { get; set; } = SharpeningMode.None;
        
        /// <summary>
        /// Include Pida (see evolis printer documentation) in raster sent to the printer
        /// </summary>
        public bool IncludePida { get; set; } = true;
        
        /// <summary>
        /// Contact Coding Station calibration mode.
        /// Possible values are: None, Linear, Stochastic.
        /// </summary>
        public CalibrationMode CSCalibrationMode { get; set; } = CalibrationMode.None;

        /// <summary>
        /// Minimum number of 'step' units where card is positioned in CS. Approximate dimension of one unit is 0.083mm.
        /// </summary>
        [Range(0, 10000)]
        public int CSCalibrationRangeMinimum { get; set; } = 370;

        /// <summary>
        /// Maximum number of 'step' units where card is positioned in CS. Approximate dimension of one unit is 0.083mm.
        /// </summary>
        [Range(0, 10000)]
        public int CSCalibrationRangeMaximum { get; set; } = 470;

        /// <summary>
        /// Restart PCSCD (linux only) before attempting ATR.
        /// </summary>
        public bool CSCalibrationPCSCDRestart { get; set; } = true;

        /// <summary>
        /// Maximum amount of time to wait for CardInserted event during contact calibration
        /// </summary>
        public TimeSpan CSCalibrationCardInsertedTimeout { get; set; } = TimeSpan.FromMilliseconds(500);

        /// <summary>
        /// Maximum time it should take to calibrate CS station, fail encoding if there is still no usable position found.
        /// </summary>
        public TimeSpan CSCalibrationAllowedTime { get; set; } = TimeSpan.FromSeconds(45);

        /// <summary>
        /// Contact reader specification by part of name (wildcard).
        /// </summary>
        public string CSCalibrationReaderNamePattern { get; set; } = "*";

        /// <summary>
        /// File containing offset histogram to persist it between service restarts (persistence is turned off when this parameter is empty string or null)
        /// </summary>
        public string CSCalibrationOffsetHistogramFile { get; set; }

        /// <summary>
        /// File containing color profile to apply to all images (no color profile applied if null or empty string)
        /// </summary>
        public string ColorProfileConfig { get; set; }
    }
}