﻿namespace EvolisPrinterApi.Models
{
    public enum CalibrationMode
    {
        None,
        Linear,
        Stochastic
    }
}