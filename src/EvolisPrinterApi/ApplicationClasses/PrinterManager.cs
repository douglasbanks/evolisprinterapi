﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using EvolisPrinterApi.ApplicationClasses.Drivers;
using EvolisPrinterApi.Config;
using EvolisPrinterApi.Contracts.Models;
using EvolisPrinterApi.Exceptions;
using EvolisPrinterApi.Interfaces;
using EvolisPrinterApi.Util;
using HidPrinterApi.Contracts.Enums;
using HidPrinterApi.Contracts.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.Threading;
using PrinterApi.Contracts.Enums;
using PrinterApi.Contracts.Models;
using PrinterApi.Contracts.Models.Dataset;
using PrinterStatus = EvolisPrinterApi.Contracts.Models.PrinterStatus;
using RibbonInfo = EvolisPrinterApi.Contracts.Models.RibbonInfo;

namespace EvolisPrinterApi.ApplicationClasses
{
    /// <summary>
    /// This class is instantiated as a singleton
    /// </summary>
    public sealed class PrinterManager : IDisposable
    {
        private readonly IPrinterDriver printerDriver;
        private readonly EvolisPrinterApiConfiguration config;
        private readonly ILogger logger;
        private readonly PCSCHelper pcscHelper;
        private readonly object chipEncodingFinishLock = new object();
        private TaskCompletionSource<bool> chipEncodingCompletion;

        public ExternalProcessing ExternalProcessing { get; private set; }

        private readonly object jobStatusLock = new object();
        private int status = (int)PrinterApiStatus.Idle; // printer api status now correlates with printing job absolutely (there is no spooling capability)
        private readonly AsyncManualResetEvent activeJobPresent = new AsyncManualResetEvent();
        private JobStatus jobStatusValue = null;
        private JobStatus JobStatus
        {
            get { lock (jobStatusLock) { return jobStatusValue; } }
        }
        private readonly Dictionary<string, JobStatus> jobStatusMap = new Dictionary<string, JobStatus>();
        
        private Action externalCommand = null;

        public PrinterManager(IPrinterDriver printerDriver, EvolisPrinterApiConfiguration config, ILogger<PrinterManager> logger, PCSCHelper pcscHelper)
        {
            this.printerDriver = printerDriver;
            this.config = config;
            this.logger = logger;
            this.pcscHelper = pcscHelper;
        }

        public void Connect()
        {
            printerDriver.Connect();
        }

        public JobStatus GetJobStatus(string id)
        {
            lock (jobStatusLock)
            {
                if (id == null && JobStatus != null)
                {
                    id = JobStatus?.Id; //JobStatus could be null cause there is no lock being held here
                }

                if (id != null && jobStatusMap.ContainsKey(id))
                {
                    return jobStatusMap[id];
                }

                throw new UnknownJobException(id);
            }
        }

        #region PrinterControl

        public PrinterIdentity ReadPrinterIdentity()
        {
            lock (jobStatusLock)
            {
                if (Interlocked.CompareExchange(ref status, (int)PrinterApiStatus.Idle, (int)PrinterApiStatus.Idle) != (int)PrinterApiStatus.Idle)
                {
                    throw new PrinterBusyException(true);
                }

                return printerDriver.ReadIdentity();
            }
        }

        public void ResetPrinter()
        {

            lock (jobStatusLock)
            {
                externalCommand = () => 
                {
                    printerDriver.Reset();
                    throw new PrintJobCancelledException();
                };
            }

            Task.Delay(TimeSpan.FromSeconds(1)).Wait();

            lock (jobStatusLock)
            {
                if (externalCommand != null)
                {
                    externalCommand = null;
                    printerDriver.Reset();
                }
            }
        }

        public PrinterInfo GetPrinterInfo()
        {
            lock (jobStatusLock)
            {
                if (Interlocked.CompareExchange(ref status, (int) PrinterApiStatus.Idle, (int) PrinterApiStatus.Idle) != (int) PrinterApiStatus.Idle)
                {
                    throw new PrinterBusyException(true);
                }

                return new PrinterInfo {
                    PrintsTillNextCleaning = printerDriver.PrintsTillNextCleaning(),
                    AdvancedCleaningCount = printerDriver.AdvancedCleaningCount(),
                    AverageCleaningCount = printerDriver.AverageCleaningCount()
                };
            }
        }

        public void SelfAdjust()
        {
            lock (jobStatusLock)
            {
                if (Interlocked.CompareExchange(ref status, (int) PrinterApiStatus.Idle, (int) PrinterApiStatus.Idle) != (int) PrinterApiStatus.Idle)
                {
                    throw new PrinterBusyException(true);
                }

                printerDriver.SelfAdjust();
            }
        }

        public void CsCalibration(CalibrationParameters parameters = null)
        {
            lock (jobStatusLock)
            {
                if (Interlocked.CompareExchange(ref status, (int)PrinterApiStatus.Idle, (int)PrinterApiStatus.Idle) != (int)PrinterApiStatus.Idle)
                {
                    throw new PrinterBusyException(true);
                }

                printerDriver.CalibrateCS(true, parameters);
            }
        }

        public void UnlockPrinter()
        {
            lock (jobStatusLock)
            {
                if (Interlocked.CompareExchange(ref status, (int)PrinterApiStatus.Idle, (int)PrinterApiStatus.Idle) != (int)PrinterApiStatus.Idle)
                {
                    throw new PrinterBusyException(true);
                }

                printerDriver.UnlockPrinter();
            }
        }

        public void SetUserPin(int userId, string pin)
        {
            lock (jobStatusLock)
            {
                if (Interlocked.CompareExchange(ref status, (int)PrinterApiStatus.Idle, (int)PrinterApiStatus.Idle) != (int)PrinterApiStatus.Idle)
                {
                    throw new PrinterBusyException(true);
                }

                printerDriver.ResetUserPin(userId, pin);
            }
        }

        public void DisplayMessage(string message, bool confirm)
        {
            lock (jobStatusLock)
            {
                if (Interlocked.CompareExchange(ref status, (int) PrinterApiStatus.Idle, (int) PrinterApiStatus.Idle) != (int) PrinterApiStatus.Idle)
                {
                    throw new PrinterBusyException(true);
                }

                printerDriver.DisplayMessage(message, confirm);
            }
        }

        public void ClearDisplayMessage()
        {
            lock (jobStatusLock)
            {
                if (Interlocked.CompareExchange(ref status, (int)PrinterApiStatus.Idle, (int)PrinterApiStatus.Idle) != (int)PrinterApiStatus.Idle)
                {
                    throw new PrinterBusyException(true);
                }

                printerDriver.ClearDisplay();
            }
        }

        public void WaitForDisplayReady()
        {
            lock (jobStatusLock)
            {
                if (Interlocked.CompareExchange(ref status, (int)PrinterApiStatus.Idle, (int)PrinterApiStatus.Idle) != (int)PrinterApiStatus.Idle)
                {
                    throw new PrinterBusyException(true);
                }

                var i = 30;
                while (i >= 0 && !printerDriver.CheckDisplayReady())
                {
                    i--;
                    Thread.Sleep(1000);
                }

                if (i < 0)
                {
                    throw new LcdNotReadyException();
                }
            }
        }

        public string SendRawCommand(string command)
        {
            lock (jobStatusLock)
            {
                if (Interlocked.CompareExchange(ref status, (int)PrinterApiStatus.Idle, (int)PrinterApiStatus.Idle) != (int)PrinterApiStatus.Idle)
                {
                    throw new PrinterBusyException(true);
                }

                return printerDriver.SendRawCommand(command).Text;
            }
        }

        #endregion

        #region JobControl

        public void PrintJob(PrintJob job)
        {
            lock (jobStatusLock)
            {
                if (Interlocked.CompareExchange(ref status, (int)PrinterApiStatus.Printing, (int)PrinterApiStatus.Idle) != (int)PrinterApiStatus.Idle)
                {
                    Debug.Assert(JobStatus != null, nameof(JobStatus) + " != null"); // when status is Printing it should not be null
                    throw new PrintJobInProgressException(JobStatus.Id);
                }

                jobStatusValue = new JobStatus(job.Id);
                jobStatusMap[job.Id] = jobStatusValue;
                Task.Run(async () => {
                    await Task.Delay(config.JobStatusTrackingMaxTime);
                    lock (jobStatusLock)
                    {
                        jobStatusMap.Remove(job.Id);
                    }
                });
                activeJobPresent.Set();
            }

            Task.Factory.StartNew(() => Print(job), TaskCreationOptions.LongRunning);
        }

        public void ChipEncodingFinished(string jobId, string processingId, bool success)
        {
            lock (chipEncodingFinishLock)
            {
                if (ExternalProcessing == null)
                {
                    throw new NoExternalProcessingRequiredException(JobStatus?.Id ?? string.Empty); // return empty job id if there is no job currently being processed
                }

                if (!string.Equals(jobId, JobStatus.Id, StringComparison.InvariantCultureIgnoreCase) ||
                    !string.Equals(processingId, ExternalProcessing.ProcessingId, StringComparison.InvariantCultureIgnoreCase))
                {
                    var ex = new ExternalProcessingIdMismatchException(JobStatus.Id, jobId, ExternalProcessing.ProcessingId, processingId);
                    ExternalProcessing = null;
                    chipEncodingCompletion.SetException(ex);
                    throw ex;
                }

                ExternalProcessing = null;
                chipEncodingCompletion.SetResult(success);
            }
        }

        private void ValidateJob(PrintJob job)
        {
            if (string.IsNullOrEmpty(job?.Id))
            {
                throw new MissingJobElementException(job?.Id, nameof(job.Id));
            }
            logger.LogDebug($"Validating job {job.Id}");

            if (job.Dataset == null)
            {
                throw new MissingJobElementException(job.Id, nameof(job.Dataset));
            }

            var settings = job.Dataset.Printer?.Settings;
            if (settings?.ContainsKey(PrinterSettingsConstants.SharpeningMode) == true)
            {
                var str = settings[PrinterSettingsConstants.SharpeningMode];
                if (!Enum.TryParse(str, true, out SharpeningMode _))
                {
                    throw new ArgumentOutOfRangeException(PrinterSettingsConstants.SharpeningMode, $"Invalid sharpening mode '{str}'");
                }
            }
            
            if (settings?.ContainsKey(PrinterSettingsConstants.MonochromeThreshold) == true)
            {
                var str = settings[PrinterSettingsConstants.MonochromeThreshold];
                if (!byte.TryParse(str, out _))
                {
                    throw new ArgumentOutOfRangeException(PrinterSettingsConstants.MonochromeThreshold, $"Invalid monochrome threshold '{str}'");
                }
            }
            
            if (settings?.ContainsKey(PrinterSettingsConstants.Pida) == true)
            {
                var str = settings[PrinterSettingsConstants.Pida];
                if (!bool.TryParse(str, out _))
                {
                    throw new ArgumentOutOfRangeException(PrinterSettingsConstants.Pida, $"Invalid Pida bool value '{str}'");
                }
            }
        }

        /// <summary>
        /// Printing pipeline allows prioritization of personalization steps. Each module dataset has Order property inherited from base class.
        /// Personalization order is defined by Dataset.ModuleXXX.Order, where lowest value has highest priority.
        /// If two or more modules specify the same value, this service will determine the personalization order with no additional guarantees.
        /// </summary>
        /// <param name="job"></param>
        private void Print(PrintJob job)
        {
            try
            {
                logger.LogInformation($"PrintJob {job.Id} started");
                ValidateJob(job);

                PrinterStatusCheck statusCheck = printerDriver.CheckPrinterStatus();
                statusCheck.Config(ConfigStatusFlag.CFG_MAGNETIC)
                           .NotInfo(InfoStatusFlag.INF_BUSY)
                           .NotWarning(WarningStatusFlag.DEF_PRINTER_LOCKED)
                           .NotWarning(WarningStatusFlag.DEF_COVER_OPEN)
                           .NotWarning(WarningStatusFlag.DEF_FEEDER_EMPTY)
                           .NotWarning(WarningStatusFlag.DEF_HOPPER_FULL)
                           .NotWarning(WarningStatusFlag.DEF_REJECT_BOX_FULL)
                           .NotWarning(WarningStatusFlag.DEF_NO_RIBBON)
                           .NotWarning(WarningStatusFlag.DEF_RIBBON_ENDED)
                           .NotWarning(WarningStatusFlag.DEF_UNSUPPORTED_RIBBON);

                printerDriver.ClearDisplay();

                List<Tuple<ModuleDataset, Action<ModuleDataset>>> persoSteps = new List<Tuple<ModuleDataset, Action<ModuleDataset>>>
                        { new Tuple<ModuleDataset, Action<ModuleDataset>>(job.Dataset.Magstripe, (m) => WriteMagstripe(m as Magstripe)),
                          new Tuple<ModuleDataset, Action<ModuleDataset>>(job.Dataset.ChipContact, (m) => EncodeChip(m as Chip, false)),
                          new Tuple<ModuleDataset, Action<ModuleDataset>>(job.Dataset.ChipContactless, (m) => EncodeChip(m as Chip, true)),
                          new Tuple<ModuleDataset, Action<ModuleDataset>>(job.Dataset.Printer, (m) => PrintCard(m as Printer)) };
                persoSteps.RemoveAll(element => element.Item1 == null);
                persoSteps.Sort((left, right) => left.Item1.Order.CompareTo(right.Item1.Order));

                foreach ((ModuleDataset data, Action<ModuleDataset> stepFunction) in persoSteps)
                {
                    stepFunction.Invoke(data);
                }

                printerDriver.MoveToOutputBox();

                JobStatus.State = JobState.Success; // side effect: finished.Set()
                logger.LogInformation($"PrintJob {job.Id} finished successfully");
            }
            catch (Exception e)
            {
                try
                {
                    PrinterStatus printerStatus = null;
                    if (e is UnexpectedPrinterResponseException upre)
                    {
                        printerStatus = upre.PrinterResponse.CheckStatus().GetPrinterStatus();
                    }
                    else
                    {
                        try
                        {
                            printerStatus = printerDriver.CheckPrinterStatus().GetPrinterStatus();
                        }
                        catch (Exception exception)
                        {
                            logger.LogError(exception, $"Unable to get printer status after error: {exception.Message}");
                        }
                    }

                    try
                    {
                        printerDriver.ClearCurrentPrinterStatus();
                    }
                    catch (Exception exception)
                    {
                        logger.LogError(exception, $"Unable to clear current printer status after error: {exception.Message}");
                    }

                    lock (jobStatusLock)
                    {
                        logger.LogError(e, $"PrintJob {job.Id} failed: {e.Message}");
                        JobStatus.PrinterStatus = printerStatus;
                        JobStatus.State = JobState.Failure; // side effect: finished.Set();
                    }

                    printerDriver.MoveToRejectBox();
                }
                catch (Exception e1)
                {
                    logger.LogError(e1, "Failed to reject card, resetting printer...");

                    try
                    {
                        printerDriver.Reset();
                    }
                    catch (Exception e2)
                    {
                        logger.LogError(e2, $"Failed to reset printer: {e2.Message}");
                    }
                }
            }
            finally
            {
                lock (jobStatusLock)
                {
                    Interlocked.Exchange(ref status, (int)PrinterApiStatus.Idle);
                    activeJobPresent.Reset();
                    jobStatusValue = null;
                }
            }
        }

        private void WriteMagstripe(Magstripe moduleData)
        {
            if (moduleData == null)
            {
                logger.LogDebug("Skipping magstripe personalization: Magstripe data not present.");
                return;
            }
            
            JobStatus.State = JobState.MagEncode;
            logger.LogDebug("Encoding magstripe.");
            printerDriver.WriteMagstripeTracks(moduleData);
        }

        private void EncodeChip(Chip moduleData, bool contactless = false)
        {
            if (moduleData == null)
            {
                logger.LogDebug($"Skipping chip personalization: ChipContact{(contactless ? "less" : "")} data not present.");
                return;
            }

            TransportToSmartcardReader(contactless);
            if (!contactless && !printerDriver.VerifyChipContact()) // ensure that chip is reachable before handing over to HL Printer API Service
            {
                logger.LogInformation("No contact with the chip detected -> running inline smartcard offset calibration");
                printerDriver.CalibrateCS(false); // perform calibration in case that chip contact cannot be verified to improve chances for personalization step
            }

            logger.LogDebug("Waiting for external signal that chip personalization is finished");
            lock (chipEncodingFinishLock)
            {
                ExternalProcessing = new ExternalProcessing { JobId = JobStatus.Id, ProcessingId = Guid.NewGuid().ToString(), Data = moduleData, Module = contactless ? "ChipContactless" : "ChipContact"};
                chipEncodingCompletion = new TaskCompletionSource<bool>();
            }

            JobStatus.State = contactless ? JobState.ContactlessEncode : JobState.ContactEncode; // side effect: chipEncodingRequired.Set();

            // ReSharper disable once InconsistentlySynchronizedField
            bool chipCodingSuccess = chipEncodingCompletion.Task.Result; //TODO: is timeout possible? //TODO: set exception on job cancellation and timeout

            if (!chipCodingSuccess)
            {
                logger.LogError($"Chip encoding failed for job {JobStatus.Id}, external processing returned with negative result.");
                throw new ChipEncodingFailedException(JobStatus.Id, true);
            }

            JobStatus.State = contactless ? JobState.ContactlessEncodeFinished : JobState.ContactEncodeFinished; // side effect: chipEncodingRequired.Reset()
        }

        private void TransportToSmartcardReader(bool contactless = false)
        {
            pcscHelper.ResetCardInsertedEvent();
            JobStatus.State = contactless ? JobState.DockContactless : JobState.DockContact;
            printerDriver.MoveToSmartcardReader(contactless);
        }

        private void PrintCard(Printer data)
        {
            if (data == null)
            {
                logger.LogDebug("Skipping printing: Printer data not present.");
                return;
            }

            JobStatus.State = JobState.Printing;
            if (data?.Data?.FrontPanels?.Count > 0)
            {
                logger.LogDebug("Printing front side images.");
                printerDriver.FlipCard(true); // flip to front side for printing ( "Sr" )
                foreach (PrintImage panel in data.Data.FrontPanels)
                {
                    printerDriver.WriteImage(panel, data.Settings);
                }
            }
            
            if (data?.Data?.BackPanels?.Count > 0)
            {
                logger.LogDebug("Printing back side images.");
                printerDriver.FlipCard(false); // "Sv"
                foreach (PrintImage panel in data.Data.BackPanels)
                {
                    printerDriver.WriteImage(panel, data.Settings);
                }
                printerDriver.FlipCard(true); // flip again to frontside after printing backside in order to support generalized perso-step ordering (e.g. magstripe after printing)
            }

            PrinterStatusCheck statusCheck = printerDriver.CheckPrinterStatus();
            statusCheck.NotError(ErrorStatusFlag.ERR_FEEDER_ERROR)
                .NotError(ErrorStatusFlag.ERR_RIBBON_ERROR)
                .NotError(ErrorStatusFlag.ERR_COVER_OPEN)
                .NotError(ErrorStatusFlag.ERR_MECHANICAL)
                .NotError(ErrorStatusFlag.ERR_BAD_RIBBON)
                .NotError(ErrorStatusFlag.ERR_RIBBON_ENDED);
        }

        #endregion

        public async Task ActiveJobIsPresent()
        {
            await activeJobPresent.WaitAsync();
        }

        public void UpgradeFirmware(byte[] firmware)
        {
            lock (jobStatusLock)
            {
                if (Interlocked.CompareExchange(ref status, (int) PrinterApiStatus.Idle, (int) PrinterApiStatus.Idle) != (int) PrinterApiStatus.Idle)
                {
                    throw new PrinterBusyException(true);
                }

                printerDriver.UpgradeFirmware(firmware);
            }
        }

        public ActionResult<List<PrinterAdvancedSetting>> GetAdvancedSettings()
        {
            lock (jobStatusLock)
            {
                if (Interlocked.CompareExchange(ref status, (int) PrinterApiStatus.Idle, (int) PrinterApiStatus.Idle) != (int) PrinterApiStatus.Idle)
                {
                    throw new PrinterBusyException(true);
                }

                return printerDriver.GetAdvancedSettings();
            }
        }

        public void SaveAdvancedSetting(int id, int value)
        {
            lock (jobStatusLock)
            {
                if (Interlocked.CompareExchange(ref status, (int) PrinterApiStatus.Idle, (int) PrinterApiStatus.Idle) != (int) PrinterApiStatus.Idle)
                {
                    throw new PrinterBusyException(true);
                }

                printerDriver.SetAdvancedSetting(id, value);
            }
        }

        public ActionResult<PrinterStatus> GetPrinterStatus()
        {
            lock (jobStatusLock)
            {
                if (Interlocked.CompareExchange(ref status, (int) PrinterApiStatus.Idle, (int) PrinterApiStatus.Idle) != (int) PrinterApiStatus.Idle)
                {
                    throw new PrinterBusyException(true);
                }

                return printerDriver.CheckPrinterStatus().GetPrinterStatus();
            }
        }

        public RibbonInfo GetRibbonInfo()
        {
            lock (jobStatusLock)
            {
                if (Interlocked.CompareExchange(ref status, (int)PrinterApiStatus.Idle, (int)PrinterApiStatus.Idle) != (int)PrinterApiStatus.Idle)
                {
                    throw new PrinterBusyException(true);
                }

                return printerDriver.GetRibbonInfo();
            }
        }

        public void Dispose()
        {
            printerDriver?.Dispose();
        }
    }
}