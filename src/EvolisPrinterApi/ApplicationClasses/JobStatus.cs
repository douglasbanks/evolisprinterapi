﻿using System.Threading.Tasks;
using EvolisPrinterApi.Contracts.Models;
using Microsoft.VisualStudio.Threading;
using Newtonsoft.Json;
using PrinterApi.Contracts.Enums;

namespace EvolisPrinterApi.ApplicationClasses
{
    /// <summary>
    /// Holds current print job status. Allows to wait for print job completion or request for external processing.
    /// This class mimics the PrinterApi.Contracts.Models.JobStatus class.
    /// </summary>
    public class JobStatus
    {
        private JobState state = JobState.New;
        private readonly AsyncManualResetEvent finished = new AsyncManualResetEvent();
        private readonly AsyncManualResetEvent chipEncodingRequired = new AsyncManualResetEvent();

        /// <summary>
        /// Print Job Id
        /// </summary>
        public string Id { get; }

        public JobState State
        {
            get => state;
            set {
                state = value;
                switch (value)
                {
                    case JobState.New:
                        finished.Reset();
                        chipEncodingRequired.Reset();
                        break;

                    case JobState.Failure:
                    case JobState.Success:
                    case JobState.Canceled:
                        finished.Set();
                        break;

                    case JobState.ContactEncode:
                    case JobState.ContactlessEncode:
                        chipEncodingRequired.Set();
                        break;

                    case JobState.ContactEncodeFinished:
                    case JobState.ContactlessEncodeFinished:
                        chipEncodingRequired.Reset();
                        break;
                }
            }
        }

        public PrinterStatus PrinterStatus { get; set; }

        public JobStatus(string id)
        {
            this.Id = id;
            this.PrinterStatus = null;
        }

        /// <summary>
        /// Task to wait for print job completion
        /// </summary>
        [JsonIgnore]
        public Task Finished => finished.WaitAsync();

        /// <summary>
        /// Task to wait for request for external processing
        /// </summary>
        [JsonIgnore]
        public Task ExternalChipEncodingRequired => chipEncodingRequired.WaitAsync();
    }
}