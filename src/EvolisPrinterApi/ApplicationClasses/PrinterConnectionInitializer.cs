﻿using System;
using System.Threading;
using System.Threading.Tasks;
using EvolisPrinterApi.Util;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace EvolisPrinterApi.ApplicationClasses
{
    public class PrinterConnectionInitializer : IHostedService
    {
        private readonly ILogger logger;
        private readonly PrinterManager printerManager;
        private readonly PCSCHelper pcscHelper;

        public PrinterConnectionInitializer(ILogger<PrinterConnectionInitializer> logger, PrinterManager printerManager, PCSCHelper pcscHelper)
        {
            this.logger = logger;
            this.printerManager = printerManager;
            this.pcscHelper = pcscHelper;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            try
            {
                printerManager.Connect();
                pcscHelper.StartSmartcardEventMonitoring();
            }
            catch (Exception e)
            {
                logger.LogCritical(e, e.Message);
            }
            
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }
    }
}