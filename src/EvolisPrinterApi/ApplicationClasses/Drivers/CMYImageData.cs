﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using EvolisPrinterApi.Util;

namespace EvolisPrinterApi.ApplicationClasses.Drivers
{
    public class CMYImageData
    {
        public readonly int Width;
        public readonly int Height;

        public double CMean => ColorPlaneAverage(cData);
        public double MMean => ColorPlaneAverage(mData);
        public double YMean => ColorPlaneAverage(yData);

        // Calculates mean by summing all values first. (for 1016x648 it is 167.883.840 maximum for all 8bit values of 255)
        private double ColorPlaneAverage(int[] data)
        {
            double returnValue = 0.0d;
            if (data != null)
            {
                double sum = data.Aggregate(0.0d, (current, t) => current + t);
                returnValue = sum / data.Length;
            }

            return returnValue;
        }

        public int WhitePixelCount 
        {
            get
            {
                int returnValue = 0;

                if  (yData != null && mData != null && cData != null)
                {
                    for (int i = 0; i < Area; i++)
                    {
                        if (yData[i] + mData[i] + cData[i] == 0.0d)
                        {
                            ++returnValue;
                        }
                    }
                }

                return returnValue;
            }
        }

        private int Area => this.Height * this.Width;

        private readonly int[] cData;
        private readonly int[] mData;
        private readonly int[] yData;

        public CMYImageData(int width, int height)
        {
            this.Width = width;
            this.Height = height;

            cData = new int[Area]; Array.Fill(cData, 0);
            mData = new int[Area]; Array.Fill(mData, 0);
            yData = new int[Area]; Array.Fill(yData, 0);
        }

        /// <summary>
        /// Restores CMYImageData from byte[] of printer raster for CMY image. This method is mainly intended for testing and examination, not for production use.
        /// </summary>
        /// <param name="rasterBytes"></param>
        /// <param name="expectedWidth"></param>
        /// <param name="expectedHeight"></param>
        public CMYImageData(byte[] rasterBytes, int expectedWidth, int expectedHeight)
        {
            this.Width = expectedWidth;
            this.Height = expectedHeight;

            cData = new int[Area]; Array.Fill(cData, 0);
            mData = new int[Area]; Array.Fill(mData, 0);
            yData = new int[Area]; Array.Fill(yData, 0);

            // we could ignore prefix and escape codes, we only check it is there - length is not fixed for first part
            int position = 0;
            while (position < rasterBytes.Length && !BinaryConversion.Contains(rasterBytes, position, RasterSectionEndSequence))
            {
                ++position;
            }
            position += RasterSectionEndSequence.Length;

            // split color plane 'Y'
            byte[] colorPlanePrefixY = Get8BitColorPlanePrefix('y');
            if (!BinaryConversion.Contains(rasterBytes, position, colorPlanePrefixY))
            {
                throw new Exception("Unexpected format of data"); // TODO: make proper exception and error message
            }
            position += colorPlanePrefixY.Length;
            if (position + Area >= rasterBytes.Length)
            {
                throw new Exception("Unexpected format of data"); // TODO: make proper exception and error message
            }
            for (int i = 0; i < Area; ++i)
            {
                yData[i] = rasterBytes[position++] & 0xFF;
            }
            position += RasterSectionEndSequence.Length;

            // split color plane 'M'
            byte[] colorPlanePrefixM = Get8BitColorPlanePrefix('m');
            if (!BinaryConversion.Contains(rasterBytes, position, colorPlanePrefixM))
            {
                throw new Exception("Unexpected format of data"); // TODO: make proper exception and error message
            }
            position += colorPlanePrefixM.Length;
            if (position + Area >= rasterBytes.Length)
            {
                throw new Exception("Unexpected format of data"); // TODO: make proper exception and error message
            }
            for (int i = 0; i < Area; ++i)
            {
                mData[i] = rasterBytes[position++] & 0xFF;
            }
            position += RasterSectionEndSequence.Length;

            // split color plane 'C'
            byte[] colorPlanePrefixC = Get8BitColorPlanePrefix('c');
            if (!BinaryConversion.Contains(rasterBytes, position, colorPlanePrefixC))
            {
                throw new Exception("Unexpected format of data"); // TODO: make proper exception and error message
            }
            position += colorPlanePrefixC.Length;
            if (position + Area >= rasterBytes.Length)
            {
                throw new Exception("Unexpected format of data"); // TODO: make proper exception and error message
            }
            for (int i = 0; i < Area; ++i)
            {
                cData[i] = rasterBytes[position++] & 0xFF;
            }
            position += RasterSectionEndSequence.Length;
            if (position != rasterBytes.Length)
            {
                throw new Exception("Unexpected format of data"); // TODO: make proper exception and error message
            }
        }

        public void SetPixel(int x, int y, int channelC, int channelM, int channelY)
        {
            int pos = x * this.Height + (this.Height - 1 - y);
            cData[pos] = channelC;
            mData[pos] = channelM;
            yData[pos] = channelY;
        }

        // return C, M, Y pixel data
        public Tuple<int, int,  int> GetPixel(int x, int y)
        {
            x = Math.Clamp(x, 0, this.Width-1);
            y = Math.Clamp(y, 0, this.Height-1);
            int pos = x * this.Height + (this.Height - 1 - y);
            return new Tuple<int, int, int>(cData[pos], mData[pos], yData[pos]);
        }

        public static readonly byte[] RasterSectionEndSequence = { 13 };

        // returns raster as it is expected by a printer (including all headers/formatting)
        [SuppressMessage("ReSharper", "SuspiciousTypeConversion.Global")]
        public byte[] GetRaster(bool includePida = true)
        {
            List<byte> result = new List<byte>();

            if (includePida)
            {
                result.AddRange(GetPidaPrefix());
                result.AddRange(RasterSectionEndSequence); // "\r"
            }

            result.AddRange(Get8BitColorPlanePrefix('y'));
            result.AddRange(yData.Select(i => (byte)(i & 0xFF)));
            result.AddRange(RasterSectionEndSequence); // "\r"
            result.AddRange(Get8BitColorPlanePrefix('m'));
            result.AddRange(mData.Select(i => (byte)(i & 0xFF)));
            result.AddRange(RasterSectionEndSequence); // "\r"
            result.AddRange(Get8BitColorPlanePrefix('c'));
            result.AddRange(cData.Select(i => (byte)(i & 0xFF)));
            result.AddRange(RasterSectionEndSequence); // "\r"

            return result.ToArray();
        }

        /// <summary>
        /// The 'y', 'm', 'c' or other valid color plane letter should be used.
        /// </summary>
        /// <param name="colorPlane"></param>
        /// <returns></returns>
        public static byte[] Get8BitColorPlanePrefix(char colorPlane)
        {
            string prefix = $"\u001bDb;{colorPlane};256;";
            byte[] returnValue = new byte[prefix.Length];
            for (int i = 0; i < prefix.Length; ++i)
            {
                returnValue[i] = (byte)prefix[i];
            }

            return returnValue;
        }
        
        public byte[] GetPidaPrefix()
        {
            return GetPidaPrefix(this.WhitePixelCount, this.Area, CMean, MMean, YMean);
        }

        public static byte[] GetPidaPrefix(int whitePixelCount, int area, double cMean, double mMean, double yMean)
        {
            string prefix = $"\u001bPida;{(int)Math.Round((double)whitePixelCount / (double)area * 100.0D)};{(int)Math.Round(yMean)};{(int)Math.Round(mMean)};{(int)Math.Round(cMean)}";
            byte[] returnValue = new byte[prefix.Length];
            for (int i = 0; i < prefix.Length; ++i)
            {
                returnValue[i] = (byte)prefix[i];
            }

            return returnValue;
        }

    }
}