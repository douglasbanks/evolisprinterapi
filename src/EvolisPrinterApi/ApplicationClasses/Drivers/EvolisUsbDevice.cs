using System;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading;
using EvolisPrinterApi.Config;
using EvolisPrinterApi.Exceptions;
using EvolisPrinterApi.Interfaces;
using EvolisPrinterApi.Util;
using LibUsbDotNet;
using LibUsbDotNet.LibUsb;
using LibUsbDotNet.Main;
using Microsoft.Extensions.Logging;

namespace EvolisPrinterApi.ApplicationClasses.Drivers
{
    /// <summary>
    /// When using on Windows libusb-win32 driver has to be manually installed for the Evolis USB device
    /// https://github.com/libusb/libusb/wiki/Windows#how-to-use-libusb-on-windows
    /// </summary>
    public class EvolisUsbDevice : IEvolisUsbDevice
    {
        private const byte Start = (byte) '\x1b'; // ESC or \u001b
        private const byte Stop = (byte) '\x0d';  // \r

        private readonly ILogger logger;
        private readonly EvolisPrinterApiConfiguration config;
        private readonly CommTelemetry commTelemetry;
        private readonly object connectLock = new object();
        private readonly UsbContext context = new UsbContext();
        private readonly int timeoutMilliseconds;
        private ushort vendorId;
        private ushort productId;
        private ReadEndpointID readEndpointId;
        private WriteEndpointID writeEndpointId;
        private bool connected;
        private IUsbDevice device;
        private UsbEndpointReader reader;
        private UsbEndpointWriter writer;
        private IDisposable loggerScope;
        private readonly byte[] mBuffer;

        public EvolisUsbDevice(ILogger<EvolisUsbDevice> logger, EvolisPrinterApiConfiguration config, CommTelemetry commTelemetry)
        {
            this.logger = logger;
            this.config = config;
            this.commTelemetry = commTelemetry;
            timeoutMilliseconds = (int) config.PrinterCommTimeout.TotalMilliseconds;
            mBuffer = new byte[config.PrinterCommBuffer];
        }

        
        [SuppressMessage("ReSharper", "ParameterHidesMember")]
        public void Connect(ushort vendorId, ushort productId, ReadEndpointID readEndpointId, WriteEndpointID writeEndpointId)
        {
            lock (connectLock)
            {
                if (!connected)
                {
                    try
                    {
                        this.vendorId = vendorId;
                        this.productId = productId;
                        this.readEndpointId = readEndpointId;
                        this.writeEndpointId = writeEndpointId;
                        
                        commTelemetry.BeginConnect();
                        loggerScope = logger.BeginScope($"{vendorId:X4}:{productId:X4}");
                        
                        logger.LogDebug($"Connecting to USB printer '{vendorId:X4}:{productId:X4}'");
                        device = context.List().FirstOrDefault(d => d.VendorId == vendorId && d.ProductId == productId);
                        if (device == null)
                        {
                            var e = new UnableToConnectToPrinterException(vendorId, productId);
                            commTelemetry.ConnectError(e.Message);
                            throw e;
                        }

                        logger.LogDebug($"Claiming interface {device.Configs[0].Interfaces[0].Number} of device {device.Info}");

                        device.Open();
                        device.ClaimInterface(device.Configs[0].Interfaces[0].Number);
                        reader = device.OpenEndpointReader(readEndpointId, config.PrinterCommBuffer);
                        writer = device.OpenEndpointWriter(writeEndpointId);

                        connected = true;
                        string message = $"Connected to USB printer '{vendorId:X4}:{productId:X4}'";
                        logger.LogInformation(message);
                        commTelemetry.EndConnect(message);
                    }
                    catch (UnableToConnectToPrinterException)
                    {
                        throw;
                    }
                    catch (Exception e)
                    {
                        var ex = new UnableToConnectToPrinterException(vendorId, productId, e);
                        commTelemetry.ConnectError(ex.Message);
                        throw ex;
                    }
                }
            }
        }

        public void Disconnect()
        {
            lock (connectLock)
            {
                if (connected)
                {
                    commTelemetry.Disconnect();
                    device?.Close();
                    device?.Dispose();
                    connected = false;
                    logger.LogInformation($"Disconnected from USB printer '{vendorId:X4}:{productId:X4}'");
                    loggerScope?.Dispose();
                }
            }
        }

        /// <summary>
        /// Bulk Transfers take up all the bandwidth that is available after the other transfers have finished. If the bus is very busy, then a Bulk Transfer may be delayed.
        /// The maximum packet size for the bulk endpoint data is:
        /// 8, 16, 32 or 64 bytes for full-speed
        /// 512 bytes for high-speed
        /// </summary>
        /// <param name="data"></param>
        /// <param name="comment"></param>
        public PrinterResponse Transceive(string data, string comment = null)
        {
            logger.LogTrace($"{(string.IsNullOrWhiteSpace(comment) ? "" : $"[{comment}] ")}<- {data}");
            commTelemetry.RequestWrite(data);
            PrinterResponse response = Transceive(data == null ? null : Encoding.ASCII.GetBytes(data), null, false);
            logger.LogTrace($"-> {response}");
            return response;
        }

        public PrinterResponse Transceive(byte[] data, string comment = null, bool trace = true)
        {
            EnsureConnected();
            
            if (trace)
            {
                logger.LogTrace($"{(string.IsNullOrWhiteSpace(comment) ? "" : $"[{comment}] ")}<- {BinaryConversion.HexTrace(data)}");
            }

            WriteData(data);
            var response = ReadData(data);

            if (trace)
            {
                logger.LogTrace($"-> {response}");
            }
            
            return response;
        }

        private void EnsureConnected()
        {
            if (!connected)
            {
                Reconnect();
            }
        }

        /// <param name="data">Only for tracing purposes, not actually used</param>
        private PrinterResponse ReadData(byte[] data)
        {
            int pollRate = config.PrinterCommPolRate;
            int polls = 0;
            int minimalTextResponseLength = (data == null || data.Length == 0) ? 0 : 1;
            Stopwatch sw = Stopwatch.StartNew();

            PrinterResponse response;
            do
            {
                ++polls;
                commTelemetry.ResponseReadBegin(polls, data, sw.ElapsedMilliseconds);

                if (sw.ElapsedMilliseconds * pollRate / polls < 1000)
                {
                    int sleepAdjustment = (int)Math.Max(0, (1000 * polls / pollRate) - sw.ElapsedMilliseconds);
                    Thread.Sleep(sleepAdjustment);
                }

                try
                {
                    Array.Clear(mBuffer, 0, mBuffer.Length);
                    Error ec = reader.Read(mBuffer, timeoutMilliseconds, out int bytesReceived);
                    response = new PrinterResponse(mBuffer, bytesReceived, ec, null);
                }
                catch (Exception ex)
                {
                    response = new PrinterResponse(mBuffer, 0, Error.Other, ex);
                    logger.LogError(ex, $"Failed to read from USB (will try to reconnect): {ex.Message}");
                    Reconnect();
                }

                commTelemetry.ResponseReadEnd(polls, data, response, sw.ElapsedMilliseconds);
            } while (sw.Elapsed < config.PrinterReceiveTimeout && response.Text.Length < minimalTextResponseLength);
            sw.Stop();

            response.AddCommTelemetryInfo(polls, sw.Elapsed);

            commTelemetry.TranscieveComplete(polls, sw.ElapsedMilliseconds, data, response);

            return response;
        }

        private void WriteData(byte[] data)
        {
            var retries = config.PrinterCommRetries;
            var success = false;
            var lastUsbError = Error.Success;
            
            while (!success && retries-- >= 0)
            {
                try
                {
                    commTelemetry.RequestWrite(data);
                    if (data != null)
                    {
                        commTelemetry.RequestWriteBegin(data);
                        WriteBytes(data);
                        commTelemetry.RequestWriteEnd(data);
                    }

                    success = true;
                }
                catch (Exception e)
                {
                    if (e is UsbWriteException uwe)
                    {
                        lastUsbError = uwe.UsbError;
                    }
                    
                    logger.LogError(e, $"Failed to write to USB (will try to reconnect): {e.Message}");
                    Reconnect();
                }
            }

            if (!success)
            {
                throw new RepeatedUsbWriteException(lastUsbError, config.PrinterCommRetries);
            }
        }

        private void WriteBytes(byte[] data)
        {
            data = data.Prepend(Start).Append(Stop).ToArray();
            var bytesSent = 0;
            
            while (bytesSent < data.Length)
            {
                var chunkSize = Math.Min(config.PrinterCommBuffer, data.Length - bytesSent);
                var ec = writer.Write(data, bytesSent, chunkSize, timeoutMilliseconds, out var bytesWritten);
                if (ec != Error.Success)
                {
                    throw new UsbWriteException(ec, bytesWritten);
                }

                bytesSent += bytesWritten;
            }
            
            writer.Write(new byte[] {0}, timeoutMilliseconds, out _);
        }

        private void Reconnect()
        {
            if (config.PrinterCommUsbResetOnError)
            {
                try
                {
                    if (device != null)
                    {
                        logger.LogInformation("USB Reset");
                        device.ResetDevice();
                    }
                }
                catch (UsbException e)
                {
                    if (e.ErrorCode == Error.NotFound)
                    {
                        logger.LogInformation("USB Reset done but device needs to be rediscovered and reconnected");
                    }
                    else
                    {
                        logger.LogError(e, $"USB Reset failure: {e.Message}");
                    }
                }
                catch (Exception e)
                {
                    logger.LogError(e, $"USB Reset failure: {e.Message}");
                }
            }
            
            logger.LogInformation($"Reconnecting to the printer after {config.PrinterCommDelayAfterReset}");
            if (config.PrinterCommDelayAfterReset > TimeSpan.Zero)
            {
                Thread.Sleep(config.PrinterCommDelayAfterReset);
            }

            Disconnect();
            Connect(vendorId, productId, readEndpointId, writeEndpointId);
        }

        public void Dispose()
        {
            Disconnect();
            context?.Dispose();
        }
    }
}