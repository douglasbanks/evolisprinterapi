﻿namespace EvolisPrinterApi.ApplicationClasses.Drivers
{
    //TODO: implement this model in order to gather telemetry data on each printer call
    //TODO: implement serialization into event stream which is periodically picked up by our backend
    //TODO: - limit amount of memory for this backlog to configured value, track overflows
    public class CommTelemetry
    {
        public void Disconnect()
        {
            //throw new NotImplementedException(); //TODO
        }

        public void ConnectError(string eMessage)
        {
            //throw new NotImplementedException(); //TODO
        }

        public void EndConnect(string message)
        {
            //throw new NotImplementedException(); //TODO
        }

        public void BeginConnect()
        {
            //throw new NotImplementedException(); //TODO
        }

        public void RequestWrite(string data)
        {
            //throw new NotImplementedException(); //TODO
        }

        public void RequestWrite(byte[] data)
        {
            //throw new NotImplementedException(); //TODO
        }

        public void RequestWriteBegin(byte[] data)
        {
            //throw new NotImplementedException(); //TODO
        }

        public void RequestWriteEnd(byte[] data)
        {
            //throw new NotImplementedException(); //TODO
        }

        public void ResponseReadBegin(int polls, byte[] data, in long swElapsedMilliseconds)
        {
            //throw new NotImplementedException(); //TODO
        }

        public void ResponseReadEnd(int polls, byte[] data, PrinterResponse response, in long swElapsedMilliseconds)
        {
            //throw new NotImplementedException(); //TODO
        }

        public void TranscieveComplete(in int polls, in long swElapsedMilliseconds, byte[] data, PrinterResponse response)
        {
            // throw new NotImplementedException(); //TODO
        }
    }
}
