﻿using System;
using System.IO;
using System.Drawing;
using EvolisPrinterApi.Contracts.Models;
using EvolisPrinterApi.Exceptions;
using PrinterApi.Contracts.Models.Dataset;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Advanced;
using SixLabors.ImageSharp.Formats;
using SixLabors.ImageSharp.Formats.Png;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp.Metadata.Profiles.Icc;

namespace EvolisPrinterApi.ApplicationClasses.Drivers
{

    // small note to 'smoothing' (sharpening) conversion and final value capping (>255.0f -> (byte)-1, <0.0f -> (byte)0, rest is (byte)((int)smoothValue & 255):
    // ... when byte bnum = -1; then int intconversion = bnum & 255; yields intconversion value of 255 (due to automatic conversion of bnum to int as it is in expression with int literal (255))

    public class EvolisImageCodec
    {
        public static CMYImageData ConvertColorImage(Image<Rgb24> image, bool autoRotate = true, SharpeningMode sharpening = SharpeningMode.None, string iccColorProfile = null)
        {
           Func<int, int, int, int, Tuple<int, int>> coordinateTransform = TransformIdentity;
           int outputWidth = image.Width;
           int outputHeight = image.Height;
           if (autoRotate && image.Height > image.Width)
           {
               // This logic is taken from Mosaic
               outputHeight = image.Width;
               outputWidth = image.Height;
               coordinateTransform = TransformRotationL90;
           }

           if (!String.IsNullOrEmpty(iccColorProfile))
           {
               image.Metadata.IccProfile = LoadColorProfile(iccColorProfile).DeepClone();
           }

           CMYImageData outputImage = new CMYImageData(outputWidth, outputHeight);

           for (int x = 0; x < image.Width; ++x)
           {
               for (int y = 0; y < image.Height; ++y)
               {
                   Rgb24 pixel = image[x, y];
                   int py = 255 - pixel.B;
                   int pm = 255 - pixel.G;
                   int pc = 255 - pixel.R;

                   (int nx, int ny) = coordinateTransform(x, y, image.Width, image.Height);

                   outputImage.SetPixel(nx, ny, pc, pm, py);
               }
           }

           for(int i = 0; i < (int)sharpening; ++i)
           {
               outputImage = EvolisSharpen(outputImage);
           }

           return outputImage;
        }

        private static IccProfile LoadColorProfile(string filename)
        {
            var profileData = File.ReadAllBytes(filename);
            IccProfile profile = new IccProfile(profileData);

            if (!profile.CheckIsValid())
            {
                throw new InvalidIccProfileException($"Invalid color profile at '{filename}'");
            }

            return profile;
        }

        public static MonochromeImageData ConvertMonoImage(Image<Rgb24> image, PrintImageType panelType, in bool panelRotateResize, int threshold = 128)
        {
            // ReSharper disable once SwitchExpressionHandlesSomeKnownEnumValuesWithExceptionInDefault
            return panelType switch
            {
                PrintImageType.K => ConvertMonoImage(image, "k", panelRotateResize, threshold),
                PrintImageType.Overlay => ConvertMonoImage(image, "o", panelRotateResize, threshold),
                _ => throw new MonochromePanelExpectedException(panelType)
            };
        }

        public static MonochromeImageData ConvertMonoImage(Image<Rgb24> image, string imageType, bool autoRotate = true, int threshold = 128)
        {
            Func<int, int, int, int, Tuple<int, int>> coordinateTransform = TransformIdentity;
            int outputWidth = image.Width;
            int outputHeight = image.Height;
            if (autoRotate && image.Height < image.Width)
            {
                // This logic is taken from Mosaic
                outputHeight = image.Width;
                outputWidth = image.Height;
                coordinateTransform = TransformRotationR90; // monochrome images are (auto) rotated when height < width TODO: figure out if this is correct and why!
            }

            MonochromeImageData outputImage = new MonochromeImageData(outputWidth, outputHeight, imageType);

            for (int x = 0; x < image.Width; ++x)
            {
                for (int y = 0; y < image.Height; ++y)
                {
                    Rgb24 pixel = image[x, y];
                    int py = 255 - pixel.B;
                    int pm = 255 - pixel.G;
                    int pc = 255 - pixel.R;

                    (int nx, int ny) = coordinateTransform(x, y, image.Width, image.Height);

                    outputImage.SetPixel(nx, ny, py+pm+pc > 3*threshold);
                }
            }

            return outputImage;
        }

        // x is a column (2nd index), y is a row (1st index) (it does not make a difference for symmetrical matrix anyway)
        private static readonly int[,] CMatrix =   {{-1, -1, -1, -1, -1},
                                                    {-1, -1, -1, -1, -1},
                                                    {-1, -1, 49, -1, -1},
                                                    {-1, -1, -1, -1, -1},
                                                    {-1, -1, -1, -1, -1}};
        
        private static CMYImageData EvolisSharpen(CMYImageData inputImage)
        {
            CMYImageData outputImage = new CMYImageData(inputImage.Width, inputImage.Height);

            double cFactor = 0.0D; // ReSharper disable once EmptyEmbeddedStatement
            for(int i = 0; i < 25; cFactor += CMatrix[i / 5, i % 5], ++i);
            cFactor = 1.0D / cFactor;

            // Apply convolution matrix transformation.
            for(int indexLine = 0; indexLine < inputImage.Height; ++indexLine)
            {
                for(int indexDot = 0; indexDot < inputImage.Width; ++indexDot)
                {
                    int colorValueC = 0;
                    int colorValueM = 0;
                    int colorValueY = 0;

                    for(int y = -2; y <= 2; ++y)
                    {
                        for(int x = -2; x <= 2; ++x)
                        {
                            (int pixelCValue, int pixelMValue, int pixelYValue) = inputImage.GetPixel(indexDot + x, indexLine + y);
                            colorValueC += pixelCValue * CMatrix[y + 2, x + 2];
                            colorValueM += pixelMValue * CMatrix[y + 2, x + 2];
                            colorValueY += pixelYValue * CMatrix[y + 2, x + 2];
                        }
                    }

                    outputImage.SetPixel(indexDot, indexLine, (int)Math.Round(Math.Clamp(colorValueC*cFactor, 0.0, 255.0)), 
                                                              (int)Math.Round(Math.Clamp(colorValueM*cFactor, 0.0, 255.0)), 
                                                              (int)Math.Round(Math.Clamp(colorValueY*cFactor, 0.0, 255.0)));
                }
            }

            return outputImage;
        }

        public static Image<Rgb24> DecodeColorImage(CMYImageData data)
        {
            Image<Rgb24> outputImage = new Image<Rgb24>(data.Width, data.Height);

            for (int x = 0; x < data.Width; ++x)
            {
                for (int y = 0; y < data.Height; ++y)
                {
                    (int pixelC, int pixelM, int pixelY) = data.GetPixel(x, y);
                    
                    int pixelB = 255 - pixelY;
                    int pixelG = 255 - pixelM;
                    int pixelR = 255 - pixelC;

                    outputImage[x, y] = new Rgb24((byte)pixelR, (byte)pixelG, (byte)pixelB);
                }
            }

            return outputImage;
        }

        public static Image<Rgb24> DecodeMonoImage(MonochromeImageData data)
        {
            Image<Rgb24> outputImage = new Image<Rgb24>(data.Width, data.Height);

            for (int x = 0; x < data.Width; ++x)
            {
                for (int y = 0; y < data.Height; ++y)
                {
                    int pixel = data.GetPixel(x, y)?0:255;
                    outputImage[x, y] = new Rgb24((byte)pixel, (byte)pixel, (byte)pixel);
                }
            }

            return outputImage;
        }

        // returns [x', y'] from given [x, y] with known w, h.
        private static Tuple<int, int> TransformIdentity(int x, int y, int w, int h)
        {
            return new Tuple<int, int>(x, y);
        }

        // returns [x', y'] from given [x, y] with known w, h. Rotate 90 degrees in positive direction.
        private static Tuple<int, int> TransformRotationL90(int x, int y, int w, int h)
        {
            return new Tuple<int, int>(y, w - x - 1);
        }

        // returns [x', y'] from given [x, y] with known w, h. Rotate 90 degrees in negative direction.
        private static Tuple<int, int> TransformRotationR90(int x, int y, int w, int h)
        {
            return new Tuple<int, int>(h - y -1, x);
        }

        public static Bitmap ToBitmap<TPixel>(Image<TPixel> image) where TPixel : unmanaged, IPixel<TPixel>
        {
            using MemoryStream memoryStream = new MemoryStream();
            IImageEncoder imageEncoder = image.GetConfiguration().ImageFormatsManager.FindEncoder(PngFormat.Instance);

            image.Save(memoryStream, imageEncoder);
            memoryStream.Seek(0, SeekOrigin.Begin);
            return new Bitmap(memoryStream);
        }
    }
}

/*   
public class MonochromeSequencer implements ISequencer {
   private byte[] _graphicalData = null;
   private int _height = 0;
   private int _width = 0;
   private int _cardRWidth = 0;
   private String _type;

   public MonochromeSequencer(String type) {
      this._type = type;
   }

   public void graphicalProcessing(BufferedImage image, SmoothingType smoothType, int threshold, boolean unused) {
      this._height = image.getHeight();
      this._width = image.getWidth();
      int w;
      int rgb;
      if (this._height < this._width) {
         BufferedImage image90 = new BufferedImage(this._height, this._width, 1);

         for(w = 0; w < this._width; ++w) {
            for(rgb = 0; rgb < this._height; ++rgb) {
               image90.setRGB(this._height - rgb - 1, w, image.getRGB(w, rgb));
            }
         }

         image = image90;
      }

      this._height = image.getHeight();
      this._width = image.getWidth();
      this._cardRWidth = this._width >> 3;
      this._graphicalData = new byte[this._height * this._width >> 3];

      for(int h = 0; h < this._height; ++h) {
         for(w = 0; w < this._width; ++w) {
            rgb = image.getRGB(w, h);
            int red = rgb >> 16 & 255;
            int green = rgb >> 8 & 255;
            int blue = rgb & 255;
            int value = (255 - blue + (255 - green) + (255 - red)) / 3;
            this.setPixel(w, h, (byte)(value > threshold ? 1 : 0));
         }
      }

   }

   private void setPixel(int w, int h, byte value) {
      int Wbyte = w >> 3;
      int Wbit = w & 7;
      int position = Wbyte + h * this._cardRWidth;
      this._graphicalData[position] = this.setBit(value, 7 - Wbit, this._graphicalData[position]);
   }

   private byte setBit(byte bitvalue, int position, byte previousValue) {
      byte returnValue;
      if (bitvalue == 1) {
         returnValue = (byte)(previousValue | 1 << position);
      } else {
         returnValue = (byte)(previousValue & ~(1 << position));
      }

      return returnValue;
   }

   public byte[] getRaster() {
      return ArrayUtil.merge(ArrayUtil.merge(("\u001bDb;" + this._type + ";2;").getBytes(), this._graphicalData), "\r".getBytes());
   }
}
*/
