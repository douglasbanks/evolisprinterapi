using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using EvolisPrinterApi.ApplicationClasses.CSCalibration;
using EvolisPrinterApi.Config;
using EvolisPrinterApi.Contracts.Models;
using EvolisPrinterApi.Exceptions;
using EvolisPrinterApi.Interfaces;
using EvolisPrinterApi.Models;
using EvolisPrinterApi.Util;
using HidPrinterApi.Contracts.Models;
using LibUsbDotNet.Main;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PrinterApi.Contracts.Models.Dataset;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.PixelFormats;
using RibbonInfo = EvolisPrinterApi.Contracts.Models.RibbonInfo;

// ReSharper disable InconsistentNaming
namespace EvolisPrinterApi.ApplicationClasses.Drivers
{
    /// <inheritdoc cref="IPrinterDriver" />
    /// <summary>
    /// This class is instantiated as a singleton.
    /// </summary>
    public abstract class EvolisPrinterBase : IPrinterDriver
    {
        private static EvolisPrinterBase instance;
        private readonly EvolisPrinterApiConfiguration config;
        private readonly ILogger logger;
        private readonly IEvolisUsbDevice device;
        private readonly PCSCHelper pcscHelper;
        private readonly CalibrationStrategyFactory calibrationStrategyFactory;

        // Printer-specific parameters
        private readonly int printWidth;
        private readonly int printHeight;
        private readonly ushort vendorId;
        private readonly ushort productId;
        private readonly ReadEndpointID readEndpointId;
        private readonly WriteEndpointID writeEndpointId;

        public EvolisPrinterBase(EvolisPrinterApiConfiguration config, ILogger<EvolisPrinterBase> logger, int printHeight, int printWidth, ushort vendorId, ushort productId,
            ReadEndpointID readEndpointId, WriteEndpointID writeEndpointId, IEvolisUsbDevice device, PCSCHelper pcscHelper, CalibrationStrategyFactory calibrationStrategyFactory)
        {
            // this is (and must be) registered as a singleton
            if (null != Interlocked.CompareExchange(ref instance, this, null))
            {
                throw new MultipleDriverInstantiationException();
            }

            this.config = config;
            this.logger = logger;

            this.printHeight = printHeight;
            this.printWidth = printWidth;
            this.vendorId = vendorId;
            this.productId = productId;
            this.readEndpointId = readEndpointId;
            this.writeEndpointId = writeEndpointId;
            this.device = device;
            this.pcscHelper = pcscHelper;
            this.calibrationStrategyFactory = calibrationStrategyFactory;
        }

        public void Connect()
        {
            device.Connect(vendorId, productId, readEndpointId, writeEndpointId);
            EnableStatusReporting(true, true);
        }

        public void Dispose()
        {
            device.Disconnect();
        }

        public PrinterResponse SendRawCommand(string command)
        {
            if (!string.IsNullOrEmpty(command))
            {
                logger.LogDebug($"Sending raw command: {command}");
            }

            return device.Transceive(command);
        }

        public void DisplayMessage(string message, bool confirm)
        {
            logger.LogDebug($"Displaying LCD message: {message}");
            SendAndValidateResponse($"Clcd;Tmsg;{message}{(confirm ? ";OK" : "")}", "Display message");
        }

        public void ClearDisplay()
        {
            logger.LogDebug("Clearing LCD display");
            SendAndValidateResponse("Clcd;Tmsg;OK", "Clear display");
        }

        public bool CheckDisplayReady()
        {
            logger.LogDebug("Checking if LCD display is ready");
            return device.Transceive("Clcd;Echo;ready", "Check if display is ready").Text.Equals("ready", StringComparison.InvariantCultureIgnoreCase);
        }

        public void ClearCurrentPrinterStatus()
        {
            logger.LogDebug("Clearing current printer status");
            SendAndValidateResponse("Scs", "Clear current printer status");
        }

        public PrinterIdentity ReadIdentity()
        {
            logger.LogDebug("Reading printer identity information");
            return new PrinterIdentity
            {
                Model = device.Transceive("Rtp", "Read printer type").Text,
                FirmwareRevision = device.Transceive("Rfv", "Read firmware version").Text,
                BoardRevision = device.Transceive("Rcpu", "Read CPU serial number").Text,
                Serial = device.Transceive("Rsn", "Read printer serial number").Text
            };
        }

        public void UnlockPrinter()
        {
            logger.LogDebug("Unlocking printer");
            SendAndValidateResponse("Clcd;Tmsg;0", "Clear display");
            SendAndValidateResponse("Clock;Unlock", "Unlock printer");
        }

        /// <summary>
        /// Original implementation deals mainly with evaluation of string contained logical expression.
        /// Way how the status is obtained is by calling `rawStatus = _ioCom.getStatus();` which is nothing more than 2x read operation:
        ///   read(); // to force status synch
        ///   return read(); 
        /// </summary>
        /// <returns></returns>
        public PrinterStatusCheck CheckPrinterStatus()
        {
            logger.LogDebug("Checking printer status");
            SendRawCommand(null);
            return SendRawCommand(null).CheckStatus();
        }

        public int PrintsTillNextCleaning()
        {
            logger.LogDebug("Reading PrintsTillNextCleaning counter");
            return ReadPrinterCounter("Rco;rc", "Prints till next cleaning");
        }

        public int AdvancedCleaningCount()
        {
            logger.LogDebug("Reading AdvancedCleaningCount counter");
            return ReadPrinterCounter("Rco;cnac", "Advanced cleaning count");
        }

        public int AverageCleaningCount()
        {
            logger.LogDebug("Reading AverageCleaningCount counter");
            return ReadPrinterCounter("Rco;ga", "Average cleaning count");
        }

        public void SelfAdjust()
        {
            logger.LogDebug("Sending printer self-adjust");
            device.Transceive("Sa", "Printer self-adjust"); // the printer doesn't seem to return anything back for the 'Sa' command
        }

        private int ReadPrinterCounter(string command, string comment = null)
        {
            var response = device.Transceive(command, comment).Text;
            if (!int.TryParse(response, out var v))
            {
                throw new NumberExpectedPrinterResponseException(command, response);
            }
            return v;
        }

        public bool VerifyChipContact()
        {
            logger.LogDebug("Checking if we have a contact with the chip");
            return pcscHelper.ProbeReader(0, null, 0);
        }

        /// <summary>
        /// Perform calibration of CS. If called from job, card is expected to be in a contact station, therefore the calibration starts and ends with card in that position.
        /// Otherwise a standalone calibration is executed (separating a card and rejecting it afterwards).
        /// </summary>
        /// <param name="standalone">If there is a job being processed, this method expects that card is in a contact station with failed ATR at current position.</param>
        /// <param name="parameters">Calibration Parameters (e.g. Maximum Calibration Time)</param>>
        public void CalibrateCS(bool standalone, CalibrationParameters parameters = null)
        {
            try
            {
                var calibrationMode = standalone ? CalibrationMode.Linear : config.CSCalibrationMode;
                logger.LogDebug($"Calibrating the smartcard reader offset using {calibrationMode} mode");
                
                ICalibrationStrategy cal = calibrationStrategyFactory.GetCalibrationStrategy(calibrationMode);
                if (cal == null)
                {
                    logger.LogError($"Cannot create calibration strategy for mode: {calibrationMode:G}.");
                    if (standalone)
                    {
                        throw new UnknownCalibrationStrategyException(calibrationMode);
                    }
                    else
                    {
                        return;
                    }
                }

                if (standalone)
                {
                    // separate a card (it will be rejected when done)
                    SendAndValidateResponse("Rok");
                    SendAndValidateResponse("Clcd;Tmsg;SMART_CARD_OFFSET_ADJ", "Display message");
                    SendAndValidateResponse("Sis", "Move card into contact encoder");
                }

                PrinterResponse response = device.Transceive("Ros", "Read smartcard offset");
                string Ros_Backup = response.Text.Trim();
                if (string.IsNullOrEmpty(Ros_Backup))
                {
                    // if not received then set to 410 as a fallback
                    Ros_Backup = "410";
                }

                if (!int.TryParse(Ros_Backup, out int RosDefault))
                {
                    RosDefault = 410;
                }

                response = device.Transceive("Ros;S", "Read module's smart offset");
                string Ros_S_Backup = response.Text.Trim();

                var maxAllowedTime = config.CSCalibrationAllowedTime;
                if (parameters?.MaxAllowedTime != null && parameters.MaxAllowedTime > TimeSpan.Zero)
                {
                    maxAllowedTime = parameters.MaxAllowedTime.Value;
                }

                int att = 1;
                Stopwatch sw = cal.Initialize(RosDefault, config.CSCalibrationRangeMinimum, config.CSCalibrationRangeMaximum, maxAllowedTime, !standalone);

                while (cal.Status == CalibrationStatus.Sampling)
                {
                    int nextPosition = cal.NextSamplingPosition;
                    SendAndValidateResponse("Pos;=;" + nextPosition, "Set smartcard contact offset position");
                    SendAndValidateResponse("Sib", "Move card back from contact encoder");
                    pcscHelper.ResetCardInsertedEvent();
                    SendAndValidateResponse("Sis;500;D;45", "Move card into contact encoder without ESD");

                    cal.SamplingResult(nextPosition, pcscHelper.ProbeReader(nextPosition, sw, att++));
                }

                // finally, restore original values, remove LCD message
                SendAndValidateResponse("Pos;S;=;" + Ros_S_Backup, "Set module's smartcard offset");
                SendAndValidateResponse("Pos;=;" + Ros_Backup, "Set smartcard offset");

                // store newly found values if appropriate
                if (cal.Status == CalibrationStatus.Result)
                {
                    if (cal.NewDefaultPositionRecommended) // calibration outcome recommends to set new default position
                    {
                        // store new default offset and move card over there
                        SendAndValidateResponse("Pos;=;" + cal.NewDefaultPosition, "Set smartcard offset");
                        // Move back the card from last smart card position
                        SendAndValidateResponse("Sib", "Move card back from contact encoder");
                        // Move the card to the new offset position
                        SendAndValidateResponse("Sis;500;D;45", "Move card into contact encoder without ESD");
                    }

                    // Otherwise just leave the card where it is (for stochastic strategy this will leave the card at first working position so it should encode even with no adjusted default offset).
                }

                if (cal.Status == CalibrationStatus.Failed)
                {
                    logger.LogError($"Calibration attempt failed: {cal.StatusDescription}.");
                }
            }
            catch (Exception e)
            {
                logger.LogError(e, $"Calibration failed due to error: {e.Message}");
                throw;
            }
            finally
            {
                if (standalone) // remove card if standalone, otherwise return to personalization step 
                {
                    SendAndValidateResponse("Clcd;Tmsg;0", "Clear display");
                    MoveToRejectBox();
                }
            }
        }

        public void Reset()
        {
            logger.LogDebug("Resetting the printer");
            SendAndValidateResponse("Srs", "Reset printer");
        }

        public void ResetUserPin(int userId, string pin)
        {
            logger.LogDebug($"Resetting PIN for user {userId}");
            SendAndValidateResponse($"Puser;{userId};{pin}", "Reset user pin");
            SendAndValidateResponse($"Puser;{userId};{pin};0", "Reset user pin failures");
        }

        public void MoveToSmartcardReader(bool contactless = false)
        {
            logger.LogDebug($"Transporting the card to the contact{(contactless ? "less" : "")} smartcard reader");
            SendAndValidateResponse(contactless ? "Sic" : "Sis", $"Move card to contact{(contactless ? "less" : "")} encoder");
        }

        public void MoveToOutputBox()
        {
            logger.LogDebug("Ejecting the card");
            SendAndValidateResponse("Se", "Eject card");
        }

        public void MoveToRejectBox()
        {
            logger.LogDebug("Rejecting the card");
            SendAndValidateResponse("Ser", "Reject card");
        }

        public void FlipCard(bool front)
        {
            SendAndValidateResponse(front ? "Sr" : "Sv", $"Flip card to {(front ? "front": "back")}");
        }

        public void UpgradeFirmware(byte[] firmware)
        {
            var firmwareCommand = Encoding.ASCII.GetBytes($"Df;{firmware.Length};")
                .Concat(firmware)
                .Append((byte) ';')
                .Append(Crc(firmware))
                .ToArray();

            PrinterResponse response = device.Transceive(firmwareCommand);
            ValidateCommandResponse(response);
        }
        
        public ActionResult<List<PrinterAdvancedSetting>> GetAdvancedSettings()
        {
            logger.LogDebug("Reading printer advanced settings");
            
            var (contrastColor, contrastBlack) = ReadContrastValues("Rc;a", "Read contrast values");
            var (luminosityColor, luminosityBlack) = ReadContrastValues("Rl;a", "Read luminosity values");
            var jitterEnabled = device.Transceive("Rkine;M", "Read jitter print").Text.ToUpper().FirstOrDefault() == 'E';
            var jitterPasses = ReadPrinterCounter("Rkine;P", "Read number of jitter passes");
            var jitterFlags = ReadPrinterCounter("Rkine;F", "Read jitter flags");
            var jitterBlackDitheringMode = ReadPrinterCounter("Rkine;K", "Read black dithering mode");

            return new List<PrinterAdvancedSetting> {
                new PrinterAdvancedSetting {
                    Id = PrinterSettingsConstants.ContrastColorSettingId,
                    FriendlyName = "ContrastColor",
                    CurrentValue = contrastColor
                },
                new PrinterAdvancedSetting {
                    Id = PrinterSettingsConstants.ContrastBlackSettingId,
                    FriendlyName = "ContrastBlack",
                    CurrentValue = contrastBlack
                },
                new PrinterAdvancedSetting {
                    Id = PrinterSettingsConstants.LuminosityColorSettingId,
                    FriendlyName = "LuminosityColor",
                    CurrentValue = luminosityColor
                },
                new PrinterAdvancedSetting {
                    Id = PrinterSettingsConstants.LuminosityBlackSettingId,
                    FriendlyName = "LuminosityBlack",
                    CurrentValue = luminosityBlack
                },
                new PrinterAdvancedSetting {
                    Id = PrinterSettingsConstants.JitterEnabledSettingId,
                    FriendlyName = "JitterEnabled",
                    CurrentValue = jitterEnabled ? 1 : 0
                },
                new PrinterAdvancedSetting {
                    Id = PrinterSettingsConstants.JitterPassesSettingId,
                    FriendlyName = "JitterPasses",
                    CurrentValue = jitterPasses
                },
                new PrinterAdvancedSetting {
                    Id = PrinterSettingsConstants.JitterFlagsSettingId,
                    FriendlyName = "JitterFlags",
                    CurrentValue = jitterFlags
                },
                new PrinterAdvancedSetting {
                    Id = PrinterSettingsConstants.JitterBlackDitheringModeSettingId,
                    FriendlyName = "JitterBlackDitheringMode",
                    CurrentValue = jitterBlackDitheringMode
                },
            };
        }

        public RibbonInfo GetRibbonInfo()
        {
            logger.LogDebug("Reading ribbon info");
            
            string type = device.Transceive("Rr", "Read ribbon type").Text;
            string partNumber = device.Transceive("Rrt;ref", "Read ribbon product code").Text;
            int ribbonRemaining = int.Parse(device.Transceive("Rrt;progress", "Read number of prints remaining").Text);

            return new RibbonInfo {
                Type = type,
                PartNumber = partNumber,
                PercentRemaining = ribbonRemaining
            };
        }

        public void SetAdvancedSetting(int id, int value)
        {
            switch (id)
            {
                case PrinterSettingsConstants.ContrastColorSettingId:
                    logger.LogDebug($"Setting color contrast to {value}");
                    SendAndValidateResponse($"Pc;a;=;{value}", "Set color contrast");
                    break;

                case PrinterSettingsConstants.ContrastBlackSettingId:
                    logger.LogDebug($"Setting monochrome contrast to {value}");
                    SendAndValidateResponse($"Pc;k;=;{value}", "Set monochrome contrast");
                    break;

                case PrinterSettingsConstants.LuminosityColorSettingId:
                    logger.LogDebug($"Setting color luminosity to {value}");
                    SendAndValidateResponse($"Pl;a;=;{value}", "Set color luminosity");
                    break;

                case PrinterSettingsConstants.LuminosityBlackSettingId:
                    logger.LogDebug($"Setting monochrome luminosity to {value}");
                    SendAndValidateResponse($"Pl;k;=;{value}", "Set monochrome luminosity");
                    break;

                case PrinterSettingsConstants.JitterEnabledSettingId:
                    logger.LogDebug($"Setting jitter enabled to {value}");
                    SendAndValidateResponse($"Pkine;{(value == 0 ? "D" : "E")}", $"{(value == 0 ? "Disable" : "Enable")} jitter print");
                    break;

                case PrinterSettingsConstants.JitterPassesSettingId:
                    logger.LogDebug($"Setting jitter passes to {value}");
                    SendAndValidateResponse($"Pkine;P;{value}", "Set number of jitter passes");
                    break;

                case PrinterSettingsConstants.JitterFlagsSettingId:
                    logger.LogDebug($"Setting jitter flags to {value}");
                    SendAndValidateResponse($"Pkine;F;{value}", "Set jitter flags");
                    break;

                case PrinterSettingsConstants.JitterBlackDitheringModeSettingId:
                    logger.LogDebug($"Setting black dithering mode to {value}");
                    SendAndValidateResponse($"Pkine;K;{value}", "Set black dithering mode");
                    break;

                default:
                    throw new UnknownAdvancedSettingIdException(id);
            }
        }

        public void EnableStatusReporting(bool enableStatus, bool enableExtensionData)
        {
            logger.LogDebug("Enabling printer status reporting");
            SendAndValidateResponse($"Pps;{(enableStatus ? "1" : "0")};{(enableExtensionData ? "1" : "0")}");
        }

        private void SendAndValidateResponse(string command, string comment = null)
        {
            PrinterResponse response = device.Transceive(command, comment);
            ValidateCommandResponse(response);
        }

        private void SendAndValidateResponse(byte[] command)
        {
            PrinterResponse response = device.Transceive(command);
            ValidateCommandResponse(response);
        }

        private void ValidateCommandResponse(PrinterResponse response)
        {
            if (!string.Equals(response.Text, "OK", StringComparison.InvariantCultureIgnoreCase) && config.PrinterCommValidateOkResponse)
            {
                throw new UnexpectedPrinterResponseException($"Expected 'OK' but got '{response.Text}'", response);
            }
        }

        /* Mosaic code does send the image as bytes in a buffer, afterwards sends 'sync' buffer of 1 byte of 0 value in a byte[].
              dos = new DataOutputStream(new FileOutputStream(fileDevIn));
              dos.write(buffer);
              dos.flush();
              byte[] sync = new byte[] { 0 };
              dos.write(sync);
              dos.flush();
              dos.close(); 
         */
        public void WriteImage(PrintImage panel, IDictionary<string, string> settings)
        {
            try
            {
                // Always try to decode image from PNG, JPG...
                using (Image<Rgb24> image = SixLabors.ImageSharp.Image.Load<Rgb24>(panel.Value))
                {
                    if (!((image.Height == printHeight && image.Width == printWidth) || (panel.RotateResize && image.Height == printWidth && image.Width == printHeight)))
                    {
                        throw new InvalidImageResolutionException(image.Height, image.Width, printHeight, printWidth, panel.Type);
                    }

                    if (panel.Type == PrintImageType.RGB24 || panel.Type == PrintImageType.RGB32) 
                    {   // create color image
                        var sharpeningMode = config.DefaultSharpeningMode;
                        if (settings?.ContainsKey(PrinterSettingsConstants.SharpeningMode) == true)
                        {
                            var sharpeningModeStr = settings[PrinterSettingsConstants.SharpeningMode];
                            if (!Enum.TryParse(sharpeningModeStr, true, out sharpeningMode))
                            {
                                logger.LogWarning($"Unknown sharpening mode '{sharpeningModeStr}' -> using default sharpening mode '{sharpeningMode:G}'");
                            }
                        }

                        var includePida = config.IncludePida;
                        if (settings?.ContainsKey(PrinterSettingsConstants.Pida) == true)
                        {
                            var includePidaStr = settings[PrinterSettingsConstants.Pida];
                            if (!bool.TryParse(includePidaStr, out includePida))
                            {
                                logger.LogWarning($"Invalid bool Pida value '{includePidaStr}' -> using default '{includePida}'");
                            }
                        }

                        var useColorProfile = false;
                        if (settings?.ContainsKey(PrinterSettingsConstants.UseColorProfile) == true)
                        {
                            var useColorProfileStr = settings[PrinterSettingsConstants.UseColorProfile];
                            if (!bool.TryParse(useColorProfileStr, out useColorProfile))
                            {
                                logger.LogWarning($"Invalid bool UseColorProfile value '{useColorProfileStr}' -> using default '{useColorProfile}'");
                            }
                        }

                        CMYImageData printerEncodedImage = EvolisImageCodec.ConvertColorImage(image, panel.RotateResize, sharpeningMode, useColorProfile ? config.ColorProfileConfig : null);
                        byte[] printerImage = printerEncodedImage.GetRaster(includePida);
                        // write printerImage to printer...
                        SendAndValidateResponse(printerImage);
                        SendAndValidateResponse(new byte[]{0});
                        SendAndValidateResponse("Sp;y");
                        SendAndValidateResponse("Sp;m");
                        SendAndValidateResponse("Sp;c");
                    }
                    else
                    {   // create monochromatic image - panel type is one of PrintImageType.K, PrintImageType.Overlay, ...
                        byte threshold = 128;
                        if (settings?.ContainsKey(PrinterSettingsConstants.MonochromeThreshold) == true)
                        {
                            var thresholdStr = settings[PrinterSettingsConstants.MonochromeThreshold];
                            if (!byte.TryParse(thresholdStr, out threshold))
                            {
                                logger.LogWarning($"Invalid monochrome threshold value '{thresholdStr}' (expected value between 0 and 255) -> using default '{threshold}'");
                            }
                        }

                        MonochromeImageData printerEncodedImage = EvolisImageCodec.ConvertMonoImage(image, panel.Type, panel.RotateResize, threshold);
                        byte[] printerImage = printerEncodedImage.GetRaster();
                        // write printerImage to printer...
                        SendAndValidateResponse(printerImage);
                        SendAndValidateResponse(new byte[]{0});
                        SendAndValidateResponse($"Sp;{printerEncodedImage.Type}");
                    }
                }
            }
            catch (InvalidImageResolutionException)
            {
                throw;
            }
            catch (ImageFormatException e)
            {
                throw new UnableToProcessPrintImageException(e, $"Unable to load panel type '{panel.Type}' from {panel.Value.Length} bytes of data: {e.Message}");
            }
            catch (Exception e)
            {
                throw new UnableToProcessPrintImageException(e, $"Unable to print image: {e.Message}");
            }
        }

        public void WriteMagstripeTracks(Magstripe dataset)
        {
            try
            {
                SendAndValidateResponse("Plmr;3;1");
                SendAndValidateResponse("Pmc;h;0");
                SendAndValidateResponse("Psmgr;2");
                SendAndValidateResponse("Psod;d");
                SendAndValidateResponse("Pc;o;=;10");
                SendAndValidateResponse("Pl;o;=;10");
                SendAndValidateResponse("Ss");
                SendAndValidateResponse("Sr");

                WriteMagstripeTrack(1, dataset.Data.Track1);
                WriteMagstripeTrack(2, dataset.Data.Track2);
                WriteMagstripeTrack(3, dataset.Data.Track3);

                SendAndValidateResponse("Smw", "Write magstripe tracks");

                // check2 - "AND:!ERR_FEEDER_ERROR,!ERR_COVER_OPEN,!ERR_MECHANICAL,!ERR_BLANK_TRACK,!ERR_MAGNETIC_DATA,!ERR_READ_MAGNETIC,!ERR_WRITE_MAGNETIC"
                PrinterStatusCheck statusCheck = CheckPrinterStatus();
                statusCheck.NotError(ErrorStatusFlag.ERR_FEEDER_ERROR)
                           .NotError(ErrorStatusFlag.ERR_COVER_OPEN)
                           .NotError(ErrorStatusFlag.ERR_MECHANICAL)
                           .NotError(ErrorStatusFlag.ERR_BLANK_TRACK)
                           .NotError(ErrorStatusFlag.ERR_MAGNETIC_DATA)
                           .NotError(ErrorStatusFlag.ERR_READ_MAGNETIC)
                           .NotError(ErrorStatusFlag.ERR_WRITE_MAGNETIC);
            }
            catch (Exception)
            {
                // send 'Se' command on error - open question is whether it needs to be sent twice (see EncodeMagstripe() in Mosaic PS)
                SendAndValidateResponse("Se", "Eject card");
                SendAndValidateResponse("Se", "Eject card");

                throw;
            }
        }

        private void WriteMagstripeTrack(int trackNumber, string track)
        {
            if (!string.IsNullOrEmpty(track))
            {
                logger.LogDebug($"Sending magstripe track {trackNumber}");
                SendAndValidateResponse($"Dm;{trackNumber};{track}", $"Download magstripe track {trackNumber}");
            }
            else
            {
                logger.LogDebug($"Skipping magstripe track {trackNumber}: no magstripe data for this track");
            }
        }

        private (int, int) ReadContrastValues(string command, string comment = null)
        {
            var response = device.Transceive(command, comment).Text;
            var values = response.Split();

            if (values.Length >= 4)
            {
                if (!int.TryParse(values[0], out var contrastColor))
                {
                    throw new InvalidContrastOrLuminosityValueException(command, values[0]);
                }

                if (!int.TryParse(values[3], out var contrastBlack))
                {
                    throw new InvalidContrastOrLuminosityValueException(command, values[3]);
                }

                return (contrastColor, contrastBlack);
            }
            else
            {
                throw new ContrastOrLuminosityPrinterResponseTooShortException(command);
            }
        }

        private static byte Crc(IEnumerable<byte> data)
        {
            byte crc = 0x00;
            foreach (byte t in data)
            {
                crc ^= t;
            }

            return crc;
        }
    }
}
