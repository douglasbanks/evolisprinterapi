﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EvolisPrinterApi.Contracts.Models;
using EvolisPrinterApi.Exceptions;
using EvolisPrinterApi.Util;
using LibUsbDotNet;

namespace EvolisPrinterApi.ApplicationClasses.Drivers
{
    public class PrinterResponse
    {
        // error as returned from USB reading
        protected Error UsbError;

        protected string UsbErrorDescription;

        // raw message data as received
        protected byte[] Buffer;

        // number of bytes received
        protected int Received;

        protected string TextPart;
        public string Text => TextPart;

        protected Exception ParsingException = null;
        public Exception ResponseError => ParsingException;

        protected Exception ReceiveException = null;
        public Exception ReceiveError => ReceiveException;

        private int polls;
        public int PollCount => polls;

        private TimeSpan took;
        public TimeSpan CallDuration => took;

        private uint sessionId;
        public uint SessionId => sessionId;

        public ISet<ConfigStatusFlag> ConfigFlags { get; } = new HashSet<ConfigStatusFlag>();

        public ISet<InfoStatusFlag> InfoFlags { get; } = new HashSet<InfoStatusFlag>();

        public ISet<WarningStatusFlag> WarningFlags { get; } = new HashSet<WarningStatusFlag>();

        public ISet<ErrorStatusFlag> ErrorFlags { get; } = new HashSet<ErrorStatusFlag>();

        public ISet<Extension1StatusFlag> Extension1Flags { get; } = new HashSet<Extension1StatusFlag>();

        public ISet<Extension2StatusFlag> Extension2Flags { get; } = new HashSet<Extension2StatusFlag>();

        public PrinterResponse(byte[] rawBuffer, int receivedSize, Error usbError, Exception ex = null)
        {
            UsbError = usbError;
            UsbErrorDescription = usbError.ToString();
            Buffer = rawBuffer.Take(receivedSize).ToArray();
            Received = receivedSize;
            ParsingException = ParseAndValidate(false);
            ReceiveException = ex;
            polls = 0;
            took = TimeSpan.Zero;
        }

        /// <summary>
        /// Parse and Validate response, throws exception on invalid response.
        /// </summary>
        public Exception ParseAndValidate(bool raiseError = false)
        {
            ParsingException = null;

            try
            {
                // set the text part at least (even if it is going to be empty)
                byte[] textPartBytes = Buffer.TakeWhile(b => b != 0).ToArray();
                TextPart = Encoding.ASCII.GetString(textPartBytes);

                if (UsbError != Error.Success)
                {
                    return ParsingException = new InvalidPrinterResponseException($"UsbError {UsbError} ({UsbErrorDescription})");
                }

                if (Buffer.Length == 0)
                {
                    return ParsingException = new InvalidPrinterResponseException("Valid response size cannot be 0.");
                }

                if (Buffer.Length < textPartBytes.Length + 1 || Buffer[textPartBytes.Length] != 0)
                {
                    return ParsingException = new InvalidPrinterResponseException($"Response is missing trailing zero after text part. Text part length: {textPartBytes.Length}, response length: {Buffer.Length}.");
                }

                int parsePosition = textPartBytes.Length + 1; // past the ending zero

                if (!TryParse(out uint textSizeCheckValue, 2, ref parsePosition, ByteOrder.LSB))
                {
                    return ParsingException = new InvalidPrinterResponseException($"Response is missing text response length after text part. Position: {parsePosition}, response length: {Buffer.Length}.");
                }

                if (textSizeCheckValue != TextPart.Length)
                {
                    return ParsingException = new InvalidPrinterResponseException($"Response text part length is different from announced value. Parsed length: {TextPart.Length}, specified length: {textSizeCheckValue}.");
                }

                if (!TryParse(out uint statusChainLength, 1, ref parsePosition, ByteOrder.MSB))
                {
                    return ParsingException = new InvalidPrinterResponseException($"Response is missing status chain length part. Position: {parsePosition}, response length: {Buffer.Length}.");
                }

                if (statusChainLength != 16)
                {
                    return ParsingException = new InvalidPrinterResponseException($"Response status chain length has unexpected value. Parsed length: {statusChainLength}, expected length: {16}.");
                }

                if (!TryParse(out uint configurationBits, 4, ref parsePosition, ByteOrder.MSB))
                {
                    return ParsingException = new InvalidPrinterResponseException($"Response is missing configuration bits (32bit block). Position: {parsePosition}, response length: {Buffer.Length}.");
                }

                // parse Configuration flags
                foreach (ConfigStatusFlag flag in Enum.GetValues(typeof(ConfigStatusFlag)))
                {
                    if (((uint)flag & configurationBits) != 0)
                    {
                        ((HashSet<ConfigStatusFlag>)ConfigFlags).Add(flag);
                    }
                }

                if (!TryParse(out uint informationBits, 4, ref parsePosition, ByteOrder.MSB))
                {
                    return ParsingException = new InvalidPrinterResponseException($"Response is missing information bits (32bit block). Position: {parsePosition}, response length: {Buffer.Length}.");
                }

                // parse Information flags
                foreach (InfoStatusFlag flag in Enum.GetValues(typeof(InfoStatusFlag)))
                {
                    if (((uint)flag & informationBits) != 0)
                    {
                        ((HashSet<InfoStatusFlag>)InfoFlags).Add(flag);
                    }
                }

                if (!TryParse(out uint warningBits, 4, ref parsePosition, ByteOrder.MSB))
                {
                    return ParsingException = new InvalidPrinterResponseException($"Response is missing warning bits (32bit block). Position: {parsePosition}, response length: {Buffer.Length}.");
                }

                // parse Warning flags
                foreach (WarningStatusFlag flag in Enum.GetValues(typeof(WarningStatusFlag)))
                {
                    if (((uint)flag & warningBits) != 0)
                    {
                        ((HashSet<WarningStatusFlag>)WarningFlags).Add(flag);
                    }
                }

                if (!TryParse(out uint errorBits, 4, ref parsePosition, ByteOrder.MSB))
                {
                    return ParsingException = new InvalidPrinterResponseException($"Response is missing error bits (32bit block). Position: {parsePosition}, response length: {Buffer.Length}.");
                }

                // parse Error flags
                foreach (ErrorStatusFlag flag in Enum.GetValues(typeof(ErrorStatusFlag)))
                {
                    if (((uint)flag & errorBits) != 0)
                    {
                        ((HashSet<ErrorStatusFlag>)ErrorFlags).Add(flag);
                    }
                }

                if (!TryParse(out uint sessionIdSize, 1, ref parsePosition, ByteOrder.MSB))
                {
                    return ParsingException = new InvalidPrinterResponseException($"Response is missing session ID size. Position: {parsePosition}, response length: {Buffer.Length}.");
                }

                if (sessionIdSize != 2)
                {
                    return ParsingException = new InvalidPrinterResponseException($"Response session ID length has unexpected value. Parsed length: {sessionIdSize}, expected length: {2}.");
                }

                if (!TryParse(out sessionId, 2, ref parsePosition, ByteOrder.MSB))
                {
                    return ParsingException = new InvalidPrinterResponseException($"Response is missing session ID. Position: {parsePosition}, response length: {Buffer.Length}.");
                }

                if (TryParse(out uint extension1DataSize, 1, ref parsePosition, ByteOrder.MSB))
                {
                    if (extension1DataSize == 4 && ConfigFlags.Contains(ConfigStatusFlag.CFG_EXTENSION_1))
                    {
                        TryParse(out uint extension1DataValue, 4, ref parsePosition, ByteOrder.MSB);

                        // parse data/flags
                        foreach (Extension1StatusFlag flag in Enum.GetValues(typeof(Extension1StatusFlag)))
                        {
                            if (((uint) flag & extension1DataValue) != 0)
                            {
                                ((HashSet<Extension1StatusFlag>) Extension1Flags).Add(flag);
                            }
                        }
                    }
                }

                if (Extension1Flags.Contains(Extension1StatusFlag.CFG_EXTENSION_2) && TryParse(out uint extension2DataSize, 1, ref parsePosition, ByteOrder.MSB))
                {
                    if (extension2DataSize == 4)
                    {
                        TryParse(out uint extension2DataValue, 4, ref parsePosition, ByteOrder.MSB);

                        // parse data/flags
                        foreach (Extension2StatusFlag flag in Enum.GetValues(typeof(Extension2StatusFlag)))
                        {
                            if (((uint) flag & extension2DataValue) != 0)
                            {
                                ((HashSet<Extension2StatusFlag>) Extension2Flags).Add(flag);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ParsingException = ex;
            }
            finally
            {
                if (raiseError && ParsingException != null)
                {
                    throw ParsingException;
                }
            }

            return ParsingException;
        }

        /// <summary>
        /// Parse block of sectionSize consecutive bytes starting (including) Buffer[parsePosition]. Shifts parsePosition by section size on success.
        /// </summary>
        /// <param name="value">decoded value</param>
        /// <param name="sectionSize">1, 2 or 4 bytes</param>
        /// <param name="parsePosition">index of first byte in a Buffer where section starts (inclusive)</param>
        /// <param name="byteOrder"></param>
        /// <returns></returns>
        private bool TryParse(out uint value, int sectionSize, ref int parsePosition, ByteOrder byteOrder)
        {
            value = 0;
            if (Buffer.Length <= parsePosition + sectionSize - 1) return false;

            for (int i = 0; i < sectionSize; ++i)
            {
                if (byteOrder == ByteOrder.LSB)
                {
                    value |= ((uint) Buffer[parsePosition + i]) << (i * 8);
                }
                else
                {
                    value <<= 8;
                    value |= Buffer[parsePosition + i];
                }
            }

            parsePosition += sectionSize;

            return true;
        }


        public override string ToString()
        {
            return $"PrinterStatus: Error={UsbError:G}({UsbError:D}), Received={Received}, Text={TextPart}, Polls={PollCount}, Duration={CallDuration}, Binary={BinaryConversion.HexTrace(Buffer)}";
        }

        /// <summary>
        /// Return status checker for this response.
        /// </summary>
        /// <returns></returns>
        public PrinterStatusCheck CheckStatus()
        {
            return new PrinterStatusCheck(this);
        }

        public void AddCommTelemetryInfo(int polls, TimeSpan swElapsed)
        {
            this.polls = polls;
            this.took = swElapsed;
        }
    }

    // ReSharper disable InconsistentNaming
    internal enum ByteOrder
    {
        LSB,
        MSB
    }
}