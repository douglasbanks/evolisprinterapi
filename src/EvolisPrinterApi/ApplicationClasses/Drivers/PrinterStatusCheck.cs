﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using EvolisPrinterApi.Contracts.Models;
using EvolisPrinterApi.Exceptions;

namespace EvolisPrinterApi.ApplicationClasses.Drivers
{
    /// <summary>
    /// Check actual printer status (obtained in every response) against expectations.
    /// Currently (in Mosaic Print Service) we use the following three combinations:
    /// 1. "AND:!ERR_FEEDER_ERROR,!ERR_COVER_OPEN,!ERR_MECHANICAL,!ERR_BLANK_TRACK,!ERR_MAGNETIC_DATA,!ERR_READ_MAGNETIC,!ERR_WRITE_MAGNETIC"
    /// 2. "AND:!ERR_FEEDER_ERROR,!ERR_RIBBON_ERROR,!ERR_COVER_OPEN,!ERR_MECHANICAL,!ERR_BAD_RIBBON,!ERR_RIBBON_ENDED"
    /// 3. "AND:CFG_MAGNETIC,!INF_BUSY,!DEF_PRINTER_LOCKED,!DEF_COVER_OPEN,!DEF_FEEDER_EMPTY,!DEF_HOPPER_FULL,!DEF_REJECT_BOX_FULL,!DEF_NO_RIBBON,!DEF_RIBBON_ENDED,!DEF_UNSUPPORTED_RIBBON"
    /// which means we need to check at least these flag groups:
    /// - ErrorFlags: ERR_FEEDER_ERROR, ERR_COVER_OPEN, ERR_MECHANICAL, ERR_BLANK_TRACK, ERR_MAGNETIC_DATA, ERR_READ_MAGNETIC, ERR_WRITE_MAGNETIC, ERR_RIBBON_ERROR, ERR_BAD_RIBBON, ERR_RIBBON_ENDED
    /// - ConfigFlags: CFG_MAGNETIC
    /// - InfoFlags: INF_BUSY
    /// - WarningFlags: DEF_PRINTER_LOCKED, DEF_PRINTER_LOCKED, DEF_COVER_OPEN, DEF_FEEDER_EMPTY, DEF_HOPPER_FULL, DEF_REJECT_BOX_FULL, DEF_NO_RIBBON, DEF_RIBBON_ENDED, DEF_UNSUPPORTED_RIBBON
    /// It is therefore possible to check using fluent approach using this helper.
    /// </summary>
    public class PrinterStatusCheck
    {
        private PrinterResponse printerStatus;

        private bool currentStatusCheckState = true;

        public PrinterStatusCheck(PrinterResponse status)
        {
            this.printerStatus = status;
        }

        public bool Result()
        {
            return currentStatusCheckState;
        }

        public PrinterStatus GetPrinterStatus()
        {
            var status = new PrinterStatus {
                InfoFlags = printerStatus.InfoFlags,
                WarningFlags = printerStatus.WarningFlags,
                ErrorFlags = printerStatus.ErrorFlags,
                ConfigFlags = printerStatus.ConfigFlags
            };

            if (printerStatus.ConfigFlags.Contains(ConfigStatusFlag.CFG_LCD))
            {
                status.LcdDetected = true;
            }
            if (printerStatus.InfoFlags.Contains(InfoStatusFlag.INF_BUSY))
            {
                status.PrinterBusy = true;
            }
            if (printerStatus.WarningFlags.Contains(WarningStatusFlag.DEF_PRINTER_LOCKED))
            {
                status.PrinterNotLocked = true;
            }
            if (printerStatus.WarningFlags.Contains(WarningStatusFlag.DEF_COVER_OPEN) || printerStatus.ErrorFlags.Contains(ErrorStatusFlag.ERR_COVER_OPEN))
            {
                status.PrinterCoverOpen = true;
            }
            if (printerStatus.WarningFlags.Contains(WarningStatusFlag.DEF_FEEDER_EMPTY))
            {
                status.CardFeederEmpty = true;
            }
            if (printerStatus.WarningFlags.Contains(WarningStatusFlag.DEF_HOPPER_FULL))
            {
                status.HopperFull = true;
            }
            if (printerStatus.WarningFlags.Contains(WarningStatusFlag.DEF_REJECT_BOX_FULL))
            {
                status.RejectBoxFull = true;
            }
            if (printerStatus.WarningFlags.Contains(WarningStatusFlag.DEF_NO_RIBBON))
            {
                status.NoRibbon = true;
            }
            if (printerStatus.WarningFlags.Contains(WarningStatusFlag.DEF_RIBBON_ENDED))
            {
                status.RibbonUsed = true;
            }
            if (printerStatus.WarningFlags.Contains(WarningStatusFlag.DEF_UNSUPPORTED_RIBBON))
            {
                status.UnsupportedRibbon = true;
            }
            if (printerStatus.ErrorFlags.Contains(ErrorStatusFlag.ERR_MECHANICAL))
            {
                status.MechanicalError = true;
            }
            if (printerStatus.ErrorFlags.Contains(ErrorStatusFlag.ERR_FEEDER_ERROR))
            {
                status.FeederError = true;
            }
            if (new HashSet<ErrorStatusFlag> {ErrorStatusFlag.ERR_BLANK_TRACK, ErrorStatusFlag.ERR_MAGNETIC_DATA, ErrorStatusFlag.ERR_READ_MAGNETIC, ErrorStatusFlag.ERR_WRITE_MAGNETIC}.Intersect(printerStatus.ErrorFlags).Any())
            {
                status.ErrorWritingMagneticStripe = true;
            }
            if (new HashSet<ErrorStatusFlag> { ErrorStatusFlag.ERR_RIBBON_ERROR, ErrorStatusFlag.ERR_BAD_RIBBON, ErrorStatusFlag.ERR_RIBBON_ENDED }.Intersect(printerStatus.ErrorFlags).Any())
            {
                status.RibbonError = true;
            }

            return status;
        }

        public void FailCheck(string flagGroupName, string flagName)
        {
            if (!currentStatusCheckState)
            {
                throw new UnexpectedPrinterStatusDetectedException(flagGroupName, flagName, printerStatus);
            }
        }

        public PrinterStatusCheck Config(ConfigStatusFlag flag)
        {
            currentStatusCheckState = currentStatusCheckState && printerStatus.ConfigFlags.Contains(flag);
            FailCheck(MethodBase.GetCurrentMethod()?.Name, flag.ToString());
            return this;
        }

        public PrinterStatusCheck Info(InfoStatusFlag flag)
        {
            currentStatusCheckState = currentStatusCheckState && printerStatus.InfoFlags.Contains(flag);
            FailCheck(MethodBase.GetCurrentMethod()?.Name, flag.ToString());
            return this;
        }

        public PrinterStatusCheck NotInfo(InfoStatusFlag flag)
        {
            currentStatusCheckState = currentStatusCheckState && !printerStatus.InfoFlags.Contains(flag);
            FailCheck(MethodBase.GetCurrentMethod()?.Name, flag.ToString());
            return this;
        }

        public PrinterStatusCheck Extension1(Extension1StatusFlag flag)
        {
            currentStatusCheckState = currentStatusCheckState && printerStatus.Extension1Flags.Contains(flag);
            FailCheck(MethodBase.GetCurrentMethod()?.Name, flag.ToString());
            return this;
        }

        public PrinterStatusCheck Extension2(Extension2StatusFlag flag)
        {
            currentStatusCheckState = currentStatusCheckState && printerStatus.Extension2Flags.Contains(flag);
            FailCheck(MethodBase.GetCurrentMethod()?.Name, flag.ToString());
            return this;
        }

        public PrinterStatusCheck NotError(ErrorStatusFlag flag)
        {
            currentStatusCheckState = currentStatusCheckState && !printerStatus.ErrorFlags.Contains(flag);
            FailCheck(MethodBase.GetCurrentMethod()?.Name, flag.ToString());
            return this;
        }

        public PrinterStatusCheck NotWarning(WarningStatusFlag flag)
        {
            currentStatusCheckState = currentStatusCheckState && !printerStatus.WarningFlags.Contains(flag);
            FailCheck(MethodBase.GetCurrentMethod()?.Name, flag.ToString());
            return this;
        }
    }
}
