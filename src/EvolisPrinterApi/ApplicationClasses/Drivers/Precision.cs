﻿using EvolisPrinterApi.ApplicationClasses.CSCalibration;
using EvolisPrinterApi.Config;
using EvolisPrinterApi.Interfaces;
using EvolisPrinterApi.Util;
using LibUsbDotNet.Main;
using Microsoft.Extensions.Logging;

// ReSharper disable InconsistentNaming
namespace EvolisPrinterApi.ApplicationClasses.Drivers
{
    /// <inheritdoc cref="IPrinterDriver" />
    /// <summary>
    /// This class is instantiated as a singleton
    /// </summary>
    public class Precision : EvolisPrinterBase
    {
        private const int PrintWidth = 1016;
        private const int PrintHeight = 648;
        private const ushort VendorId = 0x0f49;
        private const ushort ProductId = 0x0b00;
        private const ReadEndpointID ReadEndpointId = ReadEndpointID.Ep01;
        private const WriteEndpointID WriteEndpointId = WriteEndpointID.Ep02;

        public Precision(EvolisPrinterApiConfiguration config, ILogger<Precision> logger, IEvolisUsbDevice device, PCSCHelper pcscHelper, CalibrationStrategyFactory calibrationStrategyFactory)
            : base(config, logger, PrintHeight, PrintWidth, VendorId, ProductId, ReadEndpointId, WriteEndpointId, device, pcscHelper, calibrationStrategyFactory)
        {
        }
    }
}
