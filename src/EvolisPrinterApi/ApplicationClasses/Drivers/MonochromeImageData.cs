﻿using System;
using System.Collections.Generic;
using System.Linq;
using EvolisPrinterApi.Util;

namespace EvolisPrinterApi.ApplicationClasses.Drivers
{
    public class MonochromeImageData
    {
        public readonly int Width;

        public readonly int Height;

        public readonly string Type;

        private readonly byte[] kData;

        private readonly int bytesPerRow;

        public MonochromeImageData(int width, int height, string type)
        {
            Width = width;
            Height = height;
            Type = type;
            bytesPerRow = width / 8 + ((width % 8 > 0)?1:0); // Most of the time width of image should be divisible by 8 (in our case 1016 and 648 is), just in case it isn't we have to spare additional byte.
            kData = new byte[bytesPerRow * height];
        }

        public MonochromeImageData(byte[] rasterBytes, int expectedWidth, int expectedHeight)
        {
            Width = expectedWidth;
            Height = expectedHeight;
            bytesPerRow = expectedWidth / 8 + ((expectedWidth % 8 > 0)?1:0);
            kData = new byte[Height*bytesPerRow]; Array.Fill(kData, (byte)0);
            
            int position = 4; 
            while (position < rasterBytes.Length && rasterBytes[position] != 59) ++position;
            if (position > 32) position = 6;
            Type = System.Text.Encoding.UTF8.GetString(rasterBytes, 4, position-4); // TODO: error checking - case when data is malformed

            // split 1 color plane
            position = 0; // reset position when we have (guessed) a type from header
            byte[] colorPlanePrefix = Get1BitColorPlanePrefix(Type);
            if (!BinaryConversion.Contains(rasterBytes, position, colorPlanePrefix))
            {
                throw new Exception("Unexpected format of data"); // TODO: make proper exception and error message
            }
            position += colorPlanePrefix.Length;
            if (position + kData.Length >= rasterBytes.Length)
            {
                throw new Exception("Unexpected format of data"); // TODO: make proper exception and error message
            }

            for (int i = 0; i < kData.Length; ++i)
            {
                kData[i] = rasterBytes[position++];
            }

            // end of raster
            position += RasterSectionEndSequence.Length;
            if (position != rasterBytes.Length)
            {
                throw new Exception("Unexpected format of data"); // TODO: make proper exception and error message
            }
        }

        public void SetPixel(int x, int y, bool value)
        {
            x = Math.Clamp(x, 0, Width-1);
            y = Math.Clamp(y, 0, Height-1);
            int byteIndex = x / 8;
            int bitIndex = 7 - (x % 8);
            byte mask = (byte)(1 << bitIndex);
            int position = byteIndex + y * bytesPerRow;
            if (value)
            {
                kData[position] |= mask; // always sets masked bit to 1
            }
            else
            {
                kData[position] ^= (byte)(mask & kData[position]); // take only (bit) value at byteIndex.bitIndex and XOR it back => sets masked bit to 0 and preserves other bits
            }
        }
        
        public bool GetPixel(int x, int y)
        {
            x = Math.Clamp(x, 0, Width-1);
            y = Math.Clamp(y, 0, Height-1);
            int byteIndex = x / 8;
            int bitIndex = 7 - (x % 8);
            byte mask = (byte)(1 << bitIndex);
            int position = byteIndex + y * bytesPerRow;
            return (kData[position] & mask) != 0;
        }

        /// <summary>
        /// The 'y', 'm', 'c' or other valid color plane letter should be used.
        /// </summary>
        /// <param name="colorPlane"></param>
        /// <returns></returns>
        public static byte[] Get1BitColorPlanePrefix(string colorPlane)
        {
            string prefix = $"\u001bDb;{colorPlane};2;";
            byte[] returnValue = new byte[prefix.Length];
            for (int i = 0; i < prefix.Length; ++i)
            {
                returnValue[i] = (byte)prefix[i];
            }

            return returnValue;
        }

        public static readonly byte[] RasterSectionEndSequence = { 13 };

        public byte[] GetRaster()
        {
            List<byte> result = new List<byte>();

            result.AddRange(Get1BitColorPlanePrefix(this.Type));
            result.AddRange(kData.Select(i => (byte)(i & 0xFF)));
            result.AddRange(RasterSectionEndSequence); // "\r"

            return result.ToArray();
        }
    }
}