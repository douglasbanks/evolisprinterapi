﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using EvolisPrinterApi.Exceptions;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace EvolisPrinterApi.ApplicationClasses.CSCalibration
{
    public class StochasticCalibration : ICalibrationStrategy
    {
        private readonly ILogger<ICalibrationStrategy> logger;
        public CalibrationStatus Status { get; private set; }

        public string StatusDescription { get; private set; }

        public bool NewDefaultPositionRecommended { get; private set; }

        public int NewDefaultPosition { get; private set; }

        public int NextSamplingPosition { get; private set; }

        private Stopwatch sw;
        private TimeSpan timeLimit;

        private int minPos;
        private int maxPos;
        private int defaultPos;

        private readonly string offsetHistogramFile;
        private static Dictionary<int, int> _offsetHistogram = null; // holds k-v pairs of <offset, score>, initialized to 100 at default position and 1 everywhere else

        private readonly List<int> failedPositions = new List<int>(); // positions which failed during this scanning attempt
        private readonly Dictionary<int, int> candidatePositions = new Dictionary<int, int>(); // list of candidate positions for this scanning

        public StochasticCalibration(ILogger<ICalibrationStrategy> logger, bool resetPersistentHistory = false, string offsetHistogramFile = null)
        {
            this.logger = logger;
            Status = CalibrationStatus.Init;
            StatusDescription = "Initialization needed.";
            NewDefaultPositionRecommended = false;
            NewDefaultPosition = -1;
            NextSamplingPosition = -1;
            this.offsetHistogramFile = offsetHistogramFile;
            if (resetPersistentHistory)
            {
                _offsetHistogram = null;
            }
            else if (_offsetHistogram == null)
            {
                // First time constructing this object -> try to load the offset histogram from file
                LoadOffsetHistogram();
            }
        }

        public Stopwatch Initialize(int defaultPosition, int minPosition, int maxPosition, TimeSpan timeBoundary, bool failedAtDefault)
        {
            timeLimit = timeBoundary;
            defaultPos = defaultPosition;
            minPos = minPosition;
            maxPos = maxPosition;
            if (minPosition < 0 || 
                defaultPosition < minPosition || maxPosition < defaultPosition ||
                maxPosition <= minPosition || (maxPosition - minPosition) > 1000)
            {
                throw new CalibrationParametersOutOfRangeException(minPosition, defaultPosition, maxPosition);
            }

            _offsetHistogram ??= new Dictionary<int, int> {{defaultPosition, 100}};
            for (int i = minPosition; i <= maxPosition; ++i)
            {
                candidatePositions[i] = 0;
                if (!_offsetHistogram.ContainsKey(i))
                {
                    _offsetHistogram.Add(i, 1); // add all other positions as neutral
                }
            }
            
            // Remove unused keys in case the calibration range (minPosition, maxPosition) changed (see CSCalibrationRangeMinimum, CSCalibrationRangeMaximum in config)
            _offsetHistogram = _offsetHistogram.Where(pair => pair.Key >= minPosition && pair.Key <= maxPosition).ToDictionary(pair => pair.Key, pair => pair.Value);

            // Add positions just outside of min and max scanning positions as failed (as these should be guaranteed to fail), this will prevent us from scanning extreme positions first.
            failedPositions.Add(minPosition-1);
            failedPositions.Add(maxPosition+1);

            if (failedAtDefault)
            {
                _offsetHistogram[defaultPosition] = Math.Clamp((_offsetHistogram[defaultPosition] / 2) - 1, 1, 100);
                failedPositions.Add(defaultPosition);
            }

            logger?.LogInformation($"Stochastic calibration init with params - min. offset: {minPos}, max. offset: {maxPos}, default offset: {defaultPos}, limit: {timeLimit}");

            UpdateCandidates();

            Status = CalibrationStatus.Sampling;
            StatusDescription = "Sampling...";
            return sw = Stopwatch.StartNew(); // start now
        }

        // recalculate candidate positions and update next position for scanning
        // - prefer highest rated offsets according to history
        // - if more positions are equal, decide according to distance from previous failed tests within this scanning
        private void UpdateCandidates()
        {
            foreach (int failedPosition in failedPositions)
            {
                candidatePositions.Remove(failedPosition);
            }

            foreach(int offset in _offsetHistogram.Keys)
            {
                if (candidatePositions.ContainsKey(offset))
                {
                    int dist = 0;
                    foreach (int fPos in failedPositions)
                    {
                        dist += 1000 / Math.Abs(fPos - offset);
                    }
                    dist = Math.Clamp(1024*1024 - dist, 1, 1024*1024 - 1); // revert value so most balanced offsets have higher score
                    candidatePositions[offset] = (_offsetHistogram[offset] << 20) | dist; // past good result is prioritized (high part)
                }
            }

            KeyValuePair<int, int> bestOffset = candidatePositions.FirstOrDefault();
            foreach (KeyValuePair<int, int> candidatePosition in candidatePositions)
            {
                if (candidatePosition.Value > bestOffset.Value)
                {
                    bestOffset = candidatePosition;
                }
            }

            NextSamplingPosition = bestOffset.Key;
        }

        public CalibrationStatus SamplingResult(int position, bool result)
        {
            logger?.LogInformation($"Received sampling result - position: {position}, result: {(result?"OK":"FAIL")}, current sampling position: {NextSamplingPosition}");

            if (sw.Elapsed > timeLimit)
            {
                Status = CalibrationStatus.Failed;
                StatusDescription = $"Time limit of {timeLimit.TotalSeconds}s has expired.";
                logger?.LogWarning(StatusDescription);
                return Status;
            }

            if (Status != CalibrationStatus.Sampling)
            {
                StatusDescription = $"Invalid operation, not in a sampling state - current Status: {this.Status:G}";
                Status = CalibrationStatus.Failed;
                logger?.LogError(StatusDescription);
                throw new InvalidOperationException(StatusDescription);
            }

            if (position < minPos || position > maxPos || position != NextSamplingPosition)
            {
                StatusDescription = $"Invalid position reported - min. offset: {minPos}, max offset: {maxPos}, current sampling position: {NextSamplingPosition}, reported position: {position}, reported result: {(result?"OK":"FAIL")}";
                Status = CalibrationStatus.Failed;
                logger?.LogError(StatusDescription);
                throw new InvalidOperationException(StatusDescription);
            }

            if (result)
            {
                logger?.LogInformation($"Working position found at offset: {position}, scanning is done.");
                _offsetHistogram[position] = Math.Clamp(_offsetHistogram[position]*2 + 1, 1, 100);
                Status = CalibrationStatus.Result;
                StatusDescription = "Scanning is done.";
                logger?.LogInformation(StatusDescription);
                SaveOffsetHistogram();

                // secondary goal - if there is a higher scored offset in _offsetHistogram than at default position, suggest new default:
                KeyValuePair<int, int> bestOffset = _offsetHistogram.FirstOrDefault();
                foreach (KeyValuePair<int, int> candidatePosition in _offsetHistogram)
                {
                    if (candidatePosition.Value > bestOffset.Value)
                    {
                        bestOffset = candidatePosition;
                    }
                }

                // find if best offset in histogram is also one of the working positions in this sampling round
                if (bestOffset.Key != defaultPos && candidatePositions.ContainsKey(bestOffset.Key)) 
                {
                    NewDefaultPositionRecommended = true;
                    NewDefaultPosition = bestOffset.Key;
                    logger?.LogInformation($"Scanner recommends new default offset: {NewDefaultPosition}");
                }
            }
            else
            {
                _offsetHistogram[position] = Math.Clamp((_offsetHistogram[position] / 2) - 1, 1, 100);
                failedPositions.Add(position);
                UpdateCandidates();
                logger?.LogInformation($"Next sampling position is: {NextSamplingPosition}");
            }

            return Status;
        }

        private void LoadOffsetHistogram()
        {
            if (!string.IsNullOrWhiteSpace(offsetHistogramFile))
            {
                try
                {
                    var text = File.ReadAllText(offsetHistogramFile);
                    var histogram = JsonConvert.DeserializeObject<Dictionary<int, int>>(text);
                    if (histogram != null && histogram.Count > 0)
                    {
                        _offsetHistogram = histogram;
                    }
                }
                catch (Exception e)
                {
                    logger.LogError(e, $"Failed to load offset histogram from '{offsetHistogramFile}' -> starting with empty histogram");
                }
            }
        }

        private void SaveOffsetHistogram()
        {
            if (!string.IsNullOrWhiteSpace(offsetHistogramFile))
            {
                try
                {
                    var text = JsonConvert.SerializeObject(_offsetHistogram, Formatting.Indented);
                    File.WriteAllText(offsetHistogramFile, text);
                }
                catch (Exception e)
                {
                    logger.LogError(e, $"Failed to save offset histogram to '{offsetHistogramFile}' (will try again after next success, changes might be lost when service restarts)");
                }
            }
        }
    }
}
