﻿using EvolisPrinterApi.Config;
using EvolisPrinterApi.Models;
using Microsoft.Extensions.Logging;

namespace EvolisPrinterApi.ApplicationClasses.CSCalibration
{
    public class CalibrationStrategyFactory
    {
        private readonly EvolisPrinterApiConfiguration config;
        private readonly ILogger<ICalibrationStrategy> logger;

        public CalibrationStrategyFactory(EvolisPrinterApiConfiguration config, ILogger<ICalibrationStrategy> logger)
        {
            this.config = config;
            this.logger = logger;
        }

        public ICalibrationStrategy GetCalibrationStrategy(CalibrationMode cSCalibrationMode)
        {
            return cSCalibrationMode switch
            {
                // linear mode
                CalibrationMode.Linear => new LinearCalibration(logger),
                // sampling mode
                CalibrationMode.Stochastic => new StochasticCalibration(logger, offsetHistogramFile: config.CSCalibrationOffsetHistogramFile),
                // 0 == disabled and default -> null
                _ => null
            };
        }
    }
}
