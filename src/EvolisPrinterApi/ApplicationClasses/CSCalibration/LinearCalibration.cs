﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using EvolisPrinterApi.Exceptions;
using Microsoft.Extensions.Logging;

namespace EvolisPrinterApi.ApplicationClasses.CSCalibration
{
    public class LinearCalibration : ICalibrationStrategy
    {
        private readonly ILogger<ICalibrationStrategy> logger;
        public CalibrationStatus Status { get; private set; }

        public string StatusDescription { get; private set; }

        public bool NewDefaultPositionRecommended { get; private set; }

        public int NewDefaultPosition { get; private set; }

        public int NextSamplingPosition { get; private set; }

        private Stopwatch sw;
        private TimeSpan timeLimit;

        private readonly List<int> positiveSamples = new List<int>();
        private int minPos;
        private int maxPos;
        private int defaultPos;
        private int step;

        public LinearCalibration(ILogger<ICalibrationStrategy> logger)
        {
            this.logger = logger;
            Status = CalibrationStatus.Init;
            StatusDescription = "Initialization needed.";
            NewDefaultPositionRecommended = false;
            NewDefaultPosition = -1;
            NextSamplingPosition = -1;
        }

        public Stopwatch Initialize(int defaultPosition, int minPosition, int maxPosition, TimeSpan timeBoundary, bool failedAtDefault)
        {
            timeLimit = timeBoundary;
            defaultPos = defaultPosition;
            minPos = minPosition;
            maxPos = maxPosition;

            if (minPosition < 0 || 
                defaultPosition < minPosition || maxPosition < defaultPosition ||
                maxPosition <= minPosition || (maxPosition - minPosition) > 1000)
            {
                throw new CalibrationParametersOutOfRangeException(minPosition, defaultPosition, maxPosition);
            }

            NextSamplingPosition = minPos;
            step = Math.Clamp((int)((maxPos - minPos)/Math.Max(1, (timeBoundary.TotalSeconds/4) -1)), 1, 10); // autotune between 1 and 10 according to maximum allowed time, expecting 4s per one test
            logger?.LogInformation($"Linear calibration init with params - min. offset: {minPos}, max. offset: {maxPos}, default offset: {defaultPos}, step: {step}, limit: {timeLimit}");

            Status = CalibrationStatus.Sampling;
            StatusDescription = "Sampling...";
            return sw = Stopwatch.StartNew(); // start now
        }

        public CalibrationStatus SamplingResult(int position, bool result)
        {
            logger?.LogInformation($"Received sampling result - position: {position}, result: {(result?"OK":"FAIL")}, current sampling position: {NextSamplingPosition}");

            if (sw.Elapsed > timeLimit)
            {
                Status = CalibrationStatus.Failed;
                StatusDescription = $"Time limit of {timeLimit.TotalSeconds}s has expired.";
                logger?.LogWarning(StatusDescription);
                return Status;
            }

            if (Status != CalibrationStatus.Sampling)
            {
                this.StatusDescription = $"Invalid operation, not in a sampling state - current Status: {this.Status:G}";
                this.Status = CalibrationStatus.Failed;
                logger?.LogError(this.StatusDescription);
                throw new InvalidOperationException(StatusDescription);
            }

            if (position < minPos || position > maxPos || position != NextSamplingPosition)
            {
                this.StatusDescription = $"Invalid position reported - min. offset: {minPos}, max offset: {maxPos}, current sampling position: {NextSamplingPosition}, reported position: {position}, reported result: {(result?"OK":"FAIL")}";
                this.Status = CalibrationStatus.Failed;
                logger?.LogError(this.StatusDescription);
                throw new InvalidOperationException(StatusDescription);
            }

            if (result)
            {
                positiveSamples.Add(position);
                logger?.LogInformation($"Working position found at offset: {position}, now having {positiveSamples.Count} good positions in total.");
            }

            NextSamplingPosition += step;
            logger?.LogInformation($"Next sampling position is: {NextSamplingPosition}");

            // decide next step
            if ((!result || NextSamplingPosition > maxPos) && positiveSamples.Count > 0) // we are done, there were some positive samples and now we are behind the area
            {
                Status = CalibrationStatus.Result;
                StatusDescription = "Scanning is done.";
                logger?.LogInformation(StatusDescription);

                if (positiveSamples.Count > 2) // calculate whether new default should be set and provide value, we need at least 3 or more good samples
                {
                    int averagePos = (int)positiveSamples.Average();
                    int testedMin = positiveSamples.Min();
                    int testedMax = positiveSamples.Max();

                    // Evolis considers working area under '20' units invalid: We consider the range of the working area too low, there is something wrong
                    if (testedMax - testedMin >= 20)
                    {
                        NewDefaultPositionRecommended = true;
                        NewDefaultPosition = averagePos;
                        logger?.LogInformation($"Scanner recommends new default offset: {NewDefaultPosition}");
                    }
                }
            }
            
            return Status;
        }
    }
}
 