﻿using System;
using System.Diagnostics;

namespace EvolisPrinterApi.ApplicationClasses.CSCalibration
{

    /// <summary>
    /// Calibration routine implementation.
    /// Init state requires initialization with desired parameters and status information.
    /// Sampling state provides parameters for next sample and each submission of result advances state either to next sampling position or to one of final states.
    /// Failed state could mean timeout expiration or inconsistent sampling result which is not usable for position adjustment.
    /// Result state means that calibration is finished, there could be a suggestion for new default position but it is not guaranteed.
    /// </summary>
    public interface ICalibrationStrategy
    {
        CalibrationStatus Status { get; }

        string StatusDescription { get; }

        bool NewDefaultPositionRecommended { get; }

        int NewDefaultPosition { get; }

        int NextSamplingPosition { get; }

        Stopwatch Initialize(int defaultPosition, int minPosition, int maxPosition, TimeSpan timeBoundary, bool failedAtDefault);

        CalibrationStatus SamplingResult(int position, bool result);
    }

    public enum CalibrationStatus
    {
        Init = 0, // needs initialization according to specified parameters
        Sampling = 1, // needs to carry out next sample as indicated
        Failed = 2, // calibration has failed - final state with no good result, current position is default one but it may still fail
        Result = 3 // calibration is finished - final state where card is in a working position, there may be a suggestion for a new default position
    }

}
