﻿using System.Net;
using Microsoft.AspNetCore.Mvc;

namespace EvolisPrinterApi.Controllers
{
    /// <summary>
    /// API info operations
    /// </summary>
    [Route("api/info")]
    [ApiController]
    public class InfoController : ControllerBase
    {
        /// <summary>
        /// Service version
        /// </summary>
        [HttpGet("version")]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.OK)]
        public ActionResult<string> GetVersion()
        {
            return System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString(4);
        }
    }
}