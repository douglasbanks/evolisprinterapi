﻿using System;
using System.Net;
using System.Reflection;
using System.Threading.Tasks;
using EvolisPrinterApi.ApplicationClasses;
using EvolisPrinterApi.Exceptions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PrinterApi.Contracts.Models;
using JobStatus = EvolisPrinterApi.ApplicationClasses.JobStatus;

namespace EvolisPrinterApi.Controllers
{
    /// <summary>
    /// Print job related operations
    /// </summary>
    [Route("api/jobs")]
    [ApiController]
    public class JobController : ControllerBase
    {
        private readonly ILogger logger;
        private readonly PrinterManager printerManager;

        public JobController(ILogger<JobController> logger, PrinterManager printerManager)
        {
            this.logger = logger;
            this.printerManager = printerManager;
        }

        /// <summary>
        /// Immediately returns print job[id] status.
        /// </summary>
        [HttpGet("{id?}/status")]
        [ProducesResponseType(typeof(JobStatus), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ProblemDetails), (int)HttpStatusCode.InternalServerError)]
        public ActionResult<JobStatus> GetJobStatus(string id = null)
        {
            using (logger.BeginScope(id))
            {
                try
                {
                    return printerManager.GetJobStatus(id);
                }
                catch (UnknownJobException e)
                {
                    logger.LogWarning($"{MethodBase.GetCurrentMethod().Name}(id:{id}): {e.Message}");
                    return NotFound();
                }
                catch (Exception e)
                {
                    logger.LogError(e, $"{MethodBase.GetCurrentMethod().Name}(id:{id}): {e.Message}");
                    throw;
                }
            }
        }

        /// <summary>
        /// Waits (long polling) for current print job to be finished and returns its result
        /// </summary>
        [HttpGet("{id?}/result")]
        [ProducesResponseType(typeof(JobStatus), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ProblemDetails), (int)HttpStatusCode.InternalServerError)]
        public async Task<ActionResult<JobStatus>> GetJobResult(string id = null)
        {
            using (logger.BeginScope(id))
            {
                try
                {
                    JobStatus jobStatus = printerManager.GetJobStatus(id);
                    await jobStatus.Finished;
                    return jobStatus;
                }
                catch (UnknownJobException e)
                {
                    logger.LogWarning($"{MethodBase.GetCurrentMethod().Name}(id:{id}): {e.Message}");
                    return NotFound();
                }
                catch (Exception e)
                {
                    logger.LogError(e, $"{MethodBase.GetCurrentMethod().Name}(id:{id}): {e.Message}");
                    throw;
                }
            }
        }

        /// <summary>
        /// Waits (long polling) for current print job to waiting for external processing (404 if there's no job running)
        /// </summary>
        [HttpGet("current/external")]
        [ProducesResponseType(typeof(ExternalProcessing), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ProblemDetails), (int)HttpStatusCode.InternalServerError)]
        public async Task<ActionResult<ExternalProcessing>> WaitForExternalProcessingRequired()
        {
            try
            {
                await printerManager.ActiveJobIsPresent();
                await printerManager.GetJobStatus(null).ExternalChipEncodingRequired;
                return printerManager.ExternalProcessing;
            }
            catch (UnknownJobException)
            {
                return NotFound();
            }
            catch (Exception e)
            {
                logger.LogError(e, $"{MethodBase.GetCurrentMethod().Name}: {e.Message}");
                throw;
            }
        }

        /// <summary>
        /// Informs printer that external processing is finished
        /// </summary>
        [HttpPost("{id?}/external/finish")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Conflict)]
        [ProducesResponseType(typeof(ProblemDetails), (int)HttpStatusCode.InternalServerError)]
        public ActionResult FinishExternalProcessing(ExternalProcessingResult result, string id)
        {
            using (logger.BeginScope(id))
            {
                try
                {
                    printerManager.ChipEncodingFinished(id, result.ProcessingId, result.Success);
                    return Ok();
                }
                catch (NoExternalProcessingRequiredException e)
                {
                    return NotFound($"No external processing requested for job {e.JobId}");
                }
                catch (ExternalProcessingIdMismatchException e)
                {
                    return Conflict($"External processing identification mismatch. Expected: JobId={e.ExpectedJobId}, ProcessingId={e.ExpectedProcessingId}");
                }
                catch (Exception e)
                {
                    logger.LogError(e, $"{MethodBase.GetCurrentMethod().Name}: {e.Message}");
                    throw;
                }
            }
        }

        /// <summary>
        /// Submit a new job for processing on the printer (409 if there's a print job running)
        /// <param name="printJob">Print job dataset</param>>
        /// </summary>
        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.Accepted)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Conflict)]
        [ProducesResponseType(typeof(ProblemDetails), (int)HttpStatusCode.InternalServerError)]
        public ActionResult PrintJob(PrintJob printJob)
        {
            using (logger.BeginScope(printJob?.Id))
            {
                try
                {
                    printerManager.PrintJob(printJob);
                    return Accepted();
                }
                catch (PrintJobInProgressException e)
                {
                    return Conflict(e.JobId);
                }
                catch (Exception e)
                {
                    logger.LogError(e, $"{MethodBase.GetCurrentMethod().Name}: {e.Message}");
                    throw;
                }
            }
        }
    }
}