﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Reflection;
using System.Threading.Tasks;
using EvolisPrinterApi.ApplicationClasses;
using EvolisPrinterApi.Contracts.Models;
using EvolisPrinterApi.Exceptions;
using HidPrinterApi.Contracts.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using RibbonInfo = EvolisPrinterApi.Contracts.Models.RibbonInfo;

namespace EvolisPrinterApi.Controllers
{
    /// <summary>
    /// Printer control related operations
    /// </summary>
    [Route("api/printer")]
    [ApiController]
    public class PrinterController : ControllerBase
    {
        private readonly ILogger logger;
        private readonly PrinterManager printerManager;

        public PrinterController(ILogger<PrinterController> logger, PrinterManager printerManager)
        {
            this.logger = logger;
            this.printerManager = printerManager;
        }

        /// <summary>
        /// Returns identify information about the printer
        /// </summary>
        [HttpGet("identity")]
        [ProducesResponseType(typeof(PrinterIdentity), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ProblemDetails), (int)HttpStatusCode.InternalServerError)]
        public ActionResult<PrinterIdentity> Identity()
        {
            try
            {
                return printerManager.ReadPrinterIdentity();
            }
            catch (Exception e)
            {
                logger.LogError(e, $"{MethodBase.GetCurrentMethod().Name}: {e.Message}");
                throw;
            }
        }

        [HttpGet("status")]
        [ProducesResponseType(typeof(PrinterStatus), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ProblemDetails), (int)HttpStatusCode.InternalServerError)]
        public ActionResult<PrinterStatus> Status()
        {
            try
            {
                return printerManager.GetPrinterStatus();
            }
            catch (Exception e)
            {
                logger.LogError(e, $"{MethodBase.GetCurrentMethod().Name}: {e.Message}");
                throw;
            }
        }

        [HttpGet("info")]
        [ProducesResponseType(typeof(PrinterInfo), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ProblemDetails), (int)HttpStatusCode.InternalServerError)]
        public ActionResult<PrinterInfo> Info()
        {
            try
            {
                return printerManager.GetPrinterInfo();
            }
            catch (Exception e)
            {
                logger.LogError(e, $"{MethodBase.GetCurrentMethod().Name}: {e.Message}");
                throw;
            }
        }


        [HttpGet("ribbon")]
        [ProducesResponseType(typeof(RibbonInfo), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ProblemDetails), (int)HttpStatusCode.InternalServerError)]
        public ActionResult<RibbonInfo> GetRibbon()
        {
            try
            {
                return printerManager.GetRibbonInfo();
            }
            catch (Exception e)
            {
                logger.LogError(e, $"{MethodBase.GetCurrentMethod().Name}: {e.Message}");
                throw;
            }
        }

        /// <summary>
        /// Unlocks printer for printing
        /// </summary>
        [HttpPost("unlock")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ProblemDetails), (int)HttpStatusCode.InternalServerError)]
        public ActionResult UnlockPrinter()
        {
            try
            {
                printerManager.UnlockPrinter();
                return Ok();
            }
            catch (Exception e)
            {
                logger.LogError(e, $"{MethodBase.GetCurrentMethod().Name}: {e.Message}");
                throw;
            }
        }
        
        /// <summary>
        /// Printer self adjustment
        /// </summary>
        [HttpPost("adjust")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ProblemDetails), (int)HttpStatusCode.InternalServerError)]
        public ActionResult SelfAdjust()
        {
            try
            {
                printerManager.SelfAdjust();
                return Ok();
            }
            catch (Exception e)
            {
                logger.LogError(e, $"{MethodBase.GetCurrentMethod().Name}: {e.Message}");
                throw;
            }
        }

        /// <summary>
        /// Perform precise calibration of Contact Chip Coding Station.
        /// </summary>
        [HttpPost("calibrate/contact")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ProblemDetails), (int)HttpStatusCode.InternalServerError)]
        public ActionResult CsCalibrate(CalibrationParameters calibrationParameters = null)
        {
            try
            {
                printerManager.CsCalibration(calibrationParameters);
                return Ok();
            }
            catch (Exception e)
            {
                logger.LogError(e, $"{MethodBase.GetCurrentMethod().Name}: {e.Message}");
                throw;
            }
        }

        /// <summary>
        /// Reinitializes the printer and cancels all jobs. This method is not synchronized with normal processing flow and its usage could lead to inconsistencies.
        /// Do not use in normal flow, do not use for normal job cancellation. Service has configurable timeout which is applied for all job transitions during normal processing.
        /// </summary>
        [HttpPost("reset")]
        [ProducesResponseType((int)HttpStatusCode.Accepted)]
        [ProducesResponseType(typeof(ProblemDetails), (int)HttpStatusCode.InternalServerError)]
        public ActionResult Reset()
        {
            try
            {
                printerManager.ResetPrinter();
                return Accepted();
            }
            catch (Exception e)
            {
                logger.LogError(e, $"{MethodBase.GetCurrentMethod().Name}: {e.Message}");
                throw;
            }
        }

        /// <summary>
        /// Waits for LCD display to become ready
        /// </summary>
        [HttpGet("display/ready")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ProblemDetails), (int)HttpStatusCode.InternalServerError)]
        public ActionResult WaitForDisplayReady()
        {
            try
            {
                printerManager.WaitForDisplayReady();
                return Ok();
            }
            catch (Exception e)
            {
                logger.LogError(e, $"{MethodBase.GetCurrentMethod().Name}: {e.Message}");
                throw;
            }
        }

        /// <summary>
        /// Displays message on the screen
        /// </summary>
        [HttpPost("display/message")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ProblemDetails), (int)HttpStatusCode.InternalServerError)]
        public ActionResult DisplayMessage(string message, bool confirm = true)
        {
            try
            {
                printerManager.DisplayMessage(message, confirm);
                return Ok();
            }
            catch (Exception e)
            {
                logger.LogError(e, $"{MethodBase.GetCurrentMethod().Name}: {e.Message}");
                throw;
            }
        }

        /// <summary>
        /// Clears message on the screen
        /// </summary>
        [HttpDelete("display/message")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ProblemDetails), (int)HttpStatusCode.InternalServerError)]
        public ActionResult ClearDisplayMessage()
        {
            try
            {
                printerManager.ClearDisplayMessage();
                return Ok();
            }
            catch (Exception e)
            {
                logger.LogError(e, $"{MethodBase.GetCurrentMethod().Name}: {e.Message}");
                throw;
            }
        }

        // sample curl command: curl -v -F firmware=@FWupgrade.4.0.0.67.frm http://localhost:9632/api/printer/firmware
        [HttpPost("firmware")]
        [RequestFormLimits(MultipartBodyLengthLimit = 209715200)]
        [RequestSizeLimit(209715200)]
        [ProducesResponseType((int)HttpStatusCode.Accepted)]
        [ProducesResponseType(typeof(ProblemDetails), (int)HttpStatusCode.InternalServerError)]
        public async Task<ActionResult> FirmwareUpgrade(IFormFile firmware)
        {
            try
            {
                if (firmware == null)
                {
                    throw new EmptyFirmwareFileException();
                }

                byte[] firmwareBytes;
                using (var ms = new MemoryStream())
                {
                    await firmware.CopyToAsync(ms);
                    firmwareBytes = ms.ToArray();
                }

                if (firmwareBytes == null || firmwareBytes.Length == 0)
                {
                    throw new EmptyFirmwareFileException();
                }

                logger.LogWarning($"Starting firmware upgrade to '{firmware.FileName}' ({firmware.Length/1024f/1024f}MB)");
                printerManager.UpgradeFirmware(firmwareBytes);
                return Accepted();
            }
            catch (Exception e)
            {
                logger.LogError(e, $"{MethodBase.GetCurrentMethod().Name}: {e.Message}");
                throw;
            }
        }

        [HttpGet("settings")]
        [ProducesResponseType(typeof(List<PrinterAdvancedSetting>), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ProblemDetails), (int)HttpStatusCode.InternalServerError)]
        public ActionResult<List<PrinterAdvancedSetting>> GetAdvancedSettings()
        {
            try
            {
                return printerManager.GetAdvancedSettings();
            }
            catch (Exception e)
            {
                logger.LogError(e, $"{MethodBase.GetCurrentMethod().Name}: {e.Message}");
                throw;
            }
        }

        [HttpPut("settings/{id}")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ProblemDetails), (int)HttpStatusCode.InternalServerError)]
        public ActionResult SaveAdvancedSetting(int id, int value)
        {
            try
            {
                printerManager.SaveAdvancedSetting(id, value);
                return Ok();
            }
            catch (Exception e)
            {
                logger.LogError(e, $"{MethodBase.GetCurrentMethod().Name}: {e.Message}");
                throw;
            }
        }

        [HttpPut("users/{id}/pin")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ProblemDetails), (int)HttpStatusCode.InternalServerError)]
        public ActionResult SetUserPin(int id, string pin)
        {
            try
            {
                printerManager.SetUserPin(id, pin);
                return Ok();
            }
            catch (Exception e)
            {
                logger.LogError(e, $"{MethodBase.GetCurrentMethod().Name}: {e.Message}");
                throw;
            }
        }

        [HttpPost("command")]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ProblemDetails), (int)HttpStatusCode.InternalServerError)]
        public ActionResult<string> SendRawCommand(string command)
        {
            try
            {
                return printerManager.SendRawCommand(command);
            }
            catch (Exception e)
            {
                logger.LogError(e, $"{MethodBase.GetCurrentMethod().Name}: {e.Message}");
                throw;
            }
        }

        [HttpPost("commands")]
        [ProducesResponseType(typeof(IEnumerable<string>), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ProblemDetails), (int)HttpStatusCode.InternalServerError)]
        public ActionResult<IEnumerable<string>> SendRawCommands(IEnumerable<string> commands)
        {
            try
            {
                var results = new List<string>();
                foreach (var command in commands)
                {
                    results.Add(printerManager.SendRawCommand(command));
                }

                return results;
            }
            catch (Exception e)
            {
                logger.LogError(e, $"{MethodBase.GetCurrentMethod().Name}: {e.Message}");
                throw;
            }
        }
    }
}