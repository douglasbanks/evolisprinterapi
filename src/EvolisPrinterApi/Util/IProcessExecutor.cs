﻿using System;
using PrinterApi.Contracts.Models;

namespace EvolisPrinterApi.Util
{
    public interface IProcessExecutor
    {
        ExecutionResult ExecuteProcess(string path, string workDir, string args, string stdin, TimeSpan maxExecutionTime);
    }
}