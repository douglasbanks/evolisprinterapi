﻿using System;
using System.Diagnostics;
using System.IO.Enumeration;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using EvolisPrinterApi.Config;
using EvolisPrinterApi.Exceptions;
using Microsoft.Extensions.Logging;
using PCSC;
using PCSC.Monitoring;
using PcscLib.Exceptions;
using PcscLib.Interfaces;
using PcscLib.Pcsc;
using PrinterApi.Contracts.Models;

namespace EvolisPrinterApi.Util
{
    // ReSharper disable once InconsistentNaming
    public class PCSCHelper : IDisposable
    {
        private readonly IProcessExecutor processExecutor;
        private readonly ILogger<PCSCHelper> logger;
        private readonly ISmartcardReaderScanner scanner;
        private readonly EvolisPrinterApiConfiguration config;
        private readonly AutoResetEvent cardInsertedEvent = new AutoResetEvent(false);
        private readonly ISCardMonitor sCardMonitor;
        private CardStatusEventArgs cardStatusEventArgs;

        public PCSCHelper(IProcessExecutor processExecutor, ILogger<PCSCHelper> logger, ISmartcardReaderScanner scanner, EvolisPrinterApiConfiguration config)
        {
            this.processExecutor = processExecutor;
            this.logger = logger;
            this.scanner = scanner;
            this.config = config;
            sCardMonitor = MonitorFactory.Instance.Create(SCardScope.System);
            sCardMonitor.CardInserted += (_, args) => {
                cardStatusEventArgs = args;
                cardInsertedEvent.Set();
            };
        }

        public void RestartPcscDaemon()
        {
            try
            {
                if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
                {
                    ExecutionResult result = processExecutor.ExecuteProcess("/usr/bin/sudo", null, "systemctl restart pcscd", null, TimeSpan.FromSeconds(10));
                    if (result.ExitCode != 0)
                    {
                        logger.LogError($"pcscd restart failed ({result.ExitCode}): {result.StandardErrorOutput}");
                    }
                    logger.LogDebug($"pcscd restarted [{result.ExecutionTime.TotalMilliseconds}ms]");
                }
                else
                {
                    logger.LogWarning("PCSC restart ignored (only works on Linux)");
                }
            }
            catch (Exception e)
            {
                logger.LogError(e, $"pcscd restart failed: {e.Message}");
            }
        }

        public string FindSmartcardReader()
        {
            var allReaders = scanner.Scan();
            logger.LogDebug($"Found PC/SC readers: {string.Join(',', allReaders)}");

            var readerName = allReaders.FirstOrDefault(s => FileSystemName.MatchesSimpleExpression(config.CSCalibrationReaderNamePattern, s));
            if (readerName == null)
            {
                logger.LogCritical($"No PC/SC reader matching wildcard '{config.CSCalibrationReaderNamePattern}' found");
                throw new NoSmartcardReaderFoundException(config.CSCalibrationReaderNamePattern, allReaders);
            }

            logger.LogInformation($"Using PC/SC reader: {readerName}");
            return readerName;
        }

        public bool ProbeReader(int position, Stopwatch sw, int attemptNumber)
        {
            bool returnValue = false;
            try
            {
                string atr;
                logger.LogDebug($"Waiting for CardInserted event (max {config.CSCalibrationCardInsertedTimeout.TotalSeconds} seconds)");
                if (cardInsertedEvent.WaitOne(config.CSCalibrationCardInsertedTimeout))
                {
                    atr = BitConverter.ToString(cardStatusEventArgs.Atr).Replace("-", "");
                    logger.LogDebug($"CardInserted event detected: state={cardStatusEventArgs.State}; ATR={atr}");
                }
                else
                {
                    logger.LogDebug($"Didn't get CardInserted event in {config.CSCalibrationCardInsertedTimeout.TotalSeconds} seconds -> try to reset the card");
                    
                    if (config.CSCalibrationPCSCDRestart)
                    {
                        RestartPcscDaemon();
                    }
                    
                    var readerName = FindSmartcardReader();
                    using PcscReader reader = new PcscReader(readerName);
                    reader.Connect();
                    atr = reader.Atr();
                    logger.LogInformation($"Position: {position}, ATR: {atr}, elapsed: {sw?.ElapsedMilliseconds}ms, attempt: {attemptNumber}");
                }
                
                returnValue = !string.IsNullOrEmpty(atr);
            }
            catch (PcscReaderException e)
            {
                logger.LogError(e, $"Position: {position}, result: {(returnValue ? "OK" : "FAIL")}, elapsed: {sw?.ElapsedMilliseconds}ms, attempt: {attemptNumber}, message: {e.Message}, code: '{(int) e.ErrorCode & 0xFFFF}'");
            }
            catch (Exception e)
            {
                if (e is AggregateException ae)
                {
                    ae = ae.Flatten();
                    e = ae.InnerException ?? ae;
                }
                logger.LogError(e, $"Unable to probe reader: {e.Message}");
            }

            return returnValue;
        }

        public void ResetCardInsertedEvent()
        {
            cardInsertedEvent.Reset();
            cardStatusEventArgs = null;
        }

        public void StartSmartcardEventMonitoring()
        {
            var readerName = FindSmartcardReader();
            sCardMonitor.Start(readerName);
        }

        public void Dispose()
        {
            cardInsertedEvent?.Dispose();
            sCardMonitor?.Dispose();
        }
    }
}