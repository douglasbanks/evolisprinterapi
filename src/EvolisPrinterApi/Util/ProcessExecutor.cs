﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using EvolisPrinterApi.Exceptions;
using PrinterApi.Contracts.Models;

namespace EvolisPrinterApi.Util
{
    public class ProcessExecutor : IProcessExecutor
    {
        public ExecutionResult ExecuteProcess(string path, string workDir, string args, string stdin, TimeSpan maxExecutionTime)
        {
            bool redirectStandardInput = stdin != null;
            ProcessStartInfo processInfo = new ProcessStartInfo(path, args)
            {
                RedirectStandardInput = redirectStandardInput,
                RedirectStandardOutput = true,
                RedirectStandardError = true,
                UseShellExecute = false,
            };

            if (workDir != null)
            {
                processInfo.WorkingDirectory = workDir;
            }

            Stopwatch sw = Stopwatch.StartNew();
            Process process = Process.Start(processInfo);
            if (process == null)
            {
                throw new FailedToStartProcessException(path);
            }

            Task<string> standardOutput = Task.Run(async () => await process.StandardOutput.ReadToEndAsync());
            Task<string> standardError = Task.Run(async () => await process.StandardError.ReadToEndAsync());

            if (redirectStandardInput)
            {
                process.StandardInput.Write(stdin);
                process.StandardInput.Flush();
                process.StandardInput.Close();
            }

            bool exited = process.WaitForExit((int) maxExecutionTime.TotalMilliseconds);
            sw.Stop();

            if (exited)
            {
                return new ExecutionResult
                {
                    ExitCode = process.ExitCode,
                    ExecutionTime = sw.Elapsed,
                    StandardOutput = standardOutput.Result,
                    StandardErrorOutput = standardError.Result
                };
            }

            try
            {
                process.Kill();
            }
            catch (Exception)
            {
                // ignore errors
            }

            throw new MaxProcessTimeExceededException(path, maxExecutionTime);
        }
    }
}