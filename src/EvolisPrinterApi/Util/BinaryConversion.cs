﻿using System;
using System.Text;

namespace EvolisPrinterApi.Util
{
    public class BinaryConversion
    {

        /// <summary>
        /// Convert first (e.g.) 4 bytes to HEX for tracing purposes as 'ABABABAB... (1457 bytes in total)'
        /// </summary>
        /// <param name="buffer"></param>
        /// <param name="limit"></param>
        /// <returns></returns>
        public static string HexTrace(byte[] buffer, int limit = 64)
        {
            StringBuilder sb = new StringBuilder();

            int cap = Math.Min(limit, buffer.Length);
            for (int i = 0; i < cap; ++i)
            {
                sb.Append(BitConverter.ToString(buffer, i, 1));
            }

            if (buffer.Length > cap)
            {
                sb.Append("...");
            }

            sb.Append($" ({buffer.Length} bytes in total)");

            return sb.ToString();
        }

        public static bool Contains(byte[] sequence, int position, byte[] subsequence)
        {
            if (position >= 0 && position + subsequence.Length < sequence.Length)
            {
                for (int i = 0; i < subsequence.Length; ++i)
                {
                    if (sequence[position+i] != subsequence[i])
                    {
                        return false;
                    }
                }
                return true;
            }

            return false;
        }
    }
}