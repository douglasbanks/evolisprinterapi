﻿using System;
using System.Collections.Generic;
using EvolisPrinterApi.ApplicationClasses.Drivers;
using EvolisPrinterApi.Contracts.Models;
using HidPrinterApi.Contracts.Models;
using Microsoft.AspNetCore.Mvc;
using PrinterApi.Contracts.Models.Dataset;
using RibbonInfo = EvolisPrinterApi.Contracts.Models.RibbonInfo;

namespace EvolisPrinterApi.Interfaces
{
    /// <summary>
    /// Defines high-level printer operations. Important invariant: printer communication is serialized and should be done only in one thread, calls should not interleave.
    /// </summary>
    public interface IPrinterDriver : IDisposable
    {
        /// <summary>
        /// Reads printer firmware info
        /// </summary>
        /// <returns>Printer firmware and hardware revisions</returns>
        PrinterIdentity ReadIdentity();

        /// <summary>
        /// Unlocks printer for printing
        /// </summary>
        void UnlockPrinter();

        /// <summary>
        /// Reinitializes the printer and cancels all jobs
        /// </summary>
        void Reset();

        /// <summary>
        /// Displays message on the screen
        /// </summary>
        void DisplayMessage(string message, bool confirm);

        /// <summary>
        /// Moves smartcard to contact or contactless smartcard reader position
        /// </summary>
        /// <param name="contactless">If true card is moved to contactless smartcard reader, otherwise it's moved to contact position</param>
        void MoveToSmartcardReader(bool contactless = false);

        /// <summary>
        /// Moves smartcard to the output box
        /// </summary>
        void MoveToOutputBox();

        /// <summary>
        /// Moves smartcard to the reject box
        /// </summary>
        void MoveToRejectBox();

        /// <summary>
        /// Flip to backside
        /// </summary>
        void FlipCard(bool front);

        /// <summary>
        /// Perform printer firmware upgrade
        /// </summary>
        /// <param name="firmware">Serialized printer firmware contents</param>
        void UpgradeFirmware(byte[] firmware);

        /// <summary>
        /// Returns printer settings
        /// </summary>
        ActionResult<List<PrinterAdvancedSetting>> GetAdvancedSettings();

        /// <summary>
        /// Updates printer setting with given id to given value
        /// </summary>
        /// <param name="id">Advanced settings id</param>
        /// <param name="value">Advanced settings value</param>
        void SetAdvancedSetting(int id, int value);

        RibbonInfo GetRibbonInfo();
        
        void ClearDisplay();

        PrinterResponse SendRawCommand(string command);

        void ResetUserPin(int userId, string pin);

        int PrintsTillNextCleaning();

        int AdvancedCleaningCount();

        int AverageCleaningCount();

        // ReSharper disable once InconsistentNaming
        void CalibrateCS(bool standalone, CalibrationParameters parameters = null);

        public PrinterStatusCheck CheckPrinterStatus();

        void WriteMagstripeTracks(Magstripe job);

        void WriteImage(PrintImage panel, IDictionary<string, string> settings);

        bool VerifyChipContact();
        
        void Connect();
        
        void SelfAdjust();
        
        void ClearCurrentPrinterStatus();

        bool CheckDisplayReady();
    }
}