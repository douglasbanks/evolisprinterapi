﻿using System;
using EvolisPrinterApi.ApplicationClasses.Drivers;
using LibUsbDotNet.Main;

namespace EvolisPrinterApi.Interfaces
{
    public interface IEvolisUsbDevice : IDisposable
    {
        void Connect(ushort vendorId, ushort productId, ReadEndpointID readEndpointId, WriteEndpointID writeEndpointId);

        void Disconnect();

        PrinterResponse Transceive(string data, string comment = null);

        PrinterResponse Transceive(byte[] data, string comment = null, bool trace = true);
    }
}