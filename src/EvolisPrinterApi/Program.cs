﻿using GenericHostLib.Hosting;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace EvolisPrinterApi
{
    public class Program
    {
        public static void Main(string[] args)
        {
            HostingExtensions.RunHost(CreateHostBuilder(args), false);
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
