# Update existing Precision printers to Precision2
- deploy [aspnetcore-runtime](https://docs.microsoft.com/en-us/dotnet/core/install/linux-centos) >= 3.1 to the printer
- deploy and configure [PrinterApi 0.2.6](http://klondike.eftdomain.net/api/packages/PrinterApi/0.2.6.85-rc/content) to the printer
- stop and disable Mosaic service on the printer
- deploy and configure [EvolisPrinterApi 0.1.0](http://klondike.eftdomain.net/api/packages/EvolisPrinterApi/0.1.0.69-rc/content) to the printer
- stop and disable Precision printer-monitor script
- configure EvolisPrinterApi
    - set `CSCalibrationReaderNamePattern` property in `appsettings.json` to value `*Smart Card Reader*`
    - set `Serilog.MinimumLevel.Default` to value `Debug`
    - set `Serilog.MinimumLevel.Override.EvolisPrinterApi` to value `Verbose`
    - set `Serilog.MinimumLevel.Override.System` to value `Warning`
    - set `Serilog.MinimumLevel.Override.Microsoft` to value `Warning`
- remove kernel module `usblp` (`rmmod usblp`)
- add kernel module `usblp` to kernel module blacklist (`echo 'blacklist usblp' >> /etc/modprobe.d/blacklist.conf`)
- deploy [Spectrum printer-monitor script 1.1.2](https://source.eftdomain.net/cao2/printers/printers-monitoring/tree/1.1.2/spectrum) *with `Precision2` passed as 1st argument to the script*
- make sure following ports are open in the local printer firewall: `5035/tcp`, `9632/tcp`
- migrate the network-monitoring script to use EvolisPrinterApi (POST/DELETE `/api/printer/display/message`) for displaying messages
- change `ComputerTypeId` to `5` in NewSQL using the following SQL script (change the printer `hostname` first in the script):

```sql
DECLARE @hostname AS VARCHAR(50) = 'prnt19308.cao.net'
DECLARE @type AS INT = 5 -- Value 5 is Precision2; Use value 2 to rollback back to Precision
DECLARE @computerId AS INT

SELECT TOP 1 @computerId=Id FROM eftdata.cao.Computers WHERE ComputerDNS = @hostname
EXEC [eftdata].[cao].[Computers_Update]	@ComputerID = @computerId, @ComputerTypeID = @type
```

# Rollback updated Precision2 printer back to Precision
- stop and disable EvolisPrinterApi service
- load kernel module `usblp` (`modprobe usblp`)
- remove `usblp` from kernel module blacklist (`sed -i '/usblp/d' /etc/modprobe.d/blacklist.conf`)
- enable and start Mosaic service
- stop and disable Spectrum printer-monitor script
- re-enable the original Precision printer-monitor script
- change `ComputerTypeId` to `2` in NewSQL using the SQL script above (change the printer `hostname` and change `type` to `2` first)
- reboot


# Precision vs. Precision2 error messages
| Precision                         | Precision2                                                                 | Comment                                                               |
|-----------------------------------|----------------------------------------------------------------------------|-----------------------------------------------------------------------|
| Printer was locked.               |                                                                            | Mosaic-level explicit printer locking not used in Precision2          |
| Incorrect Ribbon                  | Incorrect Ribbon (expected: '{expectedRibbonType}', found: '{ribbonType}') |                                                                       |
| Incorrect Ribbon                  | Unable to read the ribbon type from the printer                            |                                                                       |
| Emv error.                        | Card encoding exited with an error of {code}:                              |                                                                       |
| read failed - null                |                                                                            | EvolisPrinterApi reconnects on errors                                 |
| write failed - Input/output error |                                                                            | EvolisPrinterApi reconnects on errors                                 |
| Error printing card.              |                                                                            | Catch-all error message in MosaicPrintService                         |
| runScript failed (ticket[])       |                                                                            | Mosaic-specific errors (EvolisPrinterApi doesn't use tickets/scripts) |