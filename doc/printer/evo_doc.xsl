<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">

<html>
	<head>
		<link rel="stylesheet" type="text/css" href="evo_doc.css" />
	</head>
	<body>
		<table class='header' width='100%'>
<!-- Header -->
			<tr>
				<td width='200px' height='200px' align='center' valing='center' background='img/red_circle.png' style='color:#ffffff; vertical-align:center;'>Firmware<br/>Documentation<br/><xsl:value-of select="EvoDocXML/type"/><br/></td>
				<td></td>
			</tr><tr><td></td><td>
<!-- Content -->
				<table class='area' width='100%'>
					<tr class='area_title'><td><a name="signet_content">Content</a></td></tr>
					<tr class='area_text'><td>
						<table width='100%'>
<xsl:for-each select="EvoDocXML/evo_cmd">
<xsl:sort select="cmd"/>
							<tr>
								<td><b><a><xsl:attribute name="href">#signet_<xsl:value-of select="cmd"/></xsl:attribute><xsl:value-of select="cmd"/></a></b></td>
								<td><a><xsl:attribute name="href">#signet_<xsl:value-of select="cmd"/></xsl:attribute><xsl:value-of select="brief"/></a></td>
							</tr>
</xsl:for-each>
						</table>
					</td></tr>
				</table>
			</td></tr><tr><td></td><td>
<!-- Categories -->
				<table class='area' width='100%'>
					<tr class='area_title'><td><a name="signet_categories">Categories</a></td></tr>
					<tr class='area_text'><td>
<xsl:for-each select="EvoDocXML/evo_category">
						<table width='100%' style='border:1px solid #c0c0c0;'>
							<tr>
								<td><b><a>
									<xsl:attribute name="name">signet_<xsl:value-of select="category"/></xsl:attribute><xsl:value-of select="category"/>
								</a></b></td>
							</tr>
							<tr>
								<td>
									<xsl:value-of select="description"/><br/>
									<xsl:if test="desc_img!=''"><br/>
										<img class='description'>
											<xsl:attribute name="src"><xsl:value-of select="desc_img"/></xsl:attribute>
										</img><br/>
									</xsl:if>
									<xsl:if test="desc_link!=''">
										<a>
											<xsl:attribute name="href"><xsl:value-of select="desc_link"/></xsl:attribute>
											For more information click
										</a><br/>
									</xsl:if>
								</td>
							</tr><tr>
								<td>
<xsl:variable name="category"><xsl:value-of select="category"/></xsl:variable> 
<xsl:for-each select="../evo_cmd[category=$category]" >
									<a><xsl:attribute name="href">#signet_<xsl:value-of select="cmd"/></xsl:attribute><xsl:value-of select="cmd"/></a>&#160;
</xsl:for-each>
								</td>
							</tr>
						</table>
</xsl:for-each>
					</td></tr>
				</table>
			</td></tr><tr><td></td><td>
<!-- Introduction -->
				<table class='area' width='100%'>
					<tr class='area_title'><td><a name="signet_categories">Introduction</a></td></tr>
					<tr class='area_text'><td>
						This document describes all Evolis Printer commands.<br/>
						The Evolis Card Printers have an internal programming language.<br/>
						The command syntax is defined as follow:<br/>
						<br/>
						<b>(Start Character) Command (Stop Character)</b><br/>
						<br/>
						The commands can get parameters and can be finished by a character string or data.<br/>
						Each element of the command must be separated by a separator character:<br/>
						<br/>
						<b>(Start Character) Command (separator) parameter 1 (separator) parameter 2 (separator) ...parameter n (Stop Character)</b><br/>
						<br/>
						Start Character:                ESC<br/>
						Separator:                        ;<br/>
						Stop Character:                CR<br/>
						<br/>
						<b>Note: the Start Character is not compulsory after the CR character.</b>
					</td></tr>
				</table>
			</td></tr><tr><td></td><td>
<!-- Commands -->
				<table class='area' width='100%'>
					<tr class='area_title'><td><a name="signet_categories">Commands</a></td></tr>
					<tr class='area_text'><td>
					
<!-- Commands extract from XML -->
<xsl:for-each select="EvoDocXML/evo_cmd">
						<table class='area' width='100%'>
<!-- Title -->
							<tr class='cmd_title'>
								<td colspan='3'><a>
									<xsl:attribute name="name">signet_<xsl:value-of select="cmd"/></xsl:attribute>
									<xsl:value-of select="cmd"/>
									<xsl:for-each select="evo_par">;&lt;<xsl:value-of select="name"/>&gt;</xsl:for-each>
									<xsl:for-each select="evo_opt">;<i>[<xsl:value-of select="name"/>]</i></xsl:for-each>
								</a></td>
							</tr>
<!-- Description -->
							<tr class='cmd_text'>
								<td colspan='3'>
									<xsl:value-of select="description" disable-output-escaping="yes"/>
									<xsl:if test="desc_img!=''">
										<br/>
										<img class='description'><xsl:attribute name="src"><xsl:value-of select="desc_img"/></xsl:attribute></img>
										<br/>
									</xsl:if>
									<xsl:if test="desc_link!=''">
										<a><xsl:attribute name="href"><xsl:value-of select="desc_link"/></xsl:attribute>For more information click</a>
										<br/>
									</xsl:if>
								</td>
							</tr>
							<tr class='cmd_text'><td width='20px'><b>Version</b></td><td><xsl:value-of select="version"/></td></tr>
							<tr class='cmd_text'><td width='20px'><b>Printers</b></td><td><xsl:value-of select="printers"/></td></tr>
							<tr class='cmd_text'><td width='20px'><b>Category</b></td><td><a><xsl:attribute name="href">#signet_<xsl:value-of select="category"/></xsl:attribute><xsl:value-of select="category"/></a></td></tr>
<!-- See also -->
							<xsl:if test="evo_see_also!=''">
								<tr class='cmd_text'><td width='20px'><b>See also</b></td><td>
									<xsl:for-each select="evo_see_also">
										<a class='see_also'><xsl:attribute name="href">#signet_<xsl:value-of select="cmd"/></xsl:attribute><xsl:value-of select="cmd"/></a>
									</xsl:for-each>
								</td></tr>
							</xsl:if>
							
							<tr><td colspan='2'>&#160;</td></tr>

<!-- Parameters -->
							<xsl:if test="evo_par!=''">
								<tr><td colspan='2'><b>Parameters</b></td></tr>
								<tr><td colspan='2'>
									<table class='param_text' width='100%'>
										<xsl:for-each select="evo_par">
											<tr class='param_text'><td width='50px'><b><xsl:value-of select="name"/></b></td><td colspan='2'><xsl:value-of select="brief"/></td></tr>
											<xsl:for-each select="evo_val">
												<tr><td width='50px'></td><td width='50px' valign='top'><b><xsl:value-of select="value"/></b></td><td><xsl:value-of select="brief" disable-output-escaping="yes"/></td></tr>
											</xsl:for-each>
										</xsl:for-each>
									</table>
								</td></tr>
							</xsl:if>
<!-- Options -->
							<xsl:if test="evo_opt!=''">
								<tr><td colspan='2'><i>Options</i></td></tr>
								<tr><td colspan='2'>
									<table class='option_text' width='100%'>
										<xsl:for-each select="evo_opt">
											<tr class='option_text'><td width='50px'><b><xsl:value-of select="name"/></b></td><td colspan='2'><xsl:value-of select="brief"/></td></tr>
											<xsl:for-each select="evo_val">
												<tr><td width='50px'></td><td width='50px'  valign='top'><b><xsl:value-of select="value"/></b></td><td><xsl:value-of select="brief"/></td></tr>
											</xsl:for-each>
										</xsl:for-each>
									</table>
								</td></tr>
							</xsl:if>
						</table>
</xsl:for-each>
					</td></tr>
				</table>
			</td></tr><tr><td></td><td>
<!-- End -->
			</td></tr>
		</table>
	</body>
</html>
</xsl:template>
</xsl:stylesheet>