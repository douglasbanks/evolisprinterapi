#addin nuget:?package=Cake.Git&version=0.19.0
#addin nuget:?package=Cake.FileHelpers&version=3.1.0
#addin nuget:?package=Cake.SemVer&version=3.0.0
#addin nuget:?package=semver&version=2.0.4
#tool nuget:?package=OctopusTools&version=5.2.0
#addin nuget:?package=Cake.Coverlet
#tool "nuget:?package=ReportGenerator"

/*************************************************************
Change project, product, and description to fit your project
*************************************************************/
var octoPackageId = "EvolisPrinterApi";
var publishedProject = "EvolisPrinterApi";
var publishedNugetProject = "EvolisPrinterApi.Contracts";

var buildDir = "build";
var nugetArtifactsDir = "artifacts/nuget";
var octoPackArtifactsDir = "artifacts/octopack";

var target = Argument("target", "Default");
var configuration = Argument("configuration", "Debug");
var built = Argument("built", false);
var pushLocalNuget = Argument("pushLocalNuget", true);
var rid = Argument<string>("rid", null);

// Commit reference name passed in from gitlab - default to 'alpha/<branchversion>' if it is a local build.
var commitRefName = EnvironmentVariable("CI_COMMIT_REF_NAME") ?? "alpha/" + System.Text.RegularExpressions.Regex.Match(GitBranchCurrent(".").FriendlyName, "(?<metadata>[^/]*)/(?<version>[\\d\\.]+)$").Groups["version"].Value;
// Build number from gitlab - default to 0 if building locally
var buildNumber = EnvironmentVariable("CI_PIPELINE_IID") ?? "0";

// Parse the reference name passed in into a metadata and version.
var parsedRefName = System.Text.RegularExpressions.Regex.Match(commitRefName, "(?<metadata>[^/]*)/(?<version>[\\d\\.]+)$");

// Default the version in case we don't have a version on our branch and have no previous tags.
var version = ParseSemVer("0.0.1");
if(!string.IsNullOrEmpty(parsedRefName.Groups["version"].Value)){
    version = ParseSemVer(parsedRefName.Groups["version"].Value);
}

var branchMetaData = parsedRefName.Groups["metadata"].Value;

// Production version has no changes to version number.
if(branchMetaData == "version"){
    Information("Building Production Release");
}
// RC tag for hotfix and release branches.
else if(branchMetaData == "hotfix" || branchMetaData == "release"){
    Information("Building Release Candidate");
    version = version.Change(prerelease: $"rc.{buildNumber}");
}
else {
    // If we have a version from the reference name, use it.
    if(!string.IsNullOrEmpty(parsedRefName.Groups["version"].Value)){
        version = ParseSemVer(parsedRefName.Groups["version"].Value);
    }
    else{
        // Get the most recent tag we can find that appears to be a version.
        var mostRecentTag = GitTags(".").Select(t => System.Text.RegularExpressions.Regex.Match(t.FriendlyName, "(?:version/)?(?<version>[\\d\\.]+)").Groups["version"].Value).OrderByDescending(t => t).FirstOrDefault(t => Semver.SemVersion.TryParse(t, out _));
        Information($"Most recent tag found: {mostRecentTag}");
        if(!string.IsNullOrEmpty(mostRecentTag) && Semver.SemVersion.TryParse(mostRecentTag, out var previousVersion)){
            version = previousVersion.Change(minor: previousVersion.Minor + 1, patch: 0);
        }
    }

    if(EnvironmentVariable("CI_COMMIT_REF_NAME") == null){
        // If this doesn't have a commit ref name, tag it as alpha, it is being built locally on a developer box.
        Information("Building Alpha Release");
        version = version.Change(prerelease: "alpha");
    }
    else {
        // Beta tag for any other branch that is built.
        Information("Building Beta");

        if(!string.IsNullOrEmpty(branchMetaData)){
            version = version.Change(prerelease: $"{branchMetaData}.{buildNumber}");
        }
        else {
            version = version.Change(prerelease: $"beta.{buildNumber}");
        }
    }
}

version = version.Change(build: GitLogTip(".").Sha);
Information($"Version: {version}");

Task("Clean")
   .WithCriteria(() => !built)
   .Does(() => {
    CleanDirectory(buildDir);
    CleanDirectory("./artifacts");
    CleanDirectory("./test");
});

Task("Build")
   .WithCriteria(() => !built)
   .IsDependentOn("Clean")
   .Does(() => {
        var settings = new DotNetCorePublishSettings
        {
            Configuration = configuration,
            OutputDirectory = buildDir,
            MSBuildSettings = new DotNetCoreMSBuildSettings().WithProperty("Version", version.ToString())
        };

        if(!string.IsNullOrEmpty(rid))
            settings.Runtime = rid;

        DotNetCorePublish($"./src/{publishedProject}/{publishedProject}.csproj", settings);
   });

Task("RunUnitTests")
   .IsDependentOn("Clean")
   .Does(() => {
       var settings = new DotNetCoreBuildSettings
       {
           Configuration = configuration,
           OutputDirectory = "test"
       };

       DotNetCoreBuild($"./src/", settings);
        var testSettings = new DotNetCoreTestSettings
        {
            Configuration = configuration,
            NoBuild = true,
            OutputDirectory = "test"
        };

        DotNetCoreTest("./src/", testSettings);
   });

Task("CodeCoverage")
    .IsDependentOn("Clean")
    .Does(() => {
        var settings = new DotNetCoreBuildSettings
        {
            Configuration = configuration,
            OutputDirectory = "test"
        };

        DotNetCoreBuild($"./src/", settings);

        var testSettings = new DotNetCoreTestSettings
        {
            Configuration = configuration,
            NoBuild = true,
            OutputDirectory = "test"
        };

        var projectFiles = GetFiles("**/*.Tests.csproj");
        foreach(var file in projectFiles)
        {
            DotNetCoreTool(file, "add", "package coverlet.msbuild");
            var coverletSettings = new CoverletSettings {
                CollectCoverage = true,
                CoverletOutputFormat = CoverletOutputFormat.json,
                CoverletOutputDirectory = Directory(@"./artifacts/coverage-results/"),
                CoverletOutputName = "coverage",
                MergeWithFile = $"{MakeAbsolute(Directory("./artifacts/coverage-results"))}/coverage.json"
            }
            .WithInclusion("[FileProcessingTasks*]*")
            .WithFilter("[*.Tests?]*")
            .WithFilter("[FileProcessingTasks]*Program")
            .WithFormat(CoverletOutputFormat.cobertura)
            .WithFormat(CoverletOutputFormat.opencover);

            DotNetCoreTest(file.ToString(), testSettings, coverletSettings);
        }

        ReportGenerator((FilePath) "./artifacts/coverage-results/coverage.opencover.xml", "./artifacts/coverage-report/", new ReportGeneratorSettings {
            ReportTypes = { ReportGeneratorReportType.Badges, ReportGeneratorReportType.HtmlInline }
        });

        Console.WriteLine($"CoveragePercent=\"{Double.Parse(System.Xml.Linq.XElement.Load("./artifacts/coverage-results/coverage.cobertura.xml").Attribute("line-rate").Value) * 100}\"");
    });

Task("PackNuget")
    .IsDependentOn("Build")
    .Does(() =>{
        var settings = new DotNetCorePackSettings
        {
            Configuration = configuration,
            WorkingDirectory = buildDir,
            OutputDirectory = nugetArtifactsDir,
            MSBuildSettings = new DotNetCoreMSBuildSettings().WithProperty("Version", version.ToString())
        };

        DotNetCorePack($"../src/{publishedNugetProject}/{publishedNugetProject}.csproj", settings);
    });

Task("PushNuget")
   .IsDependentOn("PackNuget")
   .Does(() => {
        var nuGetPath = !pushLocalNuget ? EnvironmentVariable("NUGET_URL") : EnvironmentVariable("USERPROFILE")+"\\NuGet";

        DotNetCoreNuGetPush($"{nugetArtifactsDir.Replace("/","\\")}\\*.nupkg", new DotNetCoreNuGetPushSettings {
            Source = nuGetPath,
	        ApiKey = EnvironmentVariable("NUGET_API_KEY")
        });
   });

Task("Octo-Pack")
    .IsDependentOn("Build")
    .Does(() =>{
        // Octopus Deploy doesn't support semantic versioning on our version, and octopack doesn't actually build semantic versioned nuget packages correctly.
        // Reverting the version to the old Major.Minor.Patch.Build-pre format from before.
        string prerelease = null;
        if(!string.IsNullOrEmpty(version.Prerelease)){
            if(version.Prerelease.IndexOf(".") < 0)
                prerelease = version.Prerelease;
            else
                prerelease = version.Prerelease.Substring(0, version.Prerelease.IndexOf("."));
        }

        OctoPack(octoPackageId, new OctopusPackSettings {
            BasePath = buildDir,
            Version = $"{version.Major}.{version.Minor}.{version.Patch}.{buildNumber}{(string.IsNullOrEmpty(prerelease) ? "" : $"-{prerelease}")}",
            OutFolder = octoPackArtifactsDir
        });
    });

Task("Octo-Push")
    .IsDependentOn("Octo-Pack")
    .Does(() => {
        var files = GetFiles(string.Format("{0}/*.nupkg", octoPackArtifactsDir));
        if(files.Count() == 0)
            throw new Exception("Nothing to push.");
        NuGetPush(files, new NuGetPushSettings{
            Source = EnvironmentVariable("OCTOPUS_URL"),
            ApiKey = EnvironmentVariable("OCTOPUS_KEY")
        });
		NuGetPush(files, new NuGetPushSettings{
            Source = EnvironmentVariable("CAO_OCTOPACK_URL"),
            ApiKey = EnvironmentVariable("CAO_OCTOPACK_KEY")
        });
   });

Task("Default").IsDependentOn("Build");

RunTarget(target);
